<?php
return [
    'id' => 'app-wallet-tests',
    'components' => [
        'assetManager' => [
            'basePath' => __DIR__ . '/../web/assets',
        ],
    ],
];
