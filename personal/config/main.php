<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-personal',

    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'user'],
    'controllerNamespace' => 'personal\controllers',
    'modules' => [
        'user' => [
            'class' => 'personal\modules\user\Module'
        ],
    ],
    'components' => [
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@app/themes/' . $params['theme'] . '/views',
                    '@app/modules' => '@app/themes/' . $params['theme'] . '/modules',
                    '@app/widgets' => '@app/themes/' . $params['theme'] . '/widgets',
                    '@app/layouts' => '@app/themes/' . $params['theme'] . '/layouts',
                ],
            ],
        ],
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
            'enableCsrfValidation' => false,
        ],
//        'request' => [
//            'csrfParam' => '_csrf-personal',
//        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'forceTranslation' => true,
                    'sourceMessageTable' => '{{%source_message}}',
                    'messageTable' => '{{%message}}',
                    'enableCaching' => false,
                    'cachingDuration' => 3600,
                    'sourceLanguage' => 'en_US'
                ],
                'backend*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'forceTranslation' => true,
                    'sourceMessageTable' => '{{%source_message}}',
                    'messageTable' => '{{%message}}',
                    'enableCaching' => false,
                    'cachingDuration' => 3600,
                    'sourceLanguage' => 'en_US'
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-personal', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the personal
            'name' => 'advanced-personal',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
//        'errorHandler' => [
//            'errorAction' => 'site/error',
//        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'user/front/index',
                '<action:\w+>' => 'user/front/index',
                '<controller:\w+>/<action:\w+>' => 'user/front/index',
                '<controller:\w+>/<action:\w+>/<slug:[A-Za-z0-9 -_.]+>' => 'user/front/index',
            ],
        ],
    ],
    'params' => $params,
];
