<?php
namespace personal\modules\user\controllers;

use personal\controllers\ThemeController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use Yii;

/**
 * Front controller
 */
class FrontController extends ThemeController
{
    public function init()
    {
        $this->layout = Yii::$app->getView()->theme->pathMap['@app/layouts'] . '/main';
    }
    /**
     * Displays index.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSignIn()
    {
        return $this->render('sign-in');
    }

    public function actionSignUp()
    {
        return $this->render('sign-up');
    }
}
