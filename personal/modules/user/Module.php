<?php

namespace personal\modules\user;

use Yii;

/**
 * Class Module
 * @package personal\modules\user\
 */
class Module extends \yii\base\Module
{
    /**
     * @var string
     */
    public $controllerNamespace = 'personal\modules\user\controllers';

    public function init()
    {
        parent::init();
    }


    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules([[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'sign-in',
                'route' => 'user/front/sign-in',
            ],[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'sign-up',
                'route' => 'user/front/sign-up',
            ]
        ], false);
    }

}
