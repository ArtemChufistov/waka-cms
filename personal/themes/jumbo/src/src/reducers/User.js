import {
  GET_USER_ERROR,
  GET_USER_PENDING,
  GET_USER_SUCCESS
} from "constants/ActionTypes";

const INIT_STATE = {
  info: null,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {

    case GET_USER_SUCCESS: {
      return {
        ...state,
        info: action.payload,
      };
    }
    case GET_USER_PENDING: {
      return {
        ...state,
        info: action.payload,
      };
    }
    case GET_USER_ERROR: {
      return {
        ...state,
        info: action.payload,
      };
    }
    default:
      return state;
  }
}
