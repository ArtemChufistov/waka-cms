import {apiClient, getErrorResponseMessage} from '../appUtility/Api';
import querystring from 'querystring';
import axios from 'axios';
import {
  RESTAPI_URL,
  API_VERSION
} from 'config/config';
import {
  HIDE_MESSAGE,
  INIT_URL,
  ON_HIDE_LOADER,
  ON_SHOW_LOADER,
  SHOW_MESSAGE,
  SIGNIN_FACEBOOK_USER,
  SIGNIN_FACEBOOK_USER_SUCCESS,
  SIGNIN_GITHUB_USER,
  SIGNIN_GITHUB_USER_SUCCESS,
  SIGNIN_GOOGLE_USER,
  SIGNIN_GOOGLE_USER_SUCCESS,
  SIGNIN_TWITTER_USER,
  SIGNIN_TWITTER_USER_SUCCESS,
  SIGNIN_USER_ERROR,
  SIGNIN_USER_PENDING,
  SIGNIN_USER_SUCCESS,
  SIGNOUT_USER,
  SIGNOUT_USER_SUCCESS,
  SIGNUP_USER_ERROR,
  SIGNUP_USER_PENDING,
  SIGNUP_USER_SUCCESS
} from 'constants/ActionTypes';

export const userSignUp = (data) => {
  return (dispatch) => {
    apiClient.post('/auth/signup', querystring.stringify(data)).then((response) => {
      dispatch({type: SIGNUP_USER_SUCCESS, payload: response.data});
    }).catch((error) => {
        dispatch({type: SIGNUP_USER_ERROR, payload: error.response});
    });
    dispatch({type: SIGNUP_USER_PENDING, payload: null});
  }
};
export const userSignIn = (data) => {
  return (dispatch) => {
    axios.post(RESTAPI_URL + API_VERSION + 'auth/signin', querystring.stringify(data)).then((response) => {
      dispatch({type: SIGNIN_USER_SUCCESS, payload: response.data});
    }).catch((error) => {
      dispatch({type: SIGNIN_USER_ERROR, payload: getErrorResponseMessage(error)});
    });
    dispatch({type: SIGNIN_USER_PENDING, payload: null});
  }
};
export const userSignOut = () => {
  return {
    type: SIGNOUT_USER
  };
};
export const userSignUpSuccess = (token) => {
  return {
    type: SIGNUP_USER_SUCCESS,
    payload: token
  };
};

export const userSignInSuccess = (token) => {
  return {
    type: SIGNIN_USER_SUCCESS,
    payload: token
  }
};
export const userSignOutSuccess = () => {
  return {
    type: SIGNOUT_USER_SUCCESS,
  }
};

export const showAuthMessage = (message) => {
  return {
    type: SHOW_MESSAGE,
    payload: message
  };
};


export const userGoogleSignIn = () => {
  return {
    type: SIGNIN_GOOGLE_USER
  };
};
export const userGoogleSignInSuccess = (token) => {
  return {
    type: SIGNIN_GOOGLE_USER_SUCCESS,
    payload: token
  };
};
export const userFacebookSignIn = () => {
  return {
    type: SIGNIN_FACEBOOK_USER
  };
};
export const userFacebookSignInSuccess = (token) => {
  return {
    type: SIGNIN_FACEBOOK_USER_SUCCESS,
    payload: token
  };
};
export const setInitUrl = (url) => {
  return {
    type: INIT_URL,
    payload: url
  };
};
export const userTwitterSignIn = () => {
  return {
    type: SIGNIN_TWITTER_USER
  };
};
export const userTwitterSignInSuccess = (token) => {
  return {
    type: SIGNIN_TWITTER_USER_SUCCESS,
    payload: token
  };
};
export const userGithubSignIn = () => {
  return {
    type: SIGNIN_GITHUB_USER
  };
};
export const userGithubSignInSuccess = (token) => {
  return {
    type: SIGNIN_GITHUB_USER_SUCCESS,
    payload: token
  };
};
export const showAuthLoader = () => {
  return {
    type: ON_SHOW_LOADER,
  };
};

export const hideMessage = () => {
  return {
    type: HIDE_MESSAGE,
  };
};
export const hideAuthLoader = () => {
  return {
    type: ON_HIDE_LOADER,
  };
};
