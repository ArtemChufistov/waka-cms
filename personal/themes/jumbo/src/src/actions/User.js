import {apiClient} from '../appUtility/Api';
import {
  GET_USER_ERROR,
  GET_USER_PENDING,
  GET_USER_SUCCESS
} from 'constants/ActionTypes';
const qs = require('qs');

export const getUser = (data) => {
    return function (dispatch, getState) {
        const state = getState();

        let config = {
            headers: {'Authorization': 'Bearer ' + state.auth.token.bearer},
        };
        apiClient.post('/user/get-user', qs.stringify(data), config)
            .then((response) => {
                dispatch({type: GET_USER_SUCCESS, payload: response.data})
            })
            .catch((err) => {
                dispatch({type: GET_USER_ERROR, payload: err.response});
            });
        dispatch({type: GET_USER_PENDING, payload: false});
    }
}