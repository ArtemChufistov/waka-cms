import React, {Component} from 'react';
import {NavLink, withRouter} from 'react-router-dom';
import Button from '@material-ui/core/Button';

import IntlMessages from 'util/IntlMessages';
import CustomScrollbars from 'util/CustomScrollbars';


class SidenavContent extends Component {
  componentDidMount() {
    const {history} = this.props;
    const that = this;
    const pathname = `${history.location.pathname}`;// get current path

    const menuLi = document.getElementsByClassName('menu');
    for (let i = 0; i < menuLi.length; i++) {
      menuLi[i].onclick = function (event) {
        for (let j = 0; j < menuLi.length; j++) {
          const parentLi = that.closest(this, 'li');
          if (menuLi[j] !== this && (parentLi === null || !parentLi.classList.contains('open'))) {
            menuLi[j].classList.remove('open')
          }
        }
        this.classList.toggle('open');
      }
    }

    const activeLi = document.querySelector('a[href="' + pathname + '"]');// select current a element
    try {
      const activeNav = this.closest(activeLi, 'ul'); // select closest ul
      if (activeNav.classList.contains('sub-menu')) {
        this.closest(activeNav, 'li').classList.add('open');
      } else {
        this.closest(activeLi, 'li').classList.add('open');
      }
    } catch (error) {

    }
  }

  componentWillReceiveProps(nextProps) {
    const {history} = nextProps;
    const pathname = `${history.location.pathname}`;// get current path

    const activeLi = document.querySelector('a[href="' + pathname + '"]');// select current a element
    try {
      const activeNav = this.closest(activeLi, 'ul'); // select closest ul
      if (activeNav.classList.contains('sub-menu')) {
        this.closest(activeNav, 'li').classList.add('open');
      } else {
        this.closest(activeLi, 'li').classList.add('open');
      }
    } catch (error) {

    }
  }

  closest(el, selector) {
    try {
      let matchesFn;
      // find vendor prefix
      ['matches', 'webkitMatchesSelector', 'mozMatchesSelector', 'msMatchesSelector', 'oMatchesSelector'].some(function (fn) {
        if (typeof document.body[fn] == 'function') {
          matchesFn = fn;
          return true;
        }
        return false;
      });

      let parent;

      // traverse parents
      while (el) {
        parent = el.parentElement;
        if (parent && parent[matchesFn](selector)) {
          return parent;
        }
        el = parent;
      }
    } catch (e) {

    }

    return null;
  }

  render() {
    return (
      <CustomScrollbars className=" scrollbar">
        <ul className="nav-menu">
          <li className="nav-header">
            <IntlMessages id="sidebar.personal-office"/>
          </li>
          <li className="menu no-arrow">
            <NavLink to="/personal/dashboard/news">
              <i className="zmdi zmdi-view-dashboard zmdi-hc-fw"/>
              <span className="nav-text"><IntlMessages id="sidebar.dashboard"/></span>
            </NavLink>
          </li>

          <li className="menu no-arrow">
            <NavLink to="/personal/to-do">
              <i className="zmdi zmdi-check-square zmdi-hc-fw"/>
              <span className="nav-text"><IntlMessages id="sidebar.tasks"/></span>
            </NavLink>
          </li>

          <li className="menu no-arrow">
            <NavLink to="/personal/chat-redux">
              <i className="zmdi zmdi-account-box zmdi-hc-fw"/>
              <span className="nav-text"><IntlMessages id="sidebar.messages"/></span>
            </NavLink>
          </li>
          <li className="menu no-arrow">
            <NavLink to="/personal/social-apps/profile">
              <i className="zmdi zmdi-account-box zmdi-hc-fw"/>
              <span className="nav-text"><IntlMessages id="sidebar.about"/></span>
            </NavLink>
          </li>
          <li className="menu no-arrow">
            <NavLink to="/personal/finance">
              <i className="zmdi zmdi-comment zmdi-hc-fw"/>
              <span className="nav-text"><IntlMessages id="sidebar.finance"/></span>
            </NavLink>
          </li>

          <li className="menu no-arrow">
            <NavLink to="/personal/app-module/login-1">
              <i className="zmdi zmdi-check-square zmdi-hc-fw"/>
              <span className="nav-text"><IntlMessages id="sidebar.logout"/></span>
            </NavLink>
          </li>
        </ul>
      </CustomScrollbars>
    );
  }
}

export default withRouter(SidenavContent);
