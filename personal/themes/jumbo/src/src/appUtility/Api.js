import axios from 'axios';
import {
  RESTAPI_URL,
  API_VERSION
} from 'config/config';

export default axios.create({
  baseURL: `http://67.205.129.195/medifellows/api/public/api/v1/`
});

export const httpClient = axios.create({
  baseURL: 'http://159.89.235.6/rnapp/medifellow-apis/public'
});

export const apiClient = axios.create({
  baseURL: RESTAPI_URL + API_VERSION
});

export function getErrorResponseMessage(error){
    console.log('+++error.response+++' + error);
    console.log(error.response);

    return JSON.stringify(error);

    if (error.response !== undefined && error.response.data !== undefined && error.response.data.message !== undefined) {
      return error.response.data.message;
    } else if (error.response !== undefined && error.response.data !== undefined && error.response.data.length !== undefined) {
      return error.response.data[0].message;
    } else if (error.response.statusText !== undefined) {
      return error.response.statusText;
    } else{
      return 'Unknown response error';
    }
    return 'test';
}
