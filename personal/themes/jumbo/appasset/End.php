<?php

namespace personal\themes\jumbo\appasset;

use yii\web\AssetBundle;
use yii\web\View;
use Yii;

/**
 * Main frontend application asset bundle.
 */
class End extends AssetBundle
{
    public $css = [
    ];
    public $js = [
        '/static/js/142.86ef878f.chunk.js',
        '/static/js/main.490e9350.chunk.js'
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
