<?php

namespace personal\themes\jumbo\appasset;

use yii\web\AssetBundle;
use yii\web\View;
use Yii;

/**
 * Main frontend application asset bundle.
 */
class Head extends AssetBundle
{
    public $css = [
        'static/css/142.61eab842.chunk.css',
        'static/css/main.5a4d96c0.chunk.css'
    ];
    public $js = [
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
