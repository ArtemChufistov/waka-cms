<?php

namespace personal\themes\admarkt\appasset;

use yii\web\AssetBundle;
use yii\web\View;
use Yii;

/**
 * Main frontend application asset bundle.
 */
class End extends AssetBundle
{
    public $css = [
    ];
    public $js = [
        'assets/app.js'
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
