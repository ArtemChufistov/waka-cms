<?php

namespace personal\themes\admarkt\appasset;

use yii\web\AssetBundle;
use yii\web\View;
use Yii;

/**
 * Main frontend application asset bundle.
 */
class Head extends AssetBundle
{
    public $css = [
        'vendors/material-design-iconic-font/css/material-design-iconic-font.min.css',
        'vendors/weather-icons/css/weather-icons.min.css',
        'vendors/flag/sprite-flags-32x32.css',
        'vendors/animate.css',
        'vendors/bootstrap-rtl.css',
        'vendors/loader.css',
        'vendors/react-select/react-select.css',
        'vendors/react-notification/react-notifications.css'
    ];
    public $js = [
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];

    public function __construct()
    {
        $this->sourcePath = '@app/themes/' . \Yii::$app->params['theme'] . '/assets';

    }

}
