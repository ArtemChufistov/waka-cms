<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use personal\themes\admarkt\appasset\End;
use personal\themes\admarkt\appasset\Head;

$headAsset = Head::register($this);
$endAsset = End::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <base href="<?php echo $endAsset->baseUrl;?>">
    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Admarkt">
    <link rel="apple-touch-icon-precomposed" href="favicon.ico">

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places,geometry,drawing&key=AIzaSyBgqAomc9Vukt12AV3tJLasBnNehSNKuOY"></script>
</head>
<body>
    <?php $this->beginBody() ?>
        <?php echo $content;?>
        <script>__REACT_DEVTOOLS_GLOBAL_HOOK__ = parent.__REACT_DEVTOOLS_GLOBAL_HOOK__</script>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
