<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use wallet\assets\AppAsset;

//AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <base href="/dist/">
</head>
<body>
    <?php $this->beginBody() ?>
    <div id="app"></div>
    <script src="/dist/bundle.js"></script>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
