<?php

namespace personal\assets;

use yii\web\AssetBundle;
use \yii\web\View;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->sourcePath = '@webroot/dist';
        $this->basePath = '@webroot';
        $this->js = ['bundle.js'];
        $this->jsOptions = [
            'position' => View::POS_END
        ];
    }
}
