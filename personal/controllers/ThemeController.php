<?php
namespace personal\controllers;

use Yii;
use yii\web\Controller;

class ThemeController extends Controller
{

    public function render($view, $params = [])
    {
        return parent::render(Yii::$app->getView()->theme->pathMap['@app/views'] . '/' . $view, $params);
    }
}