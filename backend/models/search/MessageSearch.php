<?php

namespace backend\models\search;

use backend\models\Message;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MessageSearch represents the model behind the search form of `Message`.
 */
class MessageSearch extends Message {
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'language', 'translation', 'correctly'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Message::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'language' => $this->language,
        ]);

        if ($this->correctly != 'empty') {
            $query->andFilterWhere([
                'correctly' => $this->correctly,
            ]);
        }

        $query->andFilterWhere(['like', 'translation', $this->translation]);

        return $dataProvider;
    }
}