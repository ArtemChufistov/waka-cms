<?php

namespace backend\models\search;

use backend\models\Transaction;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TransactionSearch represents the model behind the search form of `app\modules\finance\models\Transaction`.
 */
class TransactionSearch extends Transaction {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['currency', 'from', 'to', 'amount', 'hash'], 'safe'],
			[['id', 'from_user_id', 'to_user_id', 'currency'], 'integer'],

		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Transaction::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'from_user_id' => $this->from_user_id,
			'to_user_id' => $this->to_user_id,
		]);

		return $dataProvider;
	}
}
