<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user_log".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $type
 * @property string $additional_info
 * @property string $comments
 * @property integer $created_at
 * @property integer $updated_at
 */
class UserLog extends \common\models\UserLog
{
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }
}
