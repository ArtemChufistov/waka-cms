<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * Class Tickets
 *
 *
 * @property TicketItems $lastTicketItems
 */

class Tickets extends \common\models\Tickets
{
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastTicketItems()
    {
        return $this->hasOne(TicketItems::className(), ['ticket_id' => 'id'])->orderBy('created_at DESC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketItems()
    {
        return $this->hasMany(TicketItems::className(), ['ticket_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
