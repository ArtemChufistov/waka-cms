<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;


class PaySystem extends \common\models\PaySystem
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payment::className(), ['pay_system_id' => 'id']);
    }
}
