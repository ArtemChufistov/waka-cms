<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user_wallet".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $address
 * @property string $key_data
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 *
 * @property User $user
 */
class UserWallet extends \common\models\UserWallet {

    public $password;
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules() {
        $rules = parent::rules();
        $rules[] = ['password', 'safe'];
        return $rules;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
