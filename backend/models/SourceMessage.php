<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "source_message".
 *
 * @property integer $id
 * @property string $category
 * @property string $message
 *
 * @property Message[] $messages
 */
class SourceMessage extends \common\models\SourceMessage
{
    const CATEGORY_TYPE_APP = 'App';
    const CATEGORY_TYPE_WALLET = 'Wallet';

    public static function getListCategoryTitle(){
        return [
            self::CATEGORY_TYPE_APP => 'Общие переводы',
            self::CATEGORY_TYPE_WALLET => 'Личный кабинет пользователя',
        ];
    }

    public static function getCategoryTitle($type){
        $list = self::getListCategoryTitle();
        return $list[$type];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category' => Yii::t('app', 'Category'),
            'message' => Yii::t('app', 'Message'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['id' => 'id']);
    }
}
