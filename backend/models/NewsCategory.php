<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "news_category".
 *
 * @property integer $id
 * @property string $title
 *
 * @property News[] $news
 */
class NewsCategory extends \common\models\NewsCategory
{

    public static function getList(){
        return ArrayHelper::map(self::find()->where('id >= 0')->all(), 'id', 'title');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasMany(News::className(), ['category_id' => 'id']);
    }
}
