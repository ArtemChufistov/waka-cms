<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;

class Contract extends \common\models\Contract
{

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

}
