<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "payee_accounts".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $payee_name
 * @property string $payee_account
 * @property string $payee_key
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class PayeeAccounts extends \common\models\PayeeAccounts
{

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
