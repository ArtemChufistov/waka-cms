<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "language".
 *
 * @property integer $id
 * @property string $title
 * @property string $code
 * @property integer $created_at
 * @property integer $updated_at
 */
class Language extends \common\models\Language
{
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'code' => 'Code',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function list(){
        $list = self::find()->where('id >= :id',[':id'=> '0'])->all();
        return ArrayHelper::map($list, 'code', 'title');
    }
}
