<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;

class PersonalWallet extends \common\models\PersonalWallet
{

    const PERSONAL_WALLET_TYPE_MASTER = 1;
    const PERSONAL_WALLET_TYPE_HOT = 2;

    public static function getListTypeTitle(){
        return [
            self::PERSONAL_WALLET_TYPE_MASTER => 'Master',
            self::PERSONAL_WALLET_TYPE_HOT => 'Hot',
        ];
    }

    public static function getTypeTitle($type){
        $list = self::getListTypeTitle();
        return $list[$type];
    }

    public $password;
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['password', 'safe'];
        return $rules;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
