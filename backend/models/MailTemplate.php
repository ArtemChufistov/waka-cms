<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "mail_template".
 *
 * @property integer $id
 * @property string $type
 * @property string $headers
 * @property string $content
 */
class MailTemplate extends \common\models\MailTemplate
{

}
