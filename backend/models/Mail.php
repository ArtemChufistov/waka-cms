<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "mail_sender".
 *
 * @property integer $id
 * @property string $email
 * @property string $title
 * @property string $content
 * @property integer $status
 * @property integer $sender_at
 * @property integer $created_at
 * @property integer $updated_at
 */
class Mail extends \common\models\Mail
{

}
