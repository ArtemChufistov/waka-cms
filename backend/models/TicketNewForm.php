<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18.10.2017
 * Time: 15:43
 */

namespace backend\models;

use Yii;
use yii\base\Model;

class TicketNewForm extends Model {


    public $type;
    public $title;
    public $message;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['type', 'title', 'message'], 'required'],
            ['type', 'integer'],
            [['title', 'message'], 'string'],
        ];
    }

    public function create($user_id) {
        if ($this->validate()) {
            $ticket = new Tickets();
            $ticket->user_id = $user_id;
            $ticket->type = $this->type;
            $ticket->title = $this->title;
            $ticket->save();
            $ticketItem = new TicketItems();
            $ticketItem->user_id = $user_id;
            $ticketItem->ticket_id = $ticket->getPrimaryKey();
            $ticketItem->message = $this->message;
            $ticketItem->is_file = 0;
            $ticketItem->is_visible = 0;
            $ticketItem->save();
            return $ticket;
        } else {
            return false;
        }
    }

}
