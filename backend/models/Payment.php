<?php

namespace backend\models;

use common\modules\finance\models\Currency;
use Yii;
use yii\behaviors\TimestampBehavior;

class Payment extends \common\models\Payment
{
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'user_id' => Yii::t('app', 'User'),
            'amount' => Yii::t('app', 'Amount'),
            'real_amount' => Yii::t('app', 'Real Amount'),
            'from_currency_id' => Yii::t('app', 'From Currency'),
            'to_currency_id' => Yii::t('app', 'To Currency'),
            'to_currency_amount' => Yii::t('app', 'To Currency Amount'),
            'commission_from' => Yii::t('app', 'Commission From'),
            'commission_to' => Yii::t('app', 'Commission '),
            'pay_system_id' => Yii::t('app', 'Pay System ID'),
            'comment' => Yii::t('app', 'Comment'),
            'course_from_to' => Yii::t('app', 'Course From To'),
            'course_to_usd' => Yii::t('app', 'Course To Usd'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'from_currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'to_currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaySystem()
    {
        return $this->hasOne(PaySystem::className(), ['id' => 'pay_system_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
