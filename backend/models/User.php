<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;

class User extends \common\models\User
{
    public function rules() {
        return [
            [['username', 'auth_key', 'password_hash', 'email'], 'required'],
            [['step_auth', 'step_method', 'status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'country', 'firstName', 'lastName', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key', 'phone'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['phone'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

}
