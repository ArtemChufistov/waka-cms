<?php

namespace backend\models;

use Yii;

class Transaction extends \common\models\Transaction {

    const STATUS_CREATED = 0;
    const STATUS_COMPLETE = 5;

    const METHOD_OKPAY = 'okpay';
    const METHOD_WALLET_ONE = 'walletOne';
    const METHOD_PERFECT_MONEY = 'perfectMoney';
    const METHOD_PAYEER = 'payeer';

    const TYPE_REFILL = 1;
    const TYPE_TRANSFER = 2;
    const TYPE_WITHDRAW = 3;

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'from_user_id' => Yii::t('app', 'От пользователя'),
            'to_user_id' => Yii::t('app', 'Пользователю'),
            'currency' => Yii::t('app', 'Currency'),
            'amount' => Yii::t('app', 'Amount'),
            'from' => Yii::t('app', 'From'),
            'to' => Yii::t('app', 'To'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
            'commission_id' => Yii::t('app', 'Commission ID'),
            'hash' => Yii::t('app', 'Hash'),
            'comment' => Yii::t('app', 'Comment'),
            'type' => Yii::t('app', 'Type'),
        ];
    }
}
