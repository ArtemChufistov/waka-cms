<?php
namespace app\controllers;

use common\components\UserGroupRule;
use Yii;
use yii\console\Controller;

class RbacController extends Controller {
	public function actionInit() {

		//create roles
		$guest = Yii::$app->authManager->createRole('guest');
		$user = Yii::$app->authManager->createRole('user');
		$manager = Yii::$app->authManager->createRole('manager');
		$admin = Yii::$app->authManager->createRole('admin');
		$root = Yii::$app->authManager->createRole('root');

		// Create simple, based on action{$NAME} permissions
		$login = Yii::$app->authManager->createPermission('login');
		$logout = Yii::$app->authManager->createPermission('logout');
		$error = Yii::$app->authManager->createPermission('error');
		$signUp = Yii::$app->authManager->createPermission('sign-up');
		$index = Yii::$app->authManager->createPermission('index');
		$view = Yii::$app->authManager->createPermission('view');
		$create = Yii::$app->authManager->createPermission('create');
		$update = Yii::$app->authManager->createPermission('update');
		$delete = Yii::$app->authManager->createPermission('delete');

		// Add permissions in Yii::$app->authManager
		Yii::$app->authManager->add($login);
		Yii::$app->authManager->add($logout);
		Yii::$app->authManager->add($error);
		Yii::$app->authManager->add($signUp);
		Yii::$app->authManager->add($index);
		Yii::$app->authManager->add($view);
		Yii::$app->authManager->add($create);
		Yii::$app->authManager->add($update);
		Yii::$app->authManager->add($delete);

		// Add rule, based on UserExt->group === $user->group
		$userGroupRule = new UserGroupRule();
		Yii::$app->authManager->add($userGroupRule);

		// Add rule "UserGroupRule" in roles
		$guest->ruleName = $userGroupRule->name;
		$user->ruleName = $userGroupRule->name;
		$manager->ruleName = $userGroupRule->name;
		$admin->ruleName = $userGroupRule->name;
		$root->ruleName = $userGroupRule->name;

		// Add roles in Yii::$app->authManager
		Yii::$app->authManager->add($guest);
		Yii::$app->authManager->add($user);
		Yii::$app->authManager->add($manager);
		Yii::$app->authManager->add($admin);
		Yii::$app->authManager->add($root);

		// Add permission-per-role in Yii::$app->authManager
		// Guest
		Yii::$app->authManager->addChild($guest, $login);
		Yii::$app->authManager->addChild($guest, $logout);
		Yii::$app->authManager->addChild($guest, $error);
		Yii::$app->authManager->addChild($guest, $signUp);
		Yii::$app->authManager->addChild($guest, $index);
		Yii::$app->authManager->addChild($guest, $view);

		// BRAND
		Yii::$app->authManager->addChild($user, $update);
		Yii::$app->authManager->addChild($user, $guest);

		// TALENT
		Yii::$app->authManager->addChild($manager, $update);
		Yii::$app->authManager->addChild($manager, $guest);

		// Admin
		Yii::$app->authManager->addChild($admin, $delete);
		Yii::$app->authManager->addChild($admin, $user);
		Yii::$app->authManager->addChild($admin, $manager);

		// Root
		Yii::$app->authManager->addChild($root, $delete);
		Yii::$app->authManager->addChild($root, $user);
		Yii::$app->authManager->addChild($root, $manager);
	}
}
