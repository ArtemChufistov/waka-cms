<?php

namespace app\controllers;

use common\models\Currency;
use common\modules\crypto\coin\factory\CoinFactory;

class TestController extends \yii\console\Controller {

	public function actionTest() {

		//$userId = 8;
		//$userId = 19;
		$userId = 22;
		$userId = '';
		//$userId = '1432b602ff90fadcef6f6e7eab619d8e43401d76c0bfbcd793bd710057c012dd';
		$address = '39AcQWoFfqWHXosr8y2mfozqWdH7G5Skmo';

		$currency = Currency::find()->where(['key' => Currency::KEY_BTC])->one();

		$data = CoinFactory::get($currency->key);

		//$result = $data->getBalance($userId);
		//$result = $data->nodeInfo();
		//$result = $data->verifychain();
		//$result = $data->dumpprivkey($address);
		//$result = $data->dumpwallet();
		//$result = $data->getrawmempool();
		//$result = $data->listreceivedbyaccount();
		//$result = $data->listtransactions();
		//$result = $data->getAddresses($userId);
		//$result = $data->listaccounts();

		echo 'Результат:' . "\r\n";

		print_r($result);

		echo "\r\n";
	}

	public function actionIndex() {
		while (true) {
			echo 'test' . "\r\n";
			sleep(3);
		}
	}

	public function actionGetCurrenciesList() {
		$merchantId = 10131;
		$merchantSecret = 'cX2r$CAXpM^bxB#JIN09';

		$crypchant = new \Crypchant($merchantId, $merchantSecret);

		$result = $crypchant->getCurrenciesList();

		print_r($result);
		echo "\r\n";
	}

	public function actionGetDefaultFeesList() {
		$merchantId = 10117;
		$merchantSecret = 'UvCYa0C6GZwSPh7v';

		$crypchant = new \Crypchant($merchantId, $merchantSecret);

		$result = $crypchant->getDefaultFeesList();

		print_r($result);
		echo "\r\n";
	}

	public function actionSendFromMerchant() {

		$merchantId = 10131;
		$merchantSecret = 'cX2r$CAXpM^bxB#JIN09';

		$crypchant = new \Crypchant($merchantId, $merchantSecret);

		$address = '3AiTav7w6maxUUNskPoqFnpEJt98qvVqbr';
		//$currency = 'dogecoin';
		$currency = 'bitcoin';
		//$amount = 0.00005;
		$amount = 0.001;

		$result = $crypchant->sendFromMerchant($currency, $address, $amount, 0.00004);

		print_r($result);
		echo "\r\n";
	}

	public function actionGetTransactionsList() {

		$merchantId = 10117;
		$merchantSecret = 'UvCYa0C6GZwSPh7v';

		$crypchant = new \Crypchant($merchantId, $merchantSecret);

		//$currency = 'ethereum';
		//$currency = 'bitcoin';
		$currency = 'dogecoin';
		$count = 10;
		$pageNum = 0;

		$result = $crypchant->getTransactionsList($currency, $count, $pageNum);

		print_r($result);
		echo "\r\n";
	}

	public function actionBalance() {

		$merchantId = 10131;
		$merchantSecret = 'cX2r$CAXpM^bxB#JIN09';

		$crypchant = new \Crypchant($merchantId, $merchantSecret);

		//$currency = 'ethereum';
		$currency = 'bitcoin';
		//$currency = 'litecoin';
		//$currency = 'dogecoin';
		$confirm = 0;

		$result = $crypchant->getAddressesBalanceSum($currency, $confirm);

		print_r($result);
		echo "\r\n";
	}

	public function actionShowAddresses() {

		$merchantId = 10117;
		$merchantSecret = 'UvCYa0C6GZwSPh7v';

		$crypchant = new \Crypchant($merchantId, $merchantSecret);

		$currency = 'bitcoin';
		$count = 10;
		$pageNum = 0;
		$merchantOnly = false;

		$result = $crypchant->showAddresses($currency, $merchantOnly, $count, $pageNum);

		print_r($result);
		echo "\r\n";
	}

	public function actionGenerateAddress() {
		$merchantId = 10117;
		$merchantSecret = 'UvCYa0C6GZwSPh7v';

		$crypchant = new \Crypchant($merchantId, $merchantSecret);

		$currency = 'bitcoin';

		$result = $crypchant->generateAddress($currency);

		print_r($result);
		echo "\r\n";
	}
}