<?php
namespace app\controllers;

use common\components\Node;
use common\models\Preference;
use common\modules\crontab\models\CronLog;
use common\modules\crontab\models\Job;
use common\modules\ethernode\components\cron\HotWalletComponent;
use common\modules\finance\components\cron\SyncComponent;
use yii\console\Controller;

class CronController extends Controller {
	public function actionIndex($cronNum) {
		echo 'Запущен в: ' . date('Y-m-d H:i:s') . "\r\n";

		if (empty($cronNum)) {
			$cronNum = Job::CRON_NUM_FIRST;
		}

		$startTime = time();
		$cronRunPeriodInSec = Preference::find()->where(['key' => Preference::KEY_CRON_RUN_PERIOD])->one()->value;

		while ((time() - $startTime) < $cronRunPeriodInSec - 1) {

			$job = Job::find()
				->where(['active' => Job::ACTIVE_TRUE])
				->andWhere(['cron_num' => $cronNum])
				->orderBy('RAND()')
				->one();

			if ((time() - strtotime($job->last_date)) < $job->frequency) {
				continue;
			}

			$job->last_date = date('Y-m-d H:i:s');
			$job->save(false);

			echo 'Выполнение в: ' . date('Y-m-d H:i:s') . ' задания: ' . $job->key . "\r\n";

			try {
				$cronLog = new CronLog;

				$cronLog->success = 0;
				$cronLog->job_id = $job->id;
				$cronLog->date_start = date('Y-m-d H:i:s');

				$info = call_user_func($job->key);

				$cronLog->success = 1;
				$cronLog->addition_info = $info;
			} catch (exception $e) {
				echo $e->getMessage();
				$cronLog->addition_info = $e->getMessage();
			}

			$cronLog->date_end = date('Y-m-d H:i:s');
			$cronLog->save();
		}

		$cronLogArray = CronLog::find()->orderBy(['id' => SORT_ASC])->all();

		$counter = 0;
		while (count($cronLogArray) > CronLog::COUNT_ROWS) {
			$cronLogArray[$counter]->delete();
			$counter++;
		}

		echo 'Завершен в: ' . date('Y-m-d H:i:s') . "\r\n";
	}

	public function actionTestgetbalance() {
		$address = '0x02062571f42c978E3C111A5b32F29F3a708920Dc';

		$result = Node::sendMethod('USD', 'getBalance', $address);

		print_r($result);
	}

	public function actionTestrefill() {
		$newTransaction = Transaction::find()->where()->one();

		$params = json_decode($newTransaction->methodVars, true);
		$result = Node::sendTransaction($newTransaction->currency, $newTransaction->contractMethod, $params);

		print_r($result);

	}

	public function actionTestgetnonce() {
		$result = Node::getNonce('USD');
		print_r($result);
	}

	public function actionGetTransaction($hash) {
		$result = Node::getTransaction($hash);
		print_r($result);
	}

	public function actionEtherContractTransaction() {
		SyncComponent::etherContractTransaction();
	}

	public function actionUnbusyWallet() {
		HotWalletComponent::unbusyWallet();
	}

	public function actionRefreshBalanceHotWallet() {
		HotWalletComponent::refreshBalance();
	}

	public function actionGetEthBalance() {
		$address = '0xa2006C41A901eAfC1633F03EaeebC938599D7659';
		$balance = Node::getEthBalance($address);

		print_r($balance);
	}

	public function actionTestpreg() {
		$string = 'U12345';

		echo preg_match("/^U[0-9]{3,10}\z/", $string) . "\r\n";
	}
}
