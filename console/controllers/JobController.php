<?php
namespace app\controllers;

use common\models\Job;
use yii\console\Controller;
use yii\httpclient\Client;

class JobController extends Controller {
	const SLEEP_TIME = 0.001;

	public function actionHandle($cronNum = 1) {
		while (true) {
			$job = Job::find()
				->select(['id', 'method', 'frequency', 'active', '(unix_timestamp(last_date) + frequency) as date_freq'])
				->where(['active' => Job::ACTIVE_TRUE])
                ->andWhere(['cron_num' => $cronNum])
				->orderBy(['date_freq' => SORT_ASC])
				->one();

			if (!empty($job)) {
			    echo 'Выполнение задания: ' . $job->method . "\r\n";
                try {
                    $info = call_user_func($job->method);

                    $job->last_date = date('Y-m-d H:i:s');
                    $job->save(false);
                } catch (\Exception $e) {
                    echo 'Ошибка при выполнении задания: "' . $job->method . '" текст: ' . $e->getMessage() . "\r\n";
                }
            }

			sleep(self::SLEEP_TIME);
		}
	}
}