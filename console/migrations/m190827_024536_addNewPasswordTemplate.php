<?php

use yii\db\Migration;

/**
 * Class m190827_024536_addNewPasswordTemplate
 */
class m190827_024536_addNewPasswordTemplate extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->batchInsert('mail_template',
			['type', 'content', 'language', 'headers'], [
				['newPassword', '
                Пароль на вашу учётную запись изменён
                <br>
                <span style="color:#3bcdb0; font-size:18px; font-weight:bold;">Здравствуйте!</span><br><br>
                Была произведена смена пароля Вашего аккаунта.<br><br>
                Теперь вы можете продолжить пользоваться своей учётной записьмю
            ', 'ru_RU', 'Пароль на вашу учётную запись изменён'],
			]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m190827_024536_addNewPasswordTemplate cannot be reverted.\n";

		return false;
	}

	/*
		    // Use up()/down() to run migration code without a transaction.
		    public function up()
		    {

		    }

		    public function down()
		    {
		        echo "m190827_024536_addNewPasswordTemplate cannot be reverted.\n";

		        return false;
		    }
	*/
}
