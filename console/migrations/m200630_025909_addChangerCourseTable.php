<?php

use yii\db\Migration;

/**
 * Class m200630_025909_addChangerCourseTable
 */
class m200630_025909_addChangerCourseTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('changer_paysys', [
            'id' => $this->primaryKey(),
            'key' => $this->string(),
            'title' => $this->string(),
        ]);

        $this->createTable('changer_course', [
            'id' => $this->primaryKey(),
            'currency_id_from' => $this->integer(),
            'currency_id_to' => $this->integer(),
            'course' => $this->float(),
        ]);

        $this->createTable('changer_currency', [
            'id' => $this->primaryKey(),
            'key' => $this->string(),
            'title' => $this->string(),
        ]);

        $this->createTable('changer_wallet', [
            'id' => $this->primaryKey(),
            'key' => $this->string(),
            'title' => $this->string(),
            'paysys_id' => $this->integer(),
            'currency_id' => $this->integer(),
            'balance' => $this->float(),
            'num' => $this->string(),
            'secret1' => $this->string(),
            'secret2' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200630_025909_addChangerCourseTable cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200630_025909_addChangerCourseTable cannot be reverted.\n";

        return false;
    }
    */
}
