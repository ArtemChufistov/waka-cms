<?php

use yii\db\Migration;

/**
 * Class m181126_025527_smsMessage
 */
class m181126_025527_smsMessage extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sms}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'phone' => $this->string(),
            'text' => $this->string(),
            'status' => $this->string(),
            'date' => $this->dateTime(),
        ]);

        $this->addColumn('{{%user}}', 'sms_confirmation_code', $this->integer()->defaultValue(NULL)->after('phone'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181126_025527_smsMessage cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181126_025527_smsMessage cannot be reverted.\n";

        return false;
    }
    */
}
