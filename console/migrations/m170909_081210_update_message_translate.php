<?php

use yii\db\Migration;

class m170909_081210_update_message_translate extends Migration
{
    public function up()
    {
        $this->addColumn('{{%message}}', 'correctly', $this->smallInteger()->defaultValue(1)->notNull()->unsigned());
    }

    public function down()
    {

        $this->dropColumn('{{%message}}', 'correctly');
    }
}
