<?php

use yii\db\Migration;

/**
 * Class m190923_050042_addChartTable
 */
class m190923_050042_addChartTable extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->createTable('course_chart', [
			'id' => $this->primaryKey(),
			'high' => $this->float(),
			'low' => $this->float(),
			'open' => $this->float(),
			'close' => $this->float(),
			'volume' => $this->float(),
			'quoteVolume' => $this->float(),
			'weightedAverage' => $this->float(),
			'date' => $this->integer(),
		]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		$this->createTable('course_chart');
	}

	/*
		    // Use up()/down() to run migration code without a transaction.
		    public function up()
		    {

		    }

		    public function down()
		    {
		        echo "m190923_050042_addChartTable cannot be reverted.\n";

		        return false;
		    }
	*/
}
