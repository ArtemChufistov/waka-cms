<?php

use yii\db\Migration;

class m170925_112031_update_currency_decimal extends Migration
{
    public function safeUp()
    {
       // $this->alterColumn('currency', 'course_to_usd', $this->float());
    }

    public function safeDown()
    {
        $this->alterColumn('currency', 'course_to_usd', $this->bigInteger(20));
    }
}
