<?php

use yii\db\Migration;
use common\modules\changer\models\Currency;
use common\modules\changer\models\Paysys;
use common\modules\changer\models\Wallet;
use common\modules\changer\models\WalletCurrencyTo;

/**
 * Class m200810_182617_addWalletsForPaysys
 */
class m200810_182617_addWalletsForPaysys extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('changer_wallet_currency_to', [
            'id' => $this->primaryKey(),
            'wallet_id' => $this->integer(),
            'currency_id_to' => $this->integer()
        ]);

        /* Advanced Cash USD */

        $currency = Currency::find()->where(['key' => Currency::KEY_ADVCUSD])->one();
        $paysys = Paysys::find()->where(['key' => Paysys::KEY_ADVCASH])->one();

        $wallet = new Wallet;
        $wallet->title = 'Default Advanced Cash USD wallet';
        $wallet->paysys_id = $paysys->id;
        $wallet->currency_id = $currency->id;
        $wallet->balance = 0;
        $wallet->save(false);

        $walletCurrencyTo = new WalletCurrencyTo;
        $walletCurrencyTo->wallet_id = $wallet->id;
        $walletCurrencyTo->currency_id_to = $currency->id;
        $walletCurrencyTo->save(false);

        /* Advanced Cash RUB */

        $currency = Currency::find()->where(['key' => Currency::KEY_ADVCRUB])->one();
        $paysys = Paysys::find()->where(['key' => Paysys::KEY_ADVCASH])->one();

        $wallet = new Wallet;
        $wallet->title = 'Default Advanced Cash RUB wallet';
        $wallet->paysys_id = $paysys->id;
        $wallet->currency_id = $currency->id;
        $wallet->balance = 0;
        $wallet->save(false);

        $walletCurrencyTo = new WalletCurrencyTo;
        $walletCurrencyTo->wallet_id = $wallet->id;
        $walletCurrencyTo->currency_id_to = $currency->id;
        $walletCurrencyTo->save(false);

        /* Perfect Money USD */

        $currency = Currency::find()->where(['key' => Currency::KEY_PMUSD])->one();
        $paysys = Paysys::find()->where(['key' => Paysys::KEY_PM])->one();

        $wallet = new Wallet;
        $wallet->title = 'Default Perfect Money USD wallet';
        $wallet->paysys_id = $paysys->id;
        $wallet->currency_id = $currency->id;
        $wallet->balance = 0;
        $wallet->save(false);

        $walletCurrencyTo = new WalletCurrencyTo;
        $walletCurrencyTo->wallet_id = $wallet->id;
        $walletCurrencyTo->currency_id_to = $currency->id;
        $walletCurrencyTo->save(false);

        /* Perfect Money EUR */

        $currency = Currency::find()->where(['key' => Currency::KEY_PMEUR])->one();
        $paysys = Paysys::find()->where(['key' => Paysys::KEY_PM])->one();

        $wallet = new Wallet;
        $wallet->title = 'Default Perfect Money EUR wallet';
        $wallet->paysys_id = $paysys->id;
        $wallet->currency_id = $currency->id;
        $wallet->balance = 0;
        $wallet->save(false);

        $walletCurrencyTo = new WalletCurrencyTo;
        $walletCurrencyTo->wallet_id = $wallet->id;
        $walletCurrencyTo->currency_id_to = $currency->id;
        $walletCurrencyTo->save(false);

        /* Payeer USD */

        $currency = Currency::find()->where(['key' => Currency::KEY_PAYEERUSD])->one();
        $paysys = Paysys::find()->where(['key' => Paysys::KEY_PAYEER])->one();

        $wallet = new Wallet;
        $wallet->title = 'Default Payeer USD wallet';
        $wallet->paysys_id = $paysys->id;
        $wallet->currency_id = $currency->id;
        $wallet->balance = 0;
        $wallet->save(false);

        $walletCurrencyTo = new WalletCurrencyTo;
        $walletCurrencyTo->wallet_id = $wallet->id;
        $walletCurrencyTo->currency_id_to = $currency->id;
        $walletCurrencyTo->save(false);

        /* Payeer RUB */

        $currency = Currency::find()->where(['key' => Currency::KEY_PAYEERRUB])->one();
        $paysys = Paysys::find()->where(['key' => Paysys::KEY_PAYEER])->one();

        $wallet = new Wallet;
        $wallet->title = 'Default Payeer RUB wallet';
        $wallet->paysys_id = $paysys->id;
        $wallet->currency_id = $currency->id;
        $wallet->balance = 0;
        $wallet->save(false);

        $walletCurrencyTo = new WalletCurrencyTo;
        $walletCurrencyTo->wallet_id = $wallet->id;
        $walletCurrencyTo->currency_id_to = $currency->id;
        $walletCurrencyTo->save(false);

        /* Sberbank RUB */

        $currency = Currency::find()->where(['key' => Currency::KEY_SBERRUB])->one();
        $paysys = Paysys::find()->where(['key' => Paysys::KEY_SBERBANK])->one();

        $wallet = new Wallet;
        $wallet->title = 'Default Sberbank RUB wallet';
        $wallet->paysys_id = $paysys->id;
        $wallet->currency_id = $currency->id;
        $wallet->balance = 0;
        $wallet->save(false);

        $walletCurrencyTo = new WalletCurrencyTo;
        $walletCurrencyTo->wallet_id = $wallet->id;
        $walletCurrencyTo->currency_id_to = $currency->id;
        $walletCurrencyTo->save(false);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200810_182617_addWalletsForPaysys cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200810_182617_addWalletsForPaysys cannot be reverted.\n";

        return false;
    }
    */
}
