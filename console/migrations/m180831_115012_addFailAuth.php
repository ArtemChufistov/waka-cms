<?php

use yii\db\Migration;

/**
 * Class m180831_115012_addFailAuth
 */
class m180831_115012_addFailAuth extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%fail_auth}}', [
            'id' => $this->primaryKey(),
            'ip' => $this->string(),
            'agent' => $this->string(),
            'date' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180831_115012_addFailAuth cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180831_115012_addFailAuth cannot be reverted.\n";

        return false;
    }
    */
}
