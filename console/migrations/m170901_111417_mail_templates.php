<?php

use yii\db\Migration;

class m170901_111417_mail_templates extends Migration
{
    public function up()
    {
        $this->batchInsert('mail_template',
            ['type', 'content', 'language', 'headers'], [
            ['registrationSuccessConfirm', '
                Поздравляем с успешной регистрацией в платёжной системе <br><br>
                Для Вас был сгенерирован пароль: <span style="color:#3bcdb0; font-weight:bold;">{{password}}</span>.<br>
                Используйте его при входе в личный кабинет.
            ', 'ru_RU', 'Регистрация'],
            ['resetPassword', '
                Восстановление аккаунта
                <br>
                <span style="color:#3bcdb0; font-size:18px; font-weight:bold;">Здравствуйте!</span><br><br>
                Была произведена попытка восстановления Вашего аккаунта.<br><br>
                Если это были Вы, нажмите на кнопку "Сменить пароль" или перейдите по <a href="{{link}}" style="color:#3bcdb0; font-weight:bold;">этой ссылке</a>.
                <a href="{{link}}">Сменить пароль</a>
            ', 'ru_RU', 'Восстановление аккаунта']
        ]);
    }

    public function down()
    {
        echo "m170901_111417_mail_templates cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
