<?php

use yii\db\Migration;

class m170825_041900_list_lang extends Migration {
    public function up() {
        $this->createTable("language", [
            "id" => $this->primaryKey(),
            "title" => $this->string(),
            "code" => $this->string(),
            "created_at" => $this->integer(),
            "updated_at" => $this->integer(),
        ]);

    }

    public function down() {
        $this->dropTable("language");
    }

}
