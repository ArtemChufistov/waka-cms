<?php

use yii\db\Migration;

/**
 * Class m200718_161633_addExchangerOrderFields
 */
class m200718_161633_addExchangerOrderFields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%changer_order}}', 'type', $this->string()->defaultValue('real')->after('id'));
        $this->addColumn('{{%changer_order}}', 'status', $this->string()->defaultValue('new')->after('type'));
        $this->addColumn('{{%changer_order}}', 'amount_from', $this->float()->after('currency_id_to'));
        $this->addColumn('{{%changer_order}}', 'amount_to', $this->float()->after('amount_from'));
        $this->addColumn('{{%changer_order}}', 'email', $this->string()->after('course'));
        $this->addColumn('{{%changer_order}}', 'phone', $this->string()->after('email'));
        $this->addColumn('{{%changer_order}}', 'wallet_from', $this->string()->after('phone'));
        $this->addColumn('{{%changer_order}}', 'wallet_to', $this->string()->after('wallet_from'));
        $this->addColumn('{{%changer_order}}', 'fio_from', $this->string()->after('wallet_to'));
        $this->addColumn('{{%changer_order}}', 'fio_to', $this->string()->after('fio_from'));
        $this->addColumn('{{%changer_order}}', 'card_from', $this->string()->after('fio_to'));
        $this->addColumn('{{%changer_order}}', 'card_to', $this->string()->after('card_from'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200718_161633_addExchangerOrderFields cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200718_161633_addExchangerOrderFields cannot be reverted.\n";

        return false;
    }
    */
}
