<?php

use yii\db\Migration;

class m170718_033206_add_balance extends Migration
{
    public function up()
    {

        $this->createTable('user_balance',[
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'currency_id' => $this->integer()->notNull(),
            'amount' => $this->bigInteger()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('FK_user_balance_user', 'user_balance', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_user_balance_currency', 'user_balance', 'currency_id', 'currency', 'id');
    }

    public function down()
    {
        $this->dropTable('user_balance');
    }

}
