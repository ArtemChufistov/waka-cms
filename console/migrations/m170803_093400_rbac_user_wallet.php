<?php

use yii\db\Migration;

class m170803_093400_rbac_user_wallet extends Migration
{
    public function up() {

        $index = Yii::$app->authManager->createPermission('/user-wallet/index');
        $view = Yii::$app->authManager->createPermission('/user-wallet/view');
        $create = Yii::$app->authManager->createPermission('/user-wallet/create');
        $update = Yii::$app->authManager->createPermission('/user-wallet/update');
        $delete = Yii::$app->authManager->createPermission('/user-wallet/delete');

        Yii::$app->authManager->add($index);
        Yii::$app->authManager->add($view);
        Yii::$app->authManager->add($create);
        Yii::$app->authManager->add($update);
        Yii::$app->authManager->add($delete);

        $guest = Yii::$app->authManager->getRole('guest');
        $user = Yii::$app->authManager->getRole('user');
        $manager = Yii::$app->authManager->getRole('manager');
        $admin = Yii::$app->authManager->getRole('admin');
        $root = Yii::$app->authManager->getRole('root');

        Yii::$app->authManager->addChild($manager, $index);
        Yii::$app->authManager->addChild($manager, $view);
        Yii::$app->authManager->addChild($admin, $create);
        Yii::$app->authManager->addChild($admin, $update);
        Yii::$app->authManager->addChild($admin, $delete);
    }

    public function down() {
        $index = Yii::$app->authManager->getPermission('/user-wallet/index');
        $view = Yii::$app->authManager->getPermission('/user-wallet/view');
        $create = Yii::$app->authManager->getPermission('/user-wallet/create');
        $update = Yii::$app->authManager->getPermission('/user-wallet/update');
        $delete = Yii::$app->authManager->getPermission('/user-wallet/delete');

        $manager = Yii::$app->authManager->getRole('manager');
        $admin = Yii::$app->authManager->getRole('admin');
        Yii::$app->authManager->removeChild($manager, $index);
        Yii::$app->authManager->removeChild($manager, $view);
        Yii::$app->authManager->removeChild($admin, $create);
        Yii::$app->authManager->removeChild($admin, $update);
        Yii::$app->authManager->removeChild($admin, $delete);

        Yii::$app->getAuthManager()->remove($index);
        Yii::$app->getAuthManager()->remove($view);
        Yii::$app->getAuthManager()->remove($create);
        Yii::$app->getAuthManager()->remove($update);
        Yii::$app->getAuthManager()->remove($delete);
    }
}
