<?php

use yii\db\Migration;

class m170929_060654_mail_template_step_login extends Migration
{
    public function up()
    {
        $this->insert('mail_template',
            [
            'type' => 'confirmLogin',
            'content' => '
                Подтверждение входа
                <br><br>
                <span style="color:#3bcdb0; font-size:18px; font-weight:bold;">Здравствуйте!</span><br><br>
                Была произведена попытка вход в Ваш аккаунт.<br><br>
                Ваш код подтверждения входа {{key}}.
            ',
            'language' => 'ru_RU',
            'headers' => 'Вход в аккаунт'
            ]);
    }

    public function down()
    {
        echo "m170929_060654_mail_template_step_login cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170929_060654_mail_template_step_login cannot be reverted.\n";

        return false;
    }
    */
}
