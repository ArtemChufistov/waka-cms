<?php

use yii\db\Migration;

class m170717_034031_create_user extends Migration
{
    public function up()
    {
        $user = new \common\models\User();
        $user->username = 'admin';
        $user->email = 'admin@test.com';
        $user->setPassword('admin');
        $user->generateAuthKey();
        $user->save();
        $user_role = \Yii::$app->authManager->getRole('root');
        \Yii::$app->authManager->assign($user_role, $user->id);
    }

    public function down()
    {

        return false;
    }
}
