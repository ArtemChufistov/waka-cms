<?php

use yii\db\Migration;

class m170925_114106_create_user_log extends Migration
{
    public function up()
    {
        $this->createTable('user_log', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'type' => $this->integer(),
            'additional_info' => $this->text(),
            'comments' => $this->text(),
            "created_at" => $this->integer(),
            "updated_at" => $this->integer(),
        ]);

        $this->addForeignKey('FK_user_user_log', 'user_log', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('user_log');
    }
}
