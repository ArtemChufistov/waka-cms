<?php

use yii\db\Migration;

class m170825_042410_mail_sender extends Migration {
    public function up() {
        $this->createTable("mail", [
            "id" => $this->primaryKey(),
            "from" => $this->string(),
            "to" => $this->string(),
            "headers" => $this->string(),
            "title" => $this->text(),
            "content" => $this->text(),
            "status" => $this->smallInteger(),
            "error" => $this->string(),
            "sender_at" => $this->integer(),
            "created_at" => $this->integer(),
            "updated_at" => $this->integer(),
        ]);

        $this->createTable("mail_template", [
            "id" => $this->primaryKey(),
            "type" => $this->string(),
            "language" => $this->string(),
            "headers" => $this->text(),
            "title" => $this->text(),
            "content" => $this->text(),
        ]);

    }

    public function down() {
        $this->dropTable("mail_template");
        $this->dropTable("mail");
    }

}
