<?php

use yii\db\Migration;

class m170607_143646_add_bearer extends Migration
{
    public function up()
    {
        $this->createTable('{{%user_token_bearer}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'token' => $this->string(64)->notNull(),
            'expire_at' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey('FK_user_token_bearer_user_id', 'user_token_bearer', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('UK_user_token_bearer_token', 'user_token_bearer', ['user_id', 'token'], true);
    }

    public function down()
    {
        $this->dropTable('{{%user_token_bearer}}');
    }
}
