<?php

use yii\db\Migration;

class m171005_091535_user_ip_agent extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user_token_bearer}}', 'ip', $this->string()->after('token'));
        $this->addColumn('{{%user_token_bearer}}', 'agent', $this->string()->after('ip'));
    }

    public function down()
    {
        $this->dropColumn('{{%user_token_bearer}}', 'ip');
        $this->dropColumn('{{%user_token_bearer}}', 'agent');
    }
}
