<?php

use yii\db\Migration;

class m170720_041331_rbac_users_backend extends Migration
{
    public function up() {

        $index = Yii::$app->authManager->createPermission('/users/index');
        $view = Yii::$app->authManager->createPermission('/users/view');
        $create = Yii::$app->authManager->createPermission('/users/create');
        $update = Yii::$app->authManager->createPermission('/users/update');
        $delete = Yii::$app->authManager->createPermission('/users/delete');
        $search = Yii::$app->authManager->createPermission('/users/search');

        Yii::$app->authManager->add($index);
        Yii::$app->authManager->add($view);
        Yii::$app->authManager->add($create);
        Yii::$app->authManager->add($update);
        Yii::$app->authManager->add($delete);
        Yii::$app->authManager->add($search);

        $guest = Yii::$app->authManager->getRole('guest');
        $user = Yii::$app->authManager->getRole('user');
        $manager = Yii::$app->authManager->getRole('manager');
        $admin = Yii::$app->authManager->getRole('admin');
        $root = Yii::$app->authManager->getRole('root');

        Yii::$app->authManager->addChild($manager, $index);
        Yii::$app->authManager->addChild($manager, $view);
        Yii::$app->authManager->addChild($manager, $create);
        Yii::$app->authManager->addChild($manager, $update);
        Yii::$app->authManager->addChild($admin, $delete);
        Yii::$app->authManager->addChild($manager, $search);
    }

    public function down() {
        $index = Yii::$app->authManager->getPermission('/users/index');
        $view = Yii::$app->authManager->getPermission('/users/view');
        $create = Yii::$app->authManager->getPermission('/users/create');
        $update = Yii::$app->authManager->getPermission('/users/update');
        $delete = Yii::$app->authManager->getPermission('/users/delete');
        $search = Yii::$app->authManager->getPermission('/users/search');

        $manager = Yii::$app->authManager->getRole('manager');
        $admin = Yii::$app->authManager->getRole('admin');
        Yii::$app->authManager->removeChild($manager, $index);
        Yii::$app->authManager->removeChild($manager, $view);
        Yii::$app->authManager->removeChild($manager, $create);
        Yii::$app->authManager->removeChild($manager, $update);
        Yii::$app->authManager->removeChild($admin, $delete);
        Yii::$app->authManager->removeChild($manager, $search);

        Yii::$app->getAuthManager()->remove($index);
        Yii::$app->getAuthManager()->remove($view);
        Yii::$app->getAuthManager()->remove($create);
        Yii::$app->getAuthManager()->remove($update);
        Yii::$app->getAuthManager()->remove($delete);
        Yii::$app->getAuthManager()->remove($search);
    }
}
