<?php
use yii\db\Schema;
use yii\db\Migration;

class m170830_063654_AddPreferenceTable extends Migration
{
    public function up()
    {
        $this->createTable('{{%preference}}', [
            'id' => Schema::TYPE_PK,
            'key' => Schema::TYPE_STRING.'(255) NOT NULL',
            'value' => Schema::TYPE_STRING.'(255) NOT NULL',
        ]);
    }

    public function down()
    {
        echo "m170830_063654_AddPreferenceTable cannot be reverted.\n";

        return false;
    }
}
