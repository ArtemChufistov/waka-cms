<?php

use yii\db\Migration;

class m170902_110116_addCrontabTables extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%job}}', [
            'id' => $this->primaryKey(),
            'type' => $this->string(),
            'work' => $this->text(),
            'type' => $this->string()
        ]);

        $this->createTable('{{%cron_log}}', [
            'id' => $this->primaryKey(),
            'job_id' => $this->integer(),
            'work' => $this->integer(),
            'date_end' => $this->dateTime(),
            'date_start' => $this->dateTime(),
            'success' => $this->integer(),
            'addition_info' => $this->text()
        ]);
    }

    public function safeDown()
    {
        echo "m170902_110116_addCrontabTables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170902_110116_addCrontabTables cannot be reverted.\n";

        return false;
    }
    */
}
