<?php

use yii\db\Migration;

/**
 * Class m190923_053418_addCurrencyToToChaert
 */
class m190923_053418_addCurrencyToToChaert extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('{{%course_chart}}', 'currency_id_from', $this->integer());
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m190923_053418_addCurrencyToToChaert cannot be reverted.\n";

		return false;
	}

	/*
		    // Use up()/down() to run migration code without a transaction.
		    public function up()
		    {

		    }

		    public function down()
		    {
		        echo "m190923_053418_addCurrencyToToChaert cannot be reverted.\n";

		        return false;
		    }
	*/
}
