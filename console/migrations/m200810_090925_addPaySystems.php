<?php

use yii\db\Migration;

/**
 * Class m200810_090925_addPaySystems
 */
class m200810_090925_addPaySystems extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $paysys = new \common\modules\changer\models\Paysys;
        $paysys->key = \common\modules\changer\models\Paysys::KEY_ADVCASH;
        $paysys->title = 'Advanced Cash';
        $paysys->save(false);

        $paysys = new \common\modules\changer\models\Paysys;
        $paysys->key = \common\modules\changer\models\Paysys::KEY_PM;
        $paysys->title = 'Perfect Money';
        $paysys->save(false);

        $paysys = new \common\modules\changer\models\Paysys;
        $paysys->key = \common\modules\changer\models\Paysys::KEY_PAYEER;
        $paysys->title = 'Payeer';
        $paysys->save(false);

        $paysys = new \common\modules\changer\models\Paysys;
        $paysys->key = \common\modules\changer\models\Paysys::KEY_SBERBANK;
        $paysys->title = 'Sberbank';
        $paysys->save(false);

        $paysys = new \common\modules\changer\models\Paysys;
        $paysys->key = \common\modules\changer\models\Paysys::KEY_YAM;
        $paysys->title = 'Yandex Money';
        $paysys->save(false);

        $paysys = new \common\modules\changer\models\Paysys;
        $paysys->key = \common\modules\changer\models\Paysys::KEY_EXMO;
        $paysys->title = 'Exmo';
        $paysys->save(false);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200810_090925_addPaySystems cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200810_090925_addPaySystems cannot be reverted.\n";

        return false;
    }
    */
}
