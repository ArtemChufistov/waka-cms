<?php

use yii\db\Migration;

/**
 * Class m200630_035814_addChangerFieldsAndData
 */
class m200630_035814_addChangerFieldsAndData extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%changer_paysys}}', 'active', $this->integer()->defaultValue(1)->after('title'));
        $this->addColumn('{{%changer_course}}', 'active', $this->integer()->defaultValue(1)->after('course'));
        $this->addColumn('{{%changer_currency}}', 'active', $this->integer()->defaultValue(1)->after('title'));
        $this->addColumn('{{%changer_wallet}}', 'active', $this->integer()->defaultValue(1)->after('secret2'));

        foreach(\common\modules\changer\models\Currency::getKeyTitles() as $key => $title) {
            $paysys = new \common\modules\changer\models\Currency;
            $paysys->key = $key;
            $paysys->title = $title;
            $paysys->save(false);
        }

        foreach(\common\modules\changer\models\Currency::find()->all() as $currencyFrom) {
            foreach(\common\modules\changer\models\Currency::find()->all() as $currencyTo) {
                $course = new \common\modules\changer\models\Course;
                $course->currency_id_from = $currencyFrom->id;
                $course->currency_id_to = $currencyTo->id;
                $course->course = rand(1, 100);

                if ($course->currency_id_from == $course->currency_id_to) {
                    $course->active = false;
                }

                $course->save(false);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200630_035814_addChangerFieldsAndData cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200630_035814_addChangerFieldsAndData cannot be reverted.\n";

        return false;
    }
    */
}
