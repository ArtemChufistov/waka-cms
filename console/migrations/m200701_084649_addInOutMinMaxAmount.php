<?php

use yii\db\Migration;

/**
 * Class m200701_084649_addInOutMinMaxAmount
 */
class m200701_084649_addInOutMinMaxAmount extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%changer_currency}}', 'in', $this->float()->defaultValue(1)->after('title'));
        $this->addColumn('{{%changer_currency}}', 'out', $this->float()->defaultValue(90)->after('in'));
        $this->addColumn('{{%changer_currency}}', 'minamount', $this->float()->defaultValue(5)->after('out'));
        $this->addColumn('{{%changer_currency}}', 'maxamount', $this->float()->defaultValue(70)->after('minamount'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200701_084649_addInOutMinMaxAmount cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200701_084649_addInOutMinMaxAmount cannot be reverted.\n";

        return false;
    }
    */
}
