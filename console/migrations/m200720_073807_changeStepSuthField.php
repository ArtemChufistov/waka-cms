<?php

use yii\db\Migration;

/**
 * Class m200720_073807_changeStepSuthField
 */
class m200720_073807_changeStepSuthField extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%user}}', 'stepAuth');
        $this->dropColumn('{{%user}}', 'firstName');
        $this->dropColumn('{{%user}}', 'lastName');
        $this->addColumn('{{%user}}', 'step_auth', $this->integer()->defaultValue(1)->after('sms_confirmation_code'));
        $this->addColumn('{{%user}}', 'first_name', $this->string(255)->after('country'));
        $this->addColumn('{{%user}}', 'last_name', $this->string(255)->after('first_name'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200720_073807_changeStepSuthField cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200720_073807_changeStepSuthField cannot be reverted.\n";

        return false;
    }
    */
}
