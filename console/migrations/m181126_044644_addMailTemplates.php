<?php

use yii\db\Migration;

/**
 * Class m181126_044644_addMailTemplates
 */
class m181126_044644_addMailTemplates extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('mail_template',
            ['type', 'content', 'language', 'headers'], [
            ['innerTransferSuccess', '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,">
    <title>Evapay</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
    <style type="text/css">
        html {
            width: 100%;
        }
        body {
            margin: 0;
            padding: 0;
            width: 100%;
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
        }
        img {
            display: block !important;
            border: 0;
            -ms-interpolation-mode: bicubic;
        }
        .ReadMsgBody {
            width: 100%;
        }
        .ExternalClass {
            width: 100%;
        }
        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }
        .images {
            display: block !important;
            width: 100% !important;
        }
        .Heading {
            font-family: \'Open Sans\', Arial, Helvetica Neue, Helvetica, sans-serif !important;
        }
        .MsoNormal {
            font-family: \'Open Sans\', Arial, Helvetica Neue, Helvetica, sans-serif !important;
        }
        p {
            margin: 0 !important;
            padding: 0 !important;
        }
        a {
            font-family: \'Open Sans\', Arial, Helvetica Neue, Helvetica, sans-serif !important;
        }
        .display-button td,
        .display-button a {
            font-family: \'Open Sans\', Arial, Helvetica Neue, Helvetica, sans-serif !important;
        }
        .display-button a:hover {
            text-decoration: none !important;
        }
        /* MEDIA QUIRES */  

            @media only screen and (max-width:680px)
            {
                .display-width-main {width:100% !important;}
            }

            @media only screen and (max-width:639px)
            {
                body {width:auto !important;}
                .display-width {width:100% !important;} 
                .res-padding { padding:0 20px !important; }
                .text-center{ text-align: center !important;  }
            }
            @media only screen and (max-width:600px)
            {
                .image600x350{ background-position: center !important; }
            }
            @media only screen and (max-width:480px)
            {
                .display-width table {width:100% !important;}
                .display-width .button-width .display-button {width:auto !important;}
                .img-res{ width:100% !important; height: auto !important;  }
            }
        </style>
        <!--[if mso]>
            <style>
                .Heading{font-family: Arial, Helvetica Neue, Helvetica, sans-serif !important;}
                .MsoNormal{font-family: Arial, Helvetica Neue, Helvetica, sans-serif !important;}
            </style>
        <![endif]-->
</head>

<body>
    <repeater>
        <layout label=\'VIEW IN BROWSER STARTS\'>
            <table align="center" bgcolor="#2887b3" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                        <td height="30" style="line-height:30px;"></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table align="center" border="0" bgcolor="#2887b3" cellpadding="0" cellspacing="0" class="display-width-main" width="680">
                                <tbody>
                                    <tr>
                                        <td height="20" style="line-height:20px;"></td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="res-padding">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width" width="600">
                                                <tbody>
                                                    <tr>
                                                        <td align="right" class="MsoNormal" style="color:#ffffff; font-family:\'Segoe UI\', Arial, Verdana, Trebuchet MS, sans-serif; font-size:15px; font-weight:400; letter-spacing:1px;">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20" style="line-height:20px;"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </layout>
    </repeater>
    <repeater>
        <layout label=\'HEADER MENU STARTS\'>
            <table align="center" bgcolor="#2887b3" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center">
                        <table align="center" bgcolor="#ffffff" border="0" class="display-width-main" cellpadding="0" cellspacing="0" width="680" style="border-radius:5px 5px 0px 0px;">
                            <tr>
                                <td align="center">
                                    <table align="center" border="0" class="display-width" cellpadding="0" cellspacing="0" width="600" style="margin-top: 15px;">
                                        <tr>
                                            <td height="20" style="line-height:20px;"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <!--TABLE LEFT-->
                                                <table align="left" border="0" class="display-width" cellpadding="0" cellspacing="0" width="42%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:auto;">
                                                    <tr>
                                                        <td>
                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:auto !important;">
                                                                <tr>
                                                                    <td align="left" valign="middle" style="color:#666666; padding-top:3px;">
                                                                        <a href="#" style="color:#666666; text-decoration:none;">
                                                                            <img src="http://evapay.co/evapay/evapaylogo.png" alt="76x19" width="140" style="margin:0; border:0;" editable="true" label="76x19">
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="1" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                    <tbody>
                                                        <tr>
                                                            <td width="1" height="25" style="line-height: 25px;"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table align="right" border="0" class="display-width" cellpadding="0" cellspacing="0" width="44%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:auto;">
                                                    <tr>
                                                        <td align="center">
                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:auto !important;">
                                                                <tr>
                                                                    <td height="4" style="line-height:4px;"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" class="MsoNormal" style="color:#89a1b0; font-family:\'Segoe UI\', Arial, Verdana, Trebuchet MS, sans-serif; font-size:14px; font-weight:bold; letter-spacing:1px;">
                                                                        <multiline><a href="http://evapay.co" style="color:#2d86ae; text-decoration:none;">Перейти на сайт</a>
                                                                        </multiline>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="3" style="line-height:3px;"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="line-height:20px;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </layout>
    </repeater>
    <repeater>
        <layout label=\'HEADER BANNER STARTS\'>
            <table align="center" bgcolor="#2887b3" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                        <td align="center">
                            <div style="margin:auto;">
                                <table align="center" background="http://evapay.co/images/let-pic-1.png" border="0" cellpadding="0" cellspacing="0" class="display-width-main" width="680" style="background-image:url(http://evapay.co/images/let-pic-1.png); background-position:center; background-repeat:no-repeat;">
                                    <tbody>
                                        <tr>
                                            <td align="center" valign="top" style="color:#666666; line-height:0;">
                                                <img src="http://evapay.co/images/680x60x1.png" alt="680x60x1" width="680" height="60" style="margin:0; border:0; width:100%; height:auto;" editable="true" label="680x60x1">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <table align="center" border="0" class="display-width" cellpadding="0" cellspacing="0" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td height="15" style="line-height:20px;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" class="res-padding">
                                                                <table align="center" border="0" class="display-width" cellpadding="0" cellspacing="0" width="600">
                                                                    <tbody>
                                                                        <tr>
                                                                            <!--SECTION TITLE-->
                                                                            <td align="center" class="Heading" style="color:#ffffff; font-family:\'Segoe UI\', Arial, Verdana, Trebuchet MS, sans-serif; font-size:30px; font-weight: bold; text-transform:uppercase; letter-spacing:2px;">
                                                                                <multiline>ПЛАТЕЖНАЯ СИСТЕМА <span style="color:#f5c235;">БУДУЩЕГО</span>
                                                                                </multiline>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="30" style="line-height:30px;"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center" class="button-width">
                                                                                <!--BUTTON START-->
                                                                                <table align="right" border="0" cellpadding="0" cellspacing="0" class="display-button" style="border-radius:25px; border:1px solid #ffffff; margin-right: 50px;">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="center" valign="middle" class="MsoNormal" style="color:#ffffff; font-family:\'Segoe UI\', Arial, Verdana, Trebuchet MS, sans-serif; font-size:13px; font-weight:600; padding:7px 19px 7px 19px; text-transform:uppercase; border-radius:25px; letter-spacing:1px;">
                                                                                                <multiline><a href="http://evapay.co" style=" color:#ffffff; text-decoration:none;">Подробнее</a>
                                                                                                </multiline>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <!--BUTTON END-->
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="65" style="line-height:65px;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td height="65" style="line-height:65px;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td height="65" style="line-height:65px;"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="bottom" style="color:#666666; line-height:0;">
                                                <img src="http://evapay.co/images/680x60x2.png" alt="680x60x2" width="680" height="60" style="margin:0; border:0; width:100%; height:auto;" editable="true" label="680x60x2">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </layout>
    </repeater>

    <repeater>
        <layout label=\'WELCOME TO EVAPAY STARTS\'>
            <table align="center" bgcolor="#2887b3" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                        <td align="center">
                            <table align="center" border="0" bgcolor="#ffffff" cellpadding="0" cellspacing="0" class="display-width-main" width="680">
                                <tbody>
                                    <tr>
                                        <td align="center" class="res-padding">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width" width="600">
                                                <tbody>
                                                    <tr>
                                                        <td height="5" style="line-height: 5px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" class="Heading" style="color:#666666; font-family:Segoe UI, Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:26px; font-weight:bold; line-height:36px; letter-spacing:1px; text-transform:uppercase;">
                                                            <multiline>Инвойс отменён <span style="color:#f5c235;">Eva</span><span style="color:#337aa4;">pay</span></multiline>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="15" style="line-height: 15px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" class="Heading" style="color:#666666; font-family:Segoe UI, Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:18px; font-weight:bold; line-height:40px; letter-spacing:1px;" height="40" style="line-height:40px;">Здравствуйте!</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" class="Heading" style="color:#666666; font-family:Segoe UI, Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:16px;">Инвойс участника <span style="color:#3bcdb0; font-weight:bold; font-size: 17px;">{{fromEmail}}</span> выставленный Вам на сумму: <span style="color:#3bcdb0; font-weight:bold; font-size: 17px;">{{amount}} {{currency}}</span> был отменён</td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25" style="line-height: 25px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25" style="line-height: 25px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" class="Heading" style="color:#83a1b0; font-family:Segoe UI, Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:17px; font-weight:bold; line-height:36px; letter-spacing:1px;" height="40" style="line-height:40px;">Цифровые деньги для цифровой эпохи с приминением технологии Blockchain</td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25" style="line-height: 25px;">&nbsp;</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </layout>
    </repeater>
    <unsubscribe style="display:none;">Unsubscribe Here</unsubscribe>
</body>
</html>
            ', 'ru_RU', 'Иинвойс в платёжной системе EvaPay отменён'],
            ]);

        $this->batchInsert('mail_template',
            ['type', 'content', 'language', 'headers'], [
            ['TYPE_INVOICE_CANCELED_FROM_USER', '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,">
    <title>Evapay</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
    <style type="text/css">
        html {
            width: 100%;
        }
        body {
            margin: 0;
            padding: 0;
            width: 100%;
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
        }
        img {
            display: block !important;
            border: 0;
            -ms-interpolation-mode: bicubic;
        }
        .ReadMsgBody {
            width: 100%;
        }
        .ExternalClass {
            width: 100%;
        }
        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }
        .images {
            display: block !important;
            width: 100% !important;
        }
        .Heading {
            font-family: \'Open Sans\', Arial, Helvetica Neue, Helvetica, sans-serif !important;
        }
        .MsoNormal {
            font-family: \'Open Sans\', Arial, Helvetica Neue, Helvetica, sans-serif !important;
        }
        p {
            margin: 0 !important;
            padding: 0 !important;
        }
        a {
            font-family: \'Open Sans\', Arial, Helvetica Neue, Helvetica, sans-serif !important;
        }
        .display-button td,
        .display-button a {
            font-family: \'Open Sans\', Arial, Helvetica Neue, Helvetica, sans-serif !important;
        }
        .display-button a:hover {
            text-decoration: none !important;
        }
        /* MEDIA QUIRES */  

            @media only screen and (max-width:680px)
            {
                .display-width-main {width:100% !important;}
            }

            @media only screen and (max-width:639px)
            {
                body {width:auto !important;}
                .display-width {width:100% !important;} 
                .res-padding { padding:0 20px !important; }
                .text-center{ text-align: center !important;  }
            }
            @media only screen and (max-width:600px)
            {
                .image600x350{ background-position: center !important; }
            }
            @media only screen and (max-width:480px)
            {
                .display-width table {width:100% !important;}
                .display-width .button-width .display-button {width:auto !important;}
                .img-res{ width:100% !important; height: auto !important;  }
            }
        </style>
        <!--[if mso]>
            <style>
                .Heading{font-family: Arial, Helvetica Neue, Helvetica, sans-serif !important;}
                .MsoNormal{font-family: Arial, Helvetica Neue, Helvetica, sans-serif !important;}
            </style>
        <![endif]-->
</head>

<body>
    <repeater>
        <layout label=\'VIEW IN BROWSER STARTS\'>
            <table align="center" bgcolor="#2887b3" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                        <td height="30" style="line-height:30px;"></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table align="center" border="0" bgcolor="#2887b3" cellpadding="0" cellspacing="0" class="display-width-main" width="680">
                                <tbody>
                                    <tr>
                                        <td height="20" style="line-height:20px;"></td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="res-padding">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width" width="600">
                                                <tbody>
                                                    <tr>
                                                        <td align="right" class="MsoNormal" style="color:#ffffff; font-family:\'Segoe UI\', Arial, Verdana, Trebuchet MS, sans-serif; font-size:15px; font-weight:400; letter-spacing:1px;">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20" style="line-height:20px;"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </layout>
    </repeater>
    <repeater>
        <layout label=\'HEADER MENU STARTS\'>
            <table align="center" bgcolor="#2887b3" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center">
                        <table align="center" bgcolor="#ffffff" border="0" class="display-width-main" cellpadding="0" cellspacing="0" width="680" style="border-radius:5px 5px 0px 0px;">
                            <tr>
                                <td align="center">
                                    <table align="center" border="0" class="display-width" cellpadding="0" cellspacing="0" width="600" style="margin-top: 15px;">
                                        <tr>
                                            <td height="20" style="line-height:20px;"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <!--TABLE LEFT-->
                                                <table align="left" border="0" class="display-width" cellpadding="0" cellspacing="0" width="42%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:auto;">
                                                    <tr>
                                                        <td>
                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:auto !important;">
                                                                <tr>
                                                                    <td align="left" valign="middle" style="color:#666666; padding-top:3px;">
                                                                        <a href="#" style="color:#666666; text-decoration:none;">
                                                                            <img src="http://evapay.co/evapay/evapaylogo.png" alt="76x19" width="140" style="margin:0; border:0;" editable="true" label="76x19">
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="1" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                    <tbody>
                                                        <tr>
                                                            <td width="1" height="25" style="line-height: 25px;"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table align="right" border="0" class="display-width" cellpadding="0" cellspacing="0" width="44%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:auto;">
                                                    <tr>
                                                        <td align="center">
                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:auto !important;">
                                                                <tr>
                                                                    <td height="4" style="line-height:4px;"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" class="MsoNormal" style="color:#89a1b0; font-family:\'Segoe UI\', Arial, Verdana, Trebuchet MS, sans-serif; font-size:14px; font-weight:bold; letter-spacing:1px;">
                                                                        <multiline><a href="http://evapay.co" style="color:#2d86ae; text-decoration:none;">Перейти на сайт</a>
                                                                        </multiline>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="3" style="line-height:3px;"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="line-height:20px;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </layout>
    </repeater>
    <repeater>
        <layout label=\'HEADER BANNER STARTS\'>
            <table align="center" bgcolor="#2887b3" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                        <td align="center">
                            <div style="margin:auto;">
                                <table align="center" background="http://evapay.co/images/let-pic-1.png" border="0" cellpadding="0" cellspacing="0" class="display-width-main" width="680" style="background-image:url(http://evapay.co/images/let-pic-1.png); background-position:center; background-repeat:no-repeat;">
                                    <tbody>
                                        <tr>
                                            <td align="center" valign="top" style="color:#666666; line-height:0;">
                                                <img src="http://evapay.co/images/680x60x1.png" alt="680x60x1" width="680" height="60" style="margin:0; border:0; width:100%; height:auto;" editable="true" label="680x60x1">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <table align="center" border="0" class="display-width" cellpadding="0" cellspacing="0" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td height="15" style="line-height:20px;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" class="res-padding">
                                                                <table align="center" border="0" class="display-width" cellpadding="0" cellspacing="0" width="600">
                                                                    <tbody>
                                                                        <tr>
                                                                            <!--SECTION TITLE-->
                                                                            <td align="center" class="Heading" style="color:#ffffff; font-family:\'Segoe UI\', Arial, Verdana, Trebuchet MS, sans-serif; font-size:30px; font-weight: bold; text-transform:uppercase; letter-spacing:2px;">
                                                                                <multiline>ПЛАТЕЖНАЯ СИСТЕМА <span style="color:#f5c235;">БУДУЩЕГО</span>
                                                                                </multiline>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="30" style="line-height:30px;"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center" class="button-width">
                                                                                <!--BUTTON START-->
                                                                                <table align="right" border="0" cellpadding="0" cellspacing="0" class="display-button" style="border-radius:25px; border:1px solid #ffffff; margin-right: 50px;">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="center" valign="middle" class="MsoNormal" style="color:#ffffff; font-family:\'Segoe UI\', Arial, Verdana, Trebuchet MS, sans-serif; font-size:13px; font-weight:600; padding:7px 19px 7px 19px; text-transform:uppercase; border-radius:25px; letter-spacing:1px;">
                                                                                                <multiline><a href="http://evapay.co" style=" color:#ffffff; text-decoration:none;">Подробнее</a>
                                                                                                </multiline>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <!--BUTTON END-->
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="65" style="line-height:65px;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td height="65" style="line-height:65px;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td height="65" style="line-height:65px;"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="bottom" style="color:#666666; line-height:0;">
                                                <img src="http://evapay.co/images/680x60x2.png" alt="680x60x2" width="680" height="60" style="margin:0; border:0; width:100%; height:auto;" editable="true" label="680x60x2">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </layout>
    </repeater>

    <repeater>
        <layout label=\'WELCOME TO EVAPAY STARTS\'>
            <table align="center" bgcolor="#2887b3" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                        <td align="center">
                            <table align="center" border="0" bgcolor="#ffffff" cellpadding="0" cellspacing="0" class="display-width-main" width="680">
                                <tbody>
                                    <tr>
                                        <td align="center" class="res-padding">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width" width="600">
                                                <tbody>
                                                    <tr>
                                                        <td height="5" style="line-height: 5px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" class="Heading" style="color:#666666; font-family:Segoe UI, Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:26px; font-weight:bold; line-height:36px; letter-spacing:1px; text-transform:uppercase;">
                                                            <multiline>Инвойс отменён <span style="color:#f5c235;">Eva</span><span style="color:#337aa4;">pay</span></multiline>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="15" style="line-height: 15px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" class="Heading" style="color:#666666; font-family:Segoe UI, Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:18px; font-weight:bold; line-height:40px; letter-spacing:1px;" height="40" style="line-height:40px;">Здравствуйте!</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" class="Heading" style="color:#666666; font-family:Segoe UI, Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:16px;">Ваш инвойс участнику <span style="color:#3bcdb0; font-weight:bold; font-size: 17px;">{{email}}</span> выставленный на сумму: <span style="color:#3bcdb0; font-weight:bold; font-size: 17px;">{{amount}} {{currency}}</span> был отменён</td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25" style="line-height: 25px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25" style="line-height: 25px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" class="Heading" style="color:#83a1b0; font-family:Segoe UI, Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:17px; font-weight:bold; line-height:36px; letter-spacing:1px;" height="40" style="line-height:40px;">Цифровые деньги для цифровой эпохи с приминением технологии Blockchain</td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25" style="line-height: 25px;">&nbsp;</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </layout>
    </repeater>
    <unsubscribe style="display:none;">Unsubscribe Here</unsubscribe>
</body>
</html>
            ', 'ru_RU', 'Иинвойс в платёжной системе EvaPay отменён'],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181126_044644_addMailTemplates cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181126_044644_addMailTemplates cannot be reverted.\n";

        return false;
    }
    */
}
