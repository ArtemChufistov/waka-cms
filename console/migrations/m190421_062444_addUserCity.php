<?php

use yii\db\Migration;

/**
 * Class m190421_062444_addUserCity
 */
class m190421_062444_addUserCity extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'city', $this->string(255)->defaultValue(NULL)->after('username'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190421_062444_addUserCity cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190421_062444_addUserCity cannot be reverted.\n";

        return false;
    }
    */
}
