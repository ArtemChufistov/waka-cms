<?php

use yii\db\Migration;

/**
 * Class m190817_130256_addActiveSortCurrency
 */
class m190817_130256_addActiveSortCurrency extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%currency}}', 'active', $this->integer(1)->defaultValue(1)->after('course_to_usd'));
        $this->addColumn('{{%currency}}', 'sort', $this->integer()->defaultValue(NULL)->after('active'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190817_130256_addActiveSortCurrency cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190817_130256_addActiveSortCurrency cannot be reverted.\n";

        return false;
    }
    */
}
