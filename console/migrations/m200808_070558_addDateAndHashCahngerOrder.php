<?php

use yii\db\Migration;

/**
 * Class m200808_070558_addDateAndHashCahngerOrder
 */
class m200808_070558_addDateAndHashCahngerOrder extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%changer_order}}', 'hash', $this->string(255)->after('phone'));
        $this->addColumn('{{%changer_order}}', 'date_add', $this->dateTime()->after('hash'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200808_070558_addDateAndHashCahngerOrder cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200808_070558_addDateAndHashCahngerOrder cannot be reverted.\n";

        return false;
    }
    */
}
