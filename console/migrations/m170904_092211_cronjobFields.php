<?php

use yii\db\Migration;

class m170904_092211_cronjobFields extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%job}}', 'key', $this->string());
        $this->addColumn('{{%job}}', 'frequency', $this->integer());
        $this->addColumn('{{%job}}', 'last_date', $this->dateTime());
        $this->addColumn('{{%job}}', 'active', $this->integer());
        $this->addColumn('{{%job}}', 'cron_num', $this->integer());
    }

    public function safeDown()
    {
        echo "m170904_092211_cronjobFields cannot be reverted.\n";

        return false;
    }
}
