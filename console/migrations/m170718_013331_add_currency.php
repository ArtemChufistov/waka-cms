<?php

use yii\db\Migration;

class m170718_013331_add_currency extends Migration
{
    public function up()
    {
        $this->createTable('currency',[
            'id' => $this->primaryKey(),
            'title' => $this->string(64)->notNull(),
            'key' => $this->string(64)->notNull(),
            'course_to_usd' => $this->string(64)->defaultValue(1),
            'course_updated_at' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('currency');
    }
}
