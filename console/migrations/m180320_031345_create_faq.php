<?php

use yii\db\Migration;

class m180320_031345_create_faq extends Migration
{
    public function safeUp()
    {

        $this->createTable('ticket_faq', [
            'id' => $this->primaryKey()->unsigned(),
            'type' => $this->smallInteger(),
            'title' => $this->string(),
            'message' => $this->text(),
        ]);

    }

    public function safeDown()
    {
        $this->dropTable('ticket_faq');
    }

}
