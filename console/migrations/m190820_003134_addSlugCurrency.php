<?php

use yii\db\Migration;

/**
 * Class m190820_003134_addSlugCurrency
 */
class m190820_003134_addSlugCurrency extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%currency}}', 'slug', $this->string('20')->defaultValue(NULL)->after('key'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190820_003134_addSlugCurrency cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190820_003134_addSlugCurrency cannot be reverted.\n";

        return false;
    }
    */
}
