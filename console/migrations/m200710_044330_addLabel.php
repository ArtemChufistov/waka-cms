<?php

use yii\db\Migration;

/**
 * Class m200710_044330_addLabel
 */
class m200710_044330_addLabel extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%changer_currency}}', 'label', $this->string()->defaultValue('USD')->after('title'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200710_044330_addLabel cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200710_044330_addLabel cannot be reverted.\n";

        return false;
    }
    */
}
