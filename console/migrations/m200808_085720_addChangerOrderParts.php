<?php

use yii\db\Migration;

/**
 * Class m200808_085720_addChangerOrderParts
 */
class m200808_085720_addChangerOrderParts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%changer_order}}', 'count_parts', $this->integer()->after('status'));
        $this->addColumn('{{%changer_order}}', 'time_for_pay', $this->integer()->after('hash'));
        $this->addColumn('{{%changer_order}}', 'real_amount_from', $this->float()->after('amount_from'));

        $this->createTable('changer_order_part', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'wallet_id' => $this->integer(),
            'amount' => $this->float(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200808_085720_addChangerOrderParts cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200808_085720_addChangerOrderParts cannot be reverted.\n";

        return false;
    }
    */
}
