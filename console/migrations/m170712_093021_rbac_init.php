<?php

use yii\db\Migration;
use console\controllers\RbacController;

class m170712_093021_rbac_init extends Migration
{
    public function up()
    {
        //create roles
        $guest = Yii::$app->authManager->createRole('guest');
        $user = Yii::$app->authManager->createRole('user');
        $manager = Yii::$app->authManager->createRole('manager');
        $admin = Yii::$app->authManager->createRole('admin');
        $root = Yii::$app->authManager->createRole('root');

        // Add roles in Yii::$app->authManager
        Yii::$app->authManager->add($guest);
        Yii::$app->authManager->add($user);
        Yii::$app->authManager->add($manager);
        Yii::$app->authManager->add($admin);
        Yii::$app->authManager->add($root);

        // Create simple, based on action{$NAME} permissions
        $login  = Yii::$app->authManager->createPermission('login');
        $logout = Yii::$app->authManager->createPermission('logout');
        $error  = Yii::$app->authManager->createPermission('error');
        $signUp = Yii::$app->authManager->createPermission('sign-up');

        // Add permissions in Yii::$app->authManager
        Yii::$app->authManager->add($login);
        Yii::$app->authManager->add($logout);
        Yii::$app->authManager->add($error);
        Yii::$app->authManager->add($signUp);


        // Add permission-per-role in Yii::$app->authManager
        // Guest
        Yii::$app->authManager->addChild($guest, $login);
        Yii::$app->authManager->addChild($guest, $logout);
        Yii::$app->authManager->addChild($guest, $error);
        Yii::$app->authManager->addChild($guest, $signUp);

        // USER
        Yii::$app->authManager->addChild($user, $guest);

        // MANAGER
        Yii::$app->authManager->addChild($manager, $guest);

        // Admin
        Yii::$app->authManager->addChild($admin, $user);
        Yii::$app->authManager->addChild($admin, $manager);

        // Root
        Yii::$app->authManager->addChild($root, $user);
        Yii::$app->authManager->addChild($root, $manager);
        Yii::$app->authManager->addChild($root, $admin);

    }

    public function down()
    {
        Yii::$app->authManager->removeAll();
    }

}
