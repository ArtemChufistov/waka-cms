<?php

use yii\db\Migration;

class m171017_113058_tickets extends Migration
{

    public function up()
    {
        $this->createTable('tickets', [
            'id' => $this->primaryKey()->unsigned(),
            'user_id' => $this->integer(11)->notNull(),
            'type' => $this->smallInteger(),
            'title' => $this->string(),
            "created_at" => $this->integer(),
            "updated_at" => $this->integer(),
        ]);

        $this->createTable('ticket_items', [
            'id' => $this->primaryKey()->unsigned(),
            'ticket_id' => $this->integer(11)->unsigned()->notNull(),
            'user_id' => $this->integer(11)->notNull(),
            'message' => $this->text(),
            'is_visible' => $this->smallInteger()->defaultValue(0),
            'is_visible_admin' => $this->smallInteger()->defaultValue(0),
            'is_file' => $this->smallInteger()->defaultValue(0),
            "created_at" => $this->integer(),
            "updated_at" => $this->integer(),
        ]);

        $this->addForeignKey('FK_user_tickets', 'tickets', 'user_id', 'user', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('FK_user_ticket_items', 'ticket_items', 'user_id', 'user', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('FK_tickets_ticket_items', 'ticket_items', 'ticket_id', 'tickets', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('ticket_items');
        $this->dropTable('tickets');
    }

}
