<?php

use yii\db\Migration;

class m171010_044046_add_documents extends Migration
{

    public function up()
    {
        $this->createTable('document', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'type' => $this->smallInteger()->unsigned()->notNull(),
            'comment' => $this->text()->null(),
            'verify' => $this->smallInteger()->unsigned()->notNull()->defaultValue(0),
            'is_visible' => $this->smallInteger()->unsigned()->notNull()->defaultValue(0),
            "created_at" => $this->integer(),
            "updated_at" => $this->integer(),
        ]);

        $this->addForeignKey('FK_user_document', 'document', 'user_id', 'user', 'id', 'RESTRICT', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('document');
    }

}
