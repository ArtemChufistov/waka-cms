<?php

use yii\db\Migration;

/**
 * Class m190913_093602_addStepAuthStepMethod
 */
class m190913_093602_addStepAuthStepMethod extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('{{%user}}', 'send_auth', $this->string('20')->defaultValue(0)->after('step_method'));
		$this->addColumn('{{%user}}', 'send_method', $this->string('20')->defaultValue(0)->after('send_auth'));
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m190913_093602_addStepAuthStepMethod cannot be reverted.\n";

		return false;
	}

	/*
		    // Use up()/down() to run migration code without a transaction.
		    public function up()
		    {

		    }

		    public function down()
		    {
		        echo "m190913_093602_addStepAuthStepMethod cannot be reverted.\n";

		        return false;
		    }
	*/
}
