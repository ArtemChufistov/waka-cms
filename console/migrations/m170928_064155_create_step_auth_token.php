<?php

use yii\db\Migration;

class m170928_064155_create_step_auth_token extends Migration
{
    public function up()
    {
        $this->createTable('user_step_token', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'token' => $this->string(32)->notNull(),
            'key' => $this->string(32)->notNull(),
            "created_at" => $this->integer(),
            "updated_at" => $this->integer(),
        ]);

        $this->addForeignKey('FK_user_user_step_token', 'user_step_token', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('user_step_token');
    }
}
