<?php

use yii\db\Migration;

/**
 * Class m180921_182258_add_confirm_code_column
 */
class m180921_182258_add_confirm_code_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'email_confirmation_code', $this->integer()->after('email'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'email_confirmation_code');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180921_182258_add_confirm_code_column cannot be reverted.\n";

        return false;
    }
    */
}
