<?php

use yii\db\Migration;

/**
 * Class m181214_153251_addRefStatTable
 */
class m181214_153251_addRefStatTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%go_ref}}', [
            'id' => $this->primaryKey(),
            'ref' => $this->string(),
            'date' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181214_153251_addRefStatTable cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181214_153251_addRefStatTable cannot be reverted.\n";

        return false;
    }
    */
}
