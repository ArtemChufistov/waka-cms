<?php

use yii\db\Migration;

/**
 * Class m190421_062817_addUserCityAddress
 */
class m190421_062817_addUserCityAddress extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'city_address', $this->string(255)->defaultValue(NULL)->after('city'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190421_062817_addUserCityAddress cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190421_062817_addUserCityAddress cannot be reverted.\n";

        return false;
    }
    */
}
