<?php

use yii\db\Migration;

class m180323_071557_add_news extends Migration
{
    public function safeUp()
    {
        $this->createTable('news_category',[
            'id' => $this->primaryKey()->unsigned(),
            'title' => $this->string(255)->notNull(),
        ]);

        $this->createTable('news', [
            'id' => $this->primaryKey()->unsigned(),
            'category_id' => $this->integer()->unsigned()->notNull(),
            'title' => $this->string(255)->notNull(),
            'small_text' => $this->text()->notNull(),
            'content' => $this->text()->notNull(),
            'status' => $this->smallInteger()->unsigned()->notNull(),
            'created_at' => $this->integer()->unsigned()->notNull(),
            'updated_at' => $this->integer()->unsigned()->notNull(),
        ]);

        $this->addForeignKey('FK_news_news_category', 'news', 'category_id', 'news_category', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('news');
        $this->dropTable('news_category');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180323_071557_add_news cannot be reverted.\n";

        return false;
    }
    */
}
