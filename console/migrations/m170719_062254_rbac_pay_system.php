<?php

use yii\db\Migration;

class m170719_062254_rbac_pay_system extends Migration
{
    public function up() {

        $index = Yii::$app->authManager->createPermission('/pay-system/index');
        $view = Yii::$app->authManager->createPermission('/pay-system/view');
        $create = Yii::$app->authManager->createPermission('/pay-system/create');
        $update = Yii::$app->authManager->createPermission('/pay-system/update');
        $delete = Yii::$app->authManager->createPermission('/pay-system/delete');

        Yii::$app->authManager->add($index);
        Yii::$app->authManager->add($view);
        Yii::$app->authManager->add($create);
        Yii::$app->authManager->add($update);
        Yii::$app->authManager->add($delete);

        $guest = Yii::$app->authManager->getRole('guest');
        $user = Yii::$app->authManager->getRole('user');
        $manager = Yii::$app->authManager->getRole('manager');
        $admin = Yii::$app->authManager->getRole('admin');
        $root = Yii::$app->authManager->getRole('root');

        Yii::$app->authManager->addChild($manager, $index);
        Yii::$app->authManager->addChild($manager, $view);
        Yii::$app->authManager->addChild($manager, $create);
        Yii::$app->authManager->addChild($manager, $update);
        Yii::$app->authManager->addChild($admin, $delete);
    }

    public function down() {
        $index = Yii::$app->authManager->getPermission('/pay-system/index');
        $view = Yii::$app->authManager->getPermission('/pay-system/view');
        $create = Yii::$app->authManager->getPermission('/pay-system/create');
        $update = Yii::$app->authManager->getPermission('/pay-system/update');
        $delete = Yii::$app->authManager->getPermission('/pay-system/delete');

        $manager = Yii::$app->authManager->getRole('manager');
        $admin = Yii::$app->authManager->getRole('admin');
        Yii::$app->authManager->removeChild($manager, $index);
        Yii::$app->authManager->removeChild($manager, $view);
        Yii::$app->authManager->removeChild($manager, $create);
        Yii::$app->authManager->removeChild($manager, $update);
        Yii::$app->authManager->removeChild($admin, $delete);

        Yii::$app->getAuthManager()->remove($index);
        Yii::$app->getAuthManager()->remove($view);
        Yii::$app->getAuthManager()->remove($create);
        Yii::$app->getAuthManager()->remove($update);
        Yii::$app->getAuthManager()->remove($delete);
    }
}
