<?php

use yii\db\Migration;

class m170608_025419_after_user extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'stepAuth', $this->integer()->notNull()->defaultValue(1)->after('email'));
        $this->addColumn('{{%user}}', 'lastName', $this->string(255)->after('username'));
        $this->addColumn('{{%user}}', 'firstName', $this->string(255)->after('username'));
        $this->addColumn('{{%user}}', 'country', $this->string(255)->after('username'));
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'stepAuth');
        $this->dropColumn('{{%user}}', 'lastName');
        $this->dropColumn('{{%user}}', 'firstName');
        $this->dropColumn('{{%user}}', 'country');
    }

}
