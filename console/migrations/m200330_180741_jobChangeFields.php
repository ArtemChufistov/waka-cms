<?php

use yii\db\Migration;

/**
 * Class m200330_180741_jobChangeFields
 */
class m200330_180741_jobChangeFields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%job}}', 'method', $this->string()->defaultValue(NULL)->after('id'));
        $this->dropColumn('{{%job}}', 'type');
        $this->dropColumn('{{%job}}', 'work');
        $this->dropColumn('{{%job}}', 'key');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200330_180741_jobChangeFields cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200330_180741_jobChangeFields cannot be reverted.\n";

        return false;
    }
    */
}
