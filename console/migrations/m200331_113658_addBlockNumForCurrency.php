<?php

use yii\db\Migration;

/**
 * Class m200331_113658_addBlockNumForCurrency
 */
class m200331_113658_addBlockNumForCurrency extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%currency}}', 'latest_block', $this->integer()->defaultValue(NULL)->after('sort'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200331_113658_addBlockNumForCurrency cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200331_113658_addBlockNumForCurrency cannot be reverted.\n";

        return false;
    }
    */
}
