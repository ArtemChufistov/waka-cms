<?php

use yii\db\Migration;

/**
 * Class m190913_114949_addGASECRET
 */
class m190913_114949_addGASECRET extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('{{%user}}', 'ga_secret', $this->string('100')->defaultValue(0)->after('send_method'));
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m190913_114949_addGASECRET cannot be reverted.\n";

		return false;
	}

	/*
		    // Use up()/down() to run migration code without a transaction.
		    public function up()
		    {

		    }

		    public function down()
		    {
		        echo "m190913_114949_addGASECRET cannot be reverted.\n";

		        return false;
		    }
	*/
}
