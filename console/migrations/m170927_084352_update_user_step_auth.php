<?php

use yii\db\Migration;

class m170927_084352_update_user_step_auth extends Migration
{
    public function up()
    {
        $this->addColumn('user','step_method', $this->smallInteger()->notNull()->defaultValue(0)->after('stepAuth'));
        $this->addColumn('user','phone', $this->string(32)->after('email'));
    }

    public function down()
    {
        $this->dropColumn('user','phone');
        $this->dropColumn('user','step_method');
    }
}
