<?php

use yii\db\Migration;

/**
 * Class m180914_001825_user_add_parent_id
 */
class m180914_001825_user_add_parent_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'parent_id', $this->integer()->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'parent_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180914_001825_user_add_parent_id cannot be reverted.\n";

        return false;
    }
    */
}
