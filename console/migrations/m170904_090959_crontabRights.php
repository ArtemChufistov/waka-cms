<?php

use yii\db\Migration;

class m170904_090959_crontabRights extends Migration
{
    public function safeUp()
    {
        $index = Yii::$app->authManager->createPermission('/crontab/cronlog/index');
        $view = Yii::$app->authManager->createPermission('/crontab/cronlog/view');
        $create = Yii::$app->authManager->createPermission('/crontab/cronlog/create');
        $update = Yii::$app->authManager->createPermission('/crontab/cronlog/update');
        $delete = Yii::$app->authManager->createPermission('/crontab/cronlog/delete');

        Yii::$app->authManager->add($index);
        Yii::$app->authManager->add($view);
        Yii::$app->authManager->add($create);
        Yii::$app->authManager->add($update);
        Yii::$app->authManager->add($delete);

        $admin = Yii::$app->authManager->getRole('admin');

        Yii::$app->authManager->addChild($admin, $index);
        Yii::$app->authManager->addChild($admin, $view);
        Yii::$app->authManager->addChild($admin, $create);
        Yii::$app->authManager->addChild($admin, $update);
        Yii::$app->authManager->addChild($admin, $delete);

        $index = Yii::$app->authManager->createPermission('/crontab/job/index');
        $view = Yii::$app->authManager->createPermission('/crontab/job/view');
        $create = Yii::$app->authManager->createPermission('/crontab/job/create');
        $update = Yii::$app->authManager->createPermission('/crontab/job/update');
        $delete = Yii::$app->authManager->createPermission('/crontab/job/delete');

        Yii::$app->authManager->add($index);
        Yii::$app->authManager->add($view);
        Yii::$app->authManager->add($create);
        Yii::$app->authManager->add($update);
        Yii::$app->authManager->add($delete);

        $admin = Yii::$app->authManager->getRole('admin');

        Yii::$app->authManager->addChild($admin, $index);
        Yii::$app->authManager->addChild($admin, $view);
        Yii::$app->authManager->addChild($admin, $create);
        Yii::$app->authManager->addChild($admin, $update);
        Yii::$app->authManager->addChild($admin, $delete);
    }

    public function safeDown()
    {
        echo "m170904_090959_crontabRights cannot be reverted.\n";

        return false;
    }
}
