<?php

use yii\db\Migration;

/**
 * Class m200703_130912_addPaysysAndCourse
 */
class m200703_130912_addPaysysAndCourse extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('changer_order', [
            'id' => $this->primaryKey(),
            'currency_id_from' => $this->integer(),
            'currency_id_to' => $this->integer(),
            'course' => $this->float(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200703_130912_addPaysysAndCourse cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200703_130912_addPaysysAndCourse cannot be reverted.\n";

        return false;
    }
    */
}
