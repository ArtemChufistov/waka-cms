<?php
return [
    'adminEmail' => 'admin@example.com',
    'noReplyEmail' => 'no-reply@eva.pay'
];
