<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "currency".
 *
 */
class Currency extends \yii\db\ActiveRecord {

    const KEY_BTC = 'BTC';
    const KEY_LTC = 'LTC';
    const KEY_BITCOIN_ABC = 'BCH';
    const KEY_BITCOIN_SV = 'BCHSV';
    const KEY_DOGECOIN = 'DOGE';
    const KEY_DASH = 'DASH';
    const KEY_ZCASH = 'ZEC';
    const KEY_BTC_GOLD = 'BTG';
    const KEY_ETH = 'ETH';
    const KEY_XMR = 'XMR';
    const KEY_XRP = 'XRP';

    const ACTIVE_TRUE = 1;
    const ACTIVE_FALSE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'currency';
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['key', 'title', 'slug'], 'string', 'max' => 255],
            [['course_to_usd', 'active', 'sort'], 'safe'],
            [['course_updated_at', 'created_at', 'updated_at', 'active', 'sort'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'title' => 'Title',
            'slug' => 'Slug',
            'course_to_usd' => 'Course to usd',
            'active' => 'Active',
            'sort' => 'Sort',
            'course_updated_at' => 'Course updated at',
            'created_at' => 'Created at',
            'updated_at' => 'Updated at',
        ];
    }
}
