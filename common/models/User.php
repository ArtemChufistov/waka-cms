<?php
namespace common\models;

use valentinek\behaviors\ClosureTable;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $email_confirmation_code
 * @property integer $refill
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $step_auth
 * @property integer $last_name
 * @property integer $first_name
 * @property integer $country
 * @property string $password write-only password
 * @property null|string $city
 * @property null|string $city_address
 *
 * @property \common\models\UserWallet $wallet
 */
class User extends ActiveRecord implements IdentityInterface {
	const STATUS_DELETED = 0;
	const STATUS_ACTIVE = 10;

	const STEP_METHOD_EMAIL = 'email';
	const STEP_METHOD_GOOGLE_AUTH = 'google_auth';
	const STEP_METHOD_PHONE = 'phone';

	const STEP_AUTH_FALSE = 0;
	const STEP_AUTH_TRUE = 1;

	const SEND_METHOD_EMAIL = 'email';
	const SEND_METHOD_GOOGLE_AUTH = 'google_auth';
	const SEND_METHOD_PHONE = 'phone';

	const SEND_AUTH_FALSE = 0;
	const SEND_AUTH_TRUE = 1;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return '{{%user}}';
	}

	public function behaviors() {
		return [[
			'class' => TimestampBehavior::className(),
			'createdAtAttribute' => 'created_at',
			'updatedAtAttribute' => 'updated_at',
			'value' => date('Y-m-d H:i:s'),
		], [
			'class' => ClosureTable::className(),
			'tableName' => 'user_tree',
		],
		];
	}

	public static function getStepAuthArray() {
		return [
			self::STEP_AUTH_FALSE => Yii::t('app', '2ух факторная отключена'),
			self::STEP_AUTH_TRUE => Yii::t('app', '2ух факторная включена'),
		];
	}

	public static function getStepMethodArray() {
		return [
			self::STEP_METHOD_EMAIL => Yii::t('app', 'E-mail'),
			self::STEP_METHOD_GOOGLE_AUTH => Yii::t('app', 'Google Authentificator'),
		];
	}

	public static function getSendAuthArray() {
		return [
			self::SEND_AUTH_FALSE => Yii::t('app', 'Проверка отправки отключена'),
			self::SEND_AUTH_TRUE => Yii::t('app', 'Проверка отправки включена'),
		];
	}

	public static function getSendMethodArray() {
		return [
			self::SEND_METHOD_EMAIL => Yii::t('app', 'E-mail'),
			self::SEND_METHOD_GOOGLE_AUTH => Yii::t('app', 'Google Authentificator'),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			['status', 'default', 'value' => self::STATUS_ACTIVE],
			['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
		];
	}

	public function fields() {
		return [
			'id',
			'username',
			'ga_secret',
			'step_auth',
			'step_method',
			'send_auth',
			'send_method',
			'country',
			'city',
			'city_address',
			'first_name',
			'last_name',
			'status',
			'created_at',
			'updated_at',
		];
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentity($id) {
		return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null) {
		return static::findOne(['auth_key' => $token]);
//        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
	}

	/**
	 * Finds user by username
	 *
	 * @param string $username
	 * @return static|null
	 */
	public static function findByUsername($username) {
		return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
	}

	/**
	 * Finds user by password reset token
	 *
	 * @param string $token password reset token
	 * @return static|null
	 */
	public static function findByPasswordResetToken($token) {
		if (!static::isPasswordResetTokenValid($token)) {
			return null;
		}

		return static::findOne([
			'password_reset_token' => $token,
			'status' => self::STATUS_ACTIVE,
		]);
	}

	/**
	 * Finds out if password reset token is valid
	 *
	 * @param string $token password reset token
	 * @return bool
	 */
	public static function isPasswordResetTokenValid($token) {
		if (empty($token)) {
			return false;
		}

		$timestamp = (int) substr($token, strrpos($token, '_') + 1);
		$expire = Yii::$app->params['user.passwordResetTokenExpire'];
		return $timestamp + $expire >= time();
	}

	/**
	 * @inheritdoc
	 */
	public function getId() {
		return $this->getPrimaryKey();
	}

	/**
	 * @inheritdoc
	 */
	public function getAuthKey() {
		return $this->auth_key;
	}

	/**
	 * @inheritdoc
	 */
	public function validateAuthKey($authKey) {
		return $this->getAuthKey() === $authKey;
	}

	/**
	 * Validates password
	 *
	 * @param string $password password to validate
	 * @return bool if password provided is valid for current user
	 */
	public function validatePassword($password) {
		return Yii::$app->security->validatePassword($password, $this->password_hash);
	}

	/**
	 * Generates password hash from password and sets it to the model
	 *
	 * @param string $password
	 */
	public function setPassword($password) {
		$this->password_hash = Yii::$app->security->generatePasswordHash($password);
	}

	/**
	 * Generates "remember me" authentication key
	 */
	public function generateAuthKey() {
		$this->auth_key = Yii::$app->security->generateRandomString();
	}

	/**
	 * Generates new password reset token
	 */
	public function generatePasswordResetToken() {
		$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
	}

	/**
	 * Removes password reset token
	 */
	public function removePasswordResetToken() {
		$this->password_reset_token = null;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUserPswWallets() {
		return $this->hasOne(UserPswWallet::className(), ['user_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUserTokenBearers() {
		return $this->hasMany(UserTokenBearer::className(), ['user_id' => 'id']);
	}

	public function getAvatar() {
		return $this->hasOne(Document::className(), ['user_id' => 'id'])->onCondition(['type' => 1]);
	}

	public function getBalances() {

		$balances = [];
		foreach (Currency::find()->all() as $currency) {
			$balances[$currency->key] = $this->getBalanceForCurrency($currency);
		}

		return $balances;
	}
}
