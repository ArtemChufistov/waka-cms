<?php

namespace common\models;

use common\components\DocumentFiles;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "document".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $type
 * @property string $comment
 * @property integer $verify
 * @property integer $is_visible
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 * @property Document $file
 */
class Document extends \yii\db\ActiveRecord {

	const TYPE_IMAGE_USER_AVATAR = 1;
	const TYPE_IMAGE_USER_AGREEMENT = 5;
	const TYPE_IMAGE_VERIFY_1 = 10;
	const TYPE_IMAGE_VERIFY_2 = 11;

	public static function getTypes() {
		return [
			self::TYPE_IMAGE_USER_AVATAR => Yii::t('app', 'User Avatar'),
			self::TYPE_IMAGE_USER_AGREEMENT => Yii::t('app', 'User agreement'),
			self::TYPE_IMAGE_VERIFY_1 => Yii::t('app', 'Image Verify 1'),
			self::TYPE_IMAGE_VERIFY_2 => Yii::t('app', 'Image Verify 2'),
		];
	}

	public function getTypeName() {
		$data = self::getTypes();

		return $data[$this->type];
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'document';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['user_id'], 'required'],
			[['user_id', 'type', 'verify', 'is_visible', 'created_at', 'updated_at'], 'integer'],
			[['comment'], 'string'],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
		];
	}

	public function fields() {
		return [
			'type',
			'comment',
			'verify',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'user_id' => Yii::t('app', 'User ID'),
			'type' => Yii::t('app', 'Type'),
			'comment' => Yii::t('app', 'Comment'),
			'verify' => Yii::t('app', 'Verify'),
			'is_visible' => Yii::t('app', 'Is Visible'),
			'created_at' => Yii::t('app', 'Created At'),
			'updated_at' => Yii::t('app', 'Updated At'),
		];
	}

	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}

	public function getFile() {
		return DocumentFiles::getFile($this->user_id, $this->type, $this->id . ".jpg");
	}

	public function getImage() {
		return DocumentFiles::getFileImage($this->user_id, $this->type, $this->id . ".jpg");
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}
}
