<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mail".
 *
 * @property integer $id
 * @property string $from
 * @property string $to
 * @property string $headers
 * @property string $content
 * @property integer $status
 * @property string $error
 * @property string $addition_info
 * @property integer $sender_at
 * @property integer $created_at
 * @property integer $updated_at
 */
class Sms extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'sms';
    }

    const STATUS_NEW = 'new';
    const STATUS_SEND = 'send';

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['phone', 'text', 'user_id', 'status', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'phone' => 'Phone',
            'text' => 'Text',
            'user_id' => 'User id',
            'status' => 'Status',
            'date' => 'Date',
        ];
    }
}
