<?php

namespace common\models;

use common\models\Document;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * DocumentSearch represents the model behind the search form about `common\models\Document`.
 */
class DocumentSearch extends Document {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['id', 'user_id', 'type', 'verify', 'is_visible', 'created_at', 'updated_at'], 'integer'],
			[['comment'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Document::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query->where(['type' => Document::TYPE_IMAGE_VERIFY_1])->orWhere(['type' => Document::TYPE_IMAGE_VERIFY_2]),
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'user_id' => $this->user_id,
			'type' => $this->type,
			'verify' => $this->verify,
			'is_visible' => $this->is_visible,
			'created_at' => $this->created_at,
			'updated_at' => $this->updated_at,
		]);

		$query->andFilterWhere(['like', 'comment', $this->comment]);

		return $dataProvider;
	}
}
