<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "service_commission".
 *
 * @property integer $id
 * @property string $service
 * @property string $commission
 * @property integer $created_at
 * @property integer $type
 * @property integer $for_verified
 *
 * @property Transaction[] $transactions
 */
class ServiceCommission extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_commission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'type', 'for_verified'], 'integer'],
            [['service'], 'string', 'max' => 16],
            [['commission'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'service' => Yii::t('app', 'Service'),
            'commission' => Yii::t('app', 'Commission'),
            'created_at' => Yii::t('app', 'Created At'),
            'type' => Yii::t('app', 'Type'),
            'for_verified' => Yii::t('app', 'For Verified'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['commission_id' => 'id']);
    }
}
