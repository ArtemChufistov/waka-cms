<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property string $language
 * @property string $translation
 * @property integer $correctly
 *
 * @property SourceMessage $id0
 */
class Message extends \yii\db\ActiveRecord
{
    const CORRECTLY_TRUE = 1;
    const CORRECTLY_FALSE = 0;

    public static function getcorrectlyArray()
    {
        return [
            self::CORRECTLY_TRUE => Yii::t('app', 'Да'),
            self::CORRECTLY_FALSE => Yii::t('app', 'Нет'),
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['correctly'], 'safe'],
            [['translation'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => SourceMessage::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'translation' => 'Translation',
            'correctly' => 'Correctly',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(SourceMessage::className(), ['id' => 'id']);
    }

    public function getSource()
    {
        return $this->hasOne(SourceMessage::className(), ['id' => 'id']);
    }
}
