<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "mail".
 *
 * @property integer $id
 * @property string $from
 * @property string $to
 * @property string $headers
 * @property string $content
 * @property integer $status
 * @property string $error
 * @property integer $sender_at
 * @property integer $created_at
 * @property integer $updated_at
 */
class Mail extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'mail';
    }

    const STATUS_CREATE = 0;
    const STATUS_SEND = 1;
    const STATUS_SUCCESS = 5;
    const STATUS_ERROR = 10;

    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['content'], 'string'],
            [['status', 'sender_at', 'created_at', 'updated_at'], 'integer'],
            [['from', 'to', 'headers', 'error'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'from' => 'From',
            'to' => 'To',
            'headers' => 'Headers',
            'content' => 'Content',
            'status' => 'Status',
            'error' => 'Error',
            'sender_at' => 'Sender At',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
