<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "preference".
 *
 * @property integer $id
 * @property string $key
 * @property string $value
 */
class Preference extends \yii\db\ActiveRecord {
    // Mail
    const KEY_SEND_MAIL_FREQUENCY = 'sendMailFrequency';
    const KEY_SEND_MAIL_HOST = 'sendMailHost';
    const KEY_SEND_MAIL_USERNAME = 'sendMailUsername';
    const KEY_SEND_MAIL_PASSWORD = 'sendMailPassword';
    const KEY_SEND_MAIL_PORT = 'sendMailPort';
    const KEY_SEND_MAIL_ENCRYPTION = 'sendMailEncryption';
    // Payment
    const KEY_PENDING_PAYMENT_TIME = 'pendingPaymentTime';
    // Coinbase
    const KEY_COINBASE_API_KEY = 'coinBaseApikey';
    const KEY_COINBASE_API_SECRET = 'coinBaseApiSecret';
    // Perfect Money
    const KEY_PERF_MON_SECRET = 'perfMonSecret';
    const KEY_PERF_MON_ACCOUND_ID = 'perfMonPayeeAccountId';
    const KEY_PERF_MON_PAYEE_ACCOUNT_USD = 'perfMonPayeeAccountUSD';
    const KEY_PERF_MON_PAYEE_ACCOUNT_EUR = 'perfMonPayeeAccountEUR';
    const KEY_PERF_MON_PAYEE_NAME = 'perfMonPayeeName';
    const KEY_PERF_MON_PASSWORD = 'perfMonPassword';
    const KEY_PERF_MON_STATUS_URL = 'perfMonStatusUrl';
    const KEY_PERF_MON_PAYMENT_URL = 'perfMonPaymentUrl';
    const KEY_PERF_MON_NO_PAYMENT_URL = 'perfMonNoPaymentUrl';
    // f-change
    const KEY_PREF_FCHANGE_NAME = 'prefFchangeName';
    const KEY_PREF_FCHANGE_PASSWORD = 'prefFchangePassword';
    const KEY_PREF_FCHANGE_SUCCESS_URL = 'prefFchangeSuccessUrl';
    const KEY_PREF_FCHANGE_ERROR_URL = 'prefFchangeErrorUrl';
    // poloniex.com
    const KEY_POLONIEX_API_KEY = 'poloniexApiKey';
    const KEY_POLONIEX_API_SECRET = 'poloniexApiSecret';
    // cron run period
    const KEY_CRON_RUN_PERIOD = 'cronRunPeriodInSec';
    // Payeer
    const KEY_PAYEER_SUCCESS_URL = 'payeerSuccessUrl';
    const KEY_PAYEER_FAIL_URL= 'payeerFailUrl';
    const KEY_PAYEER_STATUS_URL = 'payeerStatusUrl';
    const KEY_PAYEER_API_ID = 'payeerApiId';
    const KEY_PAYEER_MERCH_API_ID = 'payeerMerchApiId';
    const KEY_PAYEER_API_KEY = 'payeerApiKey';
    const KEY_PAYEER_MERCH_KEY = 'payeerMerchKey';
    const KEY_PAYEER_ENCRYPTION_KEY = 'payeerEncryptionKey';
    const KEY_PAYEER_ACCOUNT_NUMBER = 'payeerAccountNumber';
    const KEY_PAYEER_SUBMERCHANT = 'payeerSubMerchant';
    // Advanced Cash
    const KEY_ADVCASH_SCI_SECRET = 'advCashSciSecret';
    const KEY_ADVCASH_SCI_ACC_EMAIL = 'advCashAccountEmail';
    const KEY_ADVCASH_SCI_NAME = 'advCashSciName';
    const KEY_ADVCASH_SCI_SUCCESS_URL = 'advCashSuccessUrl';
    const KEY_ADVCASH_SCI_FAIL_URL = 'advCashFailUrl';
    const KEY_ADVCASH_SCI_STATUS_URL = 'advCashStatusUrl';
    const KEY_ADVCASH_ACCOUNT_EMAIL = 'advCashAPIAccountEmail';
    const KEY_ADVCASH_API_NAME = 'advCashAPIName';
    const KEY_ADVCASH_API_PASSWORD = 'advCashAPIPassword';

    // transfer comission
    const KEY_TRANSFER_COMMISSION_FIX = 'comissionFix';
    const KEY_TRANSFER_COMMISSION_PERC = 'comissionPerc';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'preference';
    }

    public static function getKeyArray() {
        return [
            self::KEY_SEND_MAIL_FREQUENCY => Yii::t('app', 'Задержка между отправками писем'),
            self::KEY_SEND_MAIL_HOST => Yii::t('app', 'Хост отправки писем'),
            self::KEY_SEND_MAIL_USERNAME => Yii::t('app', 'Почтовый ящик для отправки писем'),
            self::KEY_SEND_MAIL_PASSWORD => Yii::t('app', 'Пароль для почтового ящика'),
            self::KEY_SEND_MAIL_PORT => Yii::t('app', 'Порт хоста отправки писем'),
            self::KEY_SEND_MAIL_ENCRYPTION => Yii::t('app', 'Кодировка письма'),
            self::KEY_PENDING_PAYMENT_TIME => Yii::t('app', 'Время до отмены заявки на оплату'),
            self::KEY_COINBASE_API_KEY => Yii::t('app', 'API ключ CoinBase'),
            self::KEY_COINBASE_API_SECRET => Yii::t('app', 'API секретная фраза CoinBase'),
            self::KEY_PERF_MON_SECRET => Yii::t('app', 'Perfect Money Секретная фраза'),
            self::KEY_PERF_MON_ACCOUND_ID => Yii::t('app', 'Perfect Money аккаунт id'),
            self::KEY_PERF_MON_PASSWORD => Yii::t('app', 'Perfect Money пароль'),
            self::KEY_PERF_MON_PAYEE_ACCOUNT_USD => Yii::t('app', 'Perfect Money аккаунт USD'),
            self::KEY_PERF_MON_PAYEE_ACCOUNT_EUR => Yii::t('app', 'Perfect Money аккаунт EUR'),
            self::KEY_PERF_MON_PAYEE_NAME => Yii::t('app', 'Perfect Money имя'),
            self::KEY_PERF_MON_STATUS_URL => Yii::t('app', 'Perfect Money status url'),
            self::KEY_PERF_MON_PAYMENT_URL => Yii::t('app', 'Perfect Money payment url'),
            self::KEY_PERF_MON_NO_PAYMENT_URL => Yii::t('app', 'Perfect Money no payment url'),
            self::KEY_PREF_FCHANGE_NAME => Yii::t('app', 'F-change название мерчанта'),
            self::KEY_PREF_FCHANGE_PASSWORD => Yii::t('app', 'F-change пароль мерчанта'),
            self::KEY_PREF_FCHANGE_SUCCESS_URL => Yii::t('app', 'F-change success url'),
            self::KEY_PREF_FCHANGE_ERROR_URL => Yii::t('app', 'F-change error url'),
            self::KEY_POLONIEX_API_KEY => Yii::t('app', 'Poloniex API KEY'),
            self::KEY_POLONIEX_API_SECRET => Yii::t('app', 'Poloniex API SECRET'),
            self::KEY_CRON_RUN_PERIOD => Yii::t('app', 'Частота запуска CRON в секундах'),
            self::KEY_PAYEER_ACCOUNT_NUMBER => Yii::t('app', 'Payeer номер аккаунта'),
            self::KEY_PAYEER_API_ID => Yii::t('app', 'Payeer Мерчант ID'),
            self::KEY_PAYEER_MERCH_API_ID => Yii::t('app', 'Payeer API ID'),
            self::KEY_PAYEER_API_KEY => Yii::t('app', 'Payeer API ключ'),
            self::KEY_PAYEER_MERCH_KEY => Yii::t('app', 'Payeer Мерчант ключ'),
            self::KEY_PAYEER_ENCRYPTION_KEY => Yii::t('app', 'Payeer Мерчант ключ шифрования'),
            self::KEY_PAYEER_SUCCESS_URL => Yii::t('app', 'Payeer URL успешной оплаты'),
            self::KEY_PAYEER_FAIL_URL => Yii::t('app', 'Payeer URL неуспешной оплаты'),
            self::KEY_PAYEER_STATUS_URL => Yii::t('app', 'Payeer status URL'),
            self::KEY_TRANSFER_COMMISSION_FIX => Yii::t('app', 'Фикс комиссия за перевод'),
            self::KEY_TRANSFER_COMMISSION_PERC => Yii::t('app', 'Проц комиссия за перевод'),
            self::KEY_PAYEER_SUBMERCHANT => Yii::t('app', 'Payeer сабмерчант'),
            self::KEY_ADVCASH_SCI_SECRET => Yii::t('app', 'Advanced Cash SCI секрет'),
            self::KEY_ADVCASH_SCI_ACC_EMAIL => Yii::t('app', 'Advanced Cash SCI аккаунт E-mail'),
            self::KEY_ADVCASH_SCI_NAME => Yii::t('app', 'Advanced Cash SCI название'),
            self::KEY_ADVCASH_SCI_SUCCESS_URL => Yii::t('app', 'Advanced Cash url успешной оплаты'),
            self::KEY_ADVCASH_SCI_FAIL_URL => Yii::t('app', 'Advanced Cash url отклонённой оплаты'),
            self::KEY_ADVCASH_SCI_STATUS_URL => Yii::t('app', 'Advanced Cash статус url'),
            self::KEY_ADVCASH_ACCOUNT_EMAIL => Yii::t('app', 'Advanced Cash API аккаунт E-mail'),
            self::KEY_ADVCASH_API_NAME => Yii::t('app', 'Advanced Cash API название'),
            self::KEY_ADVCASH_API_PASSWORD => Yii::t('app', 'Advanced Cash API пароль'),
        ];
    }

    public function getKeyTitle() {
        $array = self::getKeyArray();

        return $array[$this->key];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['key', 'value'], 'required'],
            [['key', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Ключ'),
            'value' => Yii::t('app', 'Значение'),
        ];
    }
}
