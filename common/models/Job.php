<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "job".
 *
 * @property integer $id
 * @property string $method
 * @property string $frequency
 * @property string $last_date
 */
class Job extends \yii\db\ActiveRecord {

	const ACTIVE_TRUE = 1;
	const ACTIVE_FALSE = 0;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'job';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['method', 'frequency'], 'required'],
			[['last_date', 'active', 'cron_num'], 'safe'],
			[['method', 'frequency'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'method' => Yii::t('app', 'Метод'),
			'frequency' => Yii::t('app', 'Частота запуска'),
			'last_date' => Yii::t('app', 'Последняя дата запуска'),
			'active' => Yii::t('app', 'Активность'),
			'cron_num' => Yii::t('app', 'Номер крона'),
		];
	}
}
