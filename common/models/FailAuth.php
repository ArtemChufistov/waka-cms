<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "fail_auth".
 *
 * @property ip $id
 * @property agent $agent
 * @property date $date
 */
class FailAuth extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fail_auth';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip', 'agent', 'date'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'agent' => Yii::t('app', 'Agent'),
            'date' => Yii::t('app', 'Date'),
        ];
    }
}
