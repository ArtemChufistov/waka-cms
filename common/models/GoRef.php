<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "go_ref".
 *
 * @property integer $id
 * @property string $ref
 * @property integer $date
 */
class GoRef extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'go_ref';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ref', 'date'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ref' => Yii::t('app', 'Ref'),
            'date' => Yii::t('app', 'Date'),
        ];
    }
}
