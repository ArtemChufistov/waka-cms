<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mail_template".
 *
 * @property integer $id
 * @property string $type
 * @property string $language
 * @property string $headers
 * @property string $content
 */
class MailTemplate extends \yii\db\ActiveRecord {

	const TYPE_REGISTRATION_SUCCESS_CONFIRM = 'registrationSuccessConfirm';
	const TYPE_UNCONFIRM_EMAIL_CODE = 'unConfirmEmailCode';
	const TYPE_CONFIRM_EMAIL_CODE = 'confirmEmailCode';
	const TYPE_CHANGE_EMAIL_CODE = 'changeEmailCode';
	const TYPE_CONFIRM_LOGIN = 'confirmLogin';
	const TYPE_RESET_PASSWORD = 'resetPassword';
	const TYPE_NEW_PASSWORD = 'newPassword';
	const TYPE_PAY_PASSWORD = 'payPassword';
	const TYPE_SEND_INVOICE = 'sendInvoice';
	const TYPE_INVOICE_CANCELED_USER = 'invoiceCanceledUser';
	const TYPE_INVOICE_CANCELED_FROM_USER = 'invoiceCanceledFromUser';
	const TYPE_INNER_TRANSFER_SUCCESS = 'innerTransferSuccess';
	const TYPE_INNER_PAY_SUCCESS = 'innerPaySuccess';

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'mail_template';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['headers', 'content'], 'string'],
			[['type', 'language'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'type' => 'Type',
			'language' => 'Language',
			'headers' => 'Headers',
			'content' => 'Content',
		];
	}
}
