<?php

namespace common\commands;

use common\components\MailCompile;

class Command {

	// Send mail
	public function sendMail() {
		$result = MailCompile::sendMail();

		if ($result instanceof \common\models\Mail) {
			echo Yii::t('app', 'Письмо успешно отправлено на E-mail: {email}, заголовок "{headers}"', ['email' => $result->to, 'headers' => $result->headers]) . "\r\n";
		} else if ($result instanceof Exception) {
			echo $result . "\r\n";
		}
	}
}