<?php
namespace common\modules\changer\models;

use Yii;
use yii\mongodb\ActiveRecord;

class OrderPart extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'changer_order_part';
    }

    public function attributes() {
        return [
            'id',
            'order_id',
            'wallet_id',
            'amount',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [[[
            'id',
            'order_id',
            'wallet_id',
            'amount',
            ], 'safe'],
        ];
    }
}