<?php
namespace common\modules\changer\models;

use Yii;
use yii\mongodb\ActiveRecord;

class Wallet extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'changer_wallet';
    }

    public function attributes() {
        return[
            'id',
            'key',
            'title',
            'paysys_id',
            'currency_id',
            'balance',
            'num',
            'secret1',
            'secret2',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [[[
            'id',
            'key',
            'title',
            'paysys_id',
            'currency_id',
            'balance',
            'num',
            'secret1',
            'secret2',
        ],
            'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Идентификатор'),
            'key' => Yii::t('app', 'Ключ'),
            'title' => Yii::t('app', 'Название'),
            'paysys_id' => Yii::t('app', 'Платёжная система'),
            'currency_id' => Yii::t('app', 'Валюта'),
            'balance' => Yii::t('app', 'Баланс'),
            'num' => Yii::t('app', 'Номер'),
            'secret1' => Yii::t('app', 'Секрет 1'),
            'secret2' => Yii::t('app', 'Секрет 2'),
        ];
    }
}