<?php
namespace common\modules\changer\models;

use Yii;
use yii\mongodb\ActiveRecord;

class Order extends \yii\db\ActiveRecord {

    const TYPE_REAL = 'real';
    const TYPE_NOT_REAL = 'not_real';

    const STATUS_NEW = 'new';
    const STATUS_WAIT_INCOMING_MONEY = 'wait_incoming_money';
    const STATUS_WAIT_OUTCOMING_MONEY = 'wait_outcoming_money';
    const STATUS_ERROR = 'error';
    const STATUS_CANCEL = 'cancel';
    const STATUS_SUCCESS = 'success';

    public static function tableName() {
        return 'changer_order';
    }

    public static function getStatuses() {
        return [
            self::STATUS_NEW => Yii::t('app', 'Новый'),
            self::STATUS_WAIT_INCOMING_MONEY => Yii::t('app', 'Ждём поступления средств'),
            self::STATUS_WAIT_OUTCOMING_MONEY => Yii::t('app', 'Ждёт выплаты средств'),
            self::STATUS_ERROR => Yii::t('app', 'Произошла ошибка'),
            self::STATUS_CANCEL => Yii::t('app', 'Отменён'),
            self::STATUS_SUCCESS => Yii::t('app', 'Успешно')
        ];
    }

    public function statusTitle() {
        $data = self::getStatuses();

        return $data[$this->status];
    }

    public static function getTypes() {
        return [
            self::TYPE_REAL => Yii::t('app', 'Настоящий'),
            self::TYPE_NOT_REAL => Yii::t('app', 'Ненастоящий'),
        ];
    }

    public function attributes() {
        return[
            'id',
            'type',
            'status',
            'count_parts',
            'currency_id_from',
            'currency_id_to',
            'amount_from',
            'real_amount_from',
            'amount_to',
            'course',
            'email',
            'phone',
            'hash',
            'time_for_pay',
            'date_add',
            'wallet_from',
            'wallet_to',
            'card_to',
            'card_from',
            'fio_from',
            'fio_to',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [[[
            'id',
            'type',
            'status',
            'count_parts',
            'currency_id_from',
            'currency_id_to',
            'amount_from',
            'real_amount_from',
            'amount_to',
            'course',
            'email',
            'phone',
            'hash',
            'time_for_pay',
            'date_add',
            'wallet_from',
            'wallet_to',
            'card_to',
            'card_from',
            'fio_from',
            'fio_to',
        ],
            'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Идентификатор'),
            'type' => Yii::t('app', 'Тип'),
            'status' => Yii::t('app', 'Статус'),
            'count_parts' => Yii::t('app', 'Количество частей'),
            'currency_id_from' => Yii::t('app', 'Начальная валюта'),
            'currency_id_to' => Yii::t('app', 'Конечная валюта'),
            'amount_from' => Yii::t('app', 'Получаемая сумма'),
            'real_amount_from' => Yii::t('app', 'Реальная получаемая сумма'),
            'amount_to' => Yii::t('app', 'Отдаваемая сумма'),
            'course' => Yii::t('app', 'Курс'),
            'email' => Yii::t('app', 'E-mail'),
            'phone' => Yii::t('app', 'Телефон'),
            'hash' => Yii::t('app', 'Хэш'),
            'time_for_pay' => Yii::t('app', 'Время на оплату в секундах'),
            'date_add' => Yii::t('app', 'Дата добавления'),
            'wallet_from' => Yii::t('app', 'Кошелёк отправителя'),
            'wallet_to' => Yii::t('app', 'Кошелёк получаетеля'),
            'card_from' => Yii::t('app', 'Карта отправителя'),
            'card_to' => Yii::t('app', 'Карта получаетеля'),
            'fio_from' => Yii::t('app', 'ФИО отправителя'),
            'fio_to' => Yii::t('app', 'ФИО получаетеля'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyFrom() {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyTo() {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id_to']);
    }
}