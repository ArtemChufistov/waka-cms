<?php
namespace common\modules\changer\models;

use Yii;
use yii\mongodb\ActiveRecord;

class Paysys extends \yii\db\ActiveRecord {

    const KEY_ADVCASH = 'AdvancedCash';
    const KEY_PM = 'PerfectMoney';
    const KEY_PAYEER = 'Payeer';
    const KEY_SBERBANK = 'Sberbank';
    const KEY_YAM = 'YandexMoney';
    const KEY_EXMO = 'Exmo';

    public static function tableName() {
        return 'changer_paysys';
    }

    public function attributes() {
        return[
            'id',
            'key',
            'title',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [[[
            'id',
            'key',
            'title',
        ],
            'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'Идентификатор'),
            'key' => Yii::t('app', 'Ключ'),
            'title' => Yii::t('app', 'Название'),
        ];
    }
}