<?php
namespace common\modules\changer\models;

use Yii;
use yii\mongodb\ActiveRecord;

class Course extends \yii\db\ActiveRecord {

    const ACTIVE_TRUE = 1;
    const ACTIVE_FALSE = 0;

    public static function tableName() {
        return 'changer_course';
    }

    public function attributes() {
        return[
            'id',
            'currency_id_from',
            'currency_id_to',
            'course',
            'active',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [[[
            'id',
            'currency_id_from',
            'currency_id_to',
            'course',
            'active',
        ],
            'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Идентификатор'),
            'currency_id_from' => Yii::t('app', 'С валюты'),
            'currency_id_to' => Yii::t('app', 'На валюту'),
            'course' => Yii::t('app', 'Курс'),
            'active' => Yii::t('app', 'Активен'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromCurrency() {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToCurrency() {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id_to']);
    }
}