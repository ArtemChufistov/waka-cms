<?php
namespace common\modules\changer\models;

use Yii;
use yii\mongodb\ActiveRecord;

class WalletCurrencyTo extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'changer_wallet_currency_to';
    }

    public function attributes() {
        return[
            'id',
            'wallet_id',
            'currency_id_to',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [[[
            'id',
            'wallet_id',
            'currency_id_to',
        ],
            'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'Идентификатор'),
            'wallet_id' => Yii::t('app', 'Кошелёк'),
            'currency_id_to' => Yii::t('app', 'На валюту'),
        ];
    }
}