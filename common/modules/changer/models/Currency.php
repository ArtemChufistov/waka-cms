<?php
namespace common\modules\changer\models;

use Yii;
use yii\mongodb\ActiveRecord;

class Currency extends \yii\db\ActiveRecord {

    const KEY_ADVCUSD = 'ADVCUSD';
    const KEY_ADVCRUB = 'ADVCRUB';
    const KEY_PMUSD = 'PMUSD';
    const KEY_PMEUR = 'PMEUR';
    const KEY_PAYEERUSD = 'PRUSD';
    const KEY_PAYEERRUB = 'PRRUB';
    const KEY_SBERRUB = 'SBERRUB';
    const KEY_YAMRUB = 'YAMRUB';
    const KEY_P24UAH = 'P24UAH';
    const KEY_CARDUAH = 'CARDUAH';
    const KEY_EXMRUB = 'EXMRUB';
    const KEY_EXMUSD = 'EXMUSD';
    const KEY_MONOBUAH = 'MONOBUAH';
    const KEY_BTC = 'BTC';
    const KEY_LTC = 'LTC';
    const KEY_ETH = 'ETH';
    const KEY_DOGE = 'DOGE';
    const KEY_XMR = 'XMR';
    const KEY_DASH = 'DASH';

    public static function tableName() {
        return 'changer_currency';
    }

    public function attributes() {
        return[
            'id',
            'key',
            'title',
            'in',
            'out',
            'minamount',
            'maxamount',
            'active',
            'label',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [[[
            'id',
            'key',
            'title',
            'label',
            'in',
            'out',
            'minamount',
            'maxamount',
            'active',
        ],
            'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'Идентификатор'),
            'key' => Yii::t('app', 'Ключ'),
            'title' => Yii::t('app', 'Название'),
            'label' => Yii::t('app', 'Аббревиатура'),
            'in' => Yii::t('app', 'Входящее значение'),
            'out' => Yii::t('app', 'Выходящее значение'),
            'minamount' => Yii::t('app', 'Минимальная сумма'),
            'maxamount' => Yii::t('app', 'Максимальная сумма'),
            'active' => Yii::t('app', 'Активен'),
        ];
    }

    public static function getKeyTitles() {
        return [
            self::KEY_ADVCUSD => Yii::t('app', 'Advanced Cash'),
            self::KEY_ADVCRUB => Yii::t('app', 'Advanced Cash'),
            self::KEY_PMUSD => Yii::t('app', 'Perfect Money'),
            self::KEY_PMEUR => Yii::t('app', 'Perfect Money'),
            self::KEY_PAYEERUSD => Yii::t('app', 'Payeer'),
            self::KEY_PAYEERRUB => Yii::t('app', 'Payeer'),
            self::KEY_SBERRUB => Yii::t('app', 'Сбербанк'),
            self::KEY_YAMRUB => Yii::t('app', 'Яндекс Деньги'),
            self::KEY_P24UAH => Yii::t('app', 'Приват 24'),
            self::KEY_CARDUAH => Yii::t('app', 'Visa/Mastercard UAH'),
            self::KEY_EXMRUB => Yii::t('app', 'Exmo RUB'),
            self::KEY_EXMUSD => Yii::t('app', 'Exmo USD'),
            self::KEY_MONOBUAH => Yii::t('app', 'МоноБанк UAH'),
            self::KEY_BTC => Yii::t('app', 'Bitcoin'),
            self::KEY_LTC => Yii::t('app', 'Litecoin'),
            self::KEY_ETH => Yii::t('app', 'Ethereum'),
            self::KEY_DOGE => Yii::t('app', 'Dogecoin'),
            self::KEY_XMR => Yii::t('app', 'Monero'),
            self::KEY_DASH => Yii::t('app', 'Dash'),
        ];
    }
}