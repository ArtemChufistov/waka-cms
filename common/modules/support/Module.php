<?php

namespace common\modules\support;

use Yii;

/**
 * Class Module
 * @package common\modules\support
 */
class Module extends \yii\base\Module {

	public function init() {
		parent::init();
	}
}
