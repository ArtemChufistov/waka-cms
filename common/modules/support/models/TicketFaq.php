<?php

namespace common\modules\support\models;

use Yii;

/**
 * This is the model class for table "ticket_faq".
 *
 * @property integer $id
 * @property integer $type
 * @property string $title
 * @property string $message
 */
class TicketFaq extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'ticket_faq';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['type'], 'integer'],
			[['message'], 'string'],
			[['title'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'type' => Yii::t('app', 'Type'),
			'title' => Yii::t('app', 'Title'),
			'message' => Yii::t('app', 'Message'),
		];
	}
}
