<?php

namespace common\modules\support\models;

use Yii;

/**
 * This is the model class for table "ticket_status".
 *
 * @property integer $id
 * @property string $name
 * @property string $color
 */
class TicketStatus extends \yii\db\ActiveRecord {
	const KEY_NEW = 'new';
	const KEY_IN_WORK = 'in_work';
	const KEY_OK = 'ok';
	const KEY_CANCEL = 'cancel';
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'ticket_status';
	}

	public function titleName() {
		return Yii::t('app', '{name}', ['name' => $this->name]);
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['name', 'color'], 'required'],
			[['key', 'name', 'color'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'key' => Yii::t('app', 'Ключ'),
			'name' => Yii::t('app', 'Название'),
			'color' => Yii::t('app', 'Цвет'),
		];
	}
}
