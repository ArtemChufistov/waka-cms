<?php

namespace common\modules\support\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "ticket_items".
 *
 * @property integer $id
 * @property integer $ticket_id
 * @property integer $user_id
 * @property string $message
 * @property integer $is_visible
 * @property integer $is_visible_admin
 * @property integer $is_file
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Tickets $ticket
 * @property User $user
 */
class TicketItems extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'ticket_items';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['ticket_id', 'user_id'], 'required'],
			[['ticket_id', 'user_id', 'is_visible', 'is_visible_admin', 'is_file', 'created_at', 'updated_at'], 'integer'],
			[['message'], 'string'],
			[['ticket_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tickets::className(), 'targetAttribute' => ['ticket_id' => 'id']],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'ticket_id' => Yii::t('app', 'Ticket ID'),
			'user_id' => Yii::t('app', 'User ID'),
			'message' => Yii::t('app', 'Message'),
			'is_visible' => Yii::t('app', 'Is Visible'),
			'is_visible_admin' => Yii::t('app', 'Is Visible Admin'),
			'is_file' => Yii::t('app', 'Is File'),
			'created_at' => Yii::t('app', 'Created At'),
			'updated_at' => Yii::t('app', 'Updated At'),
		];
	}

	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTicket() {
		return $this->hasOne(Tickets::className(), ['id' => 'ticket_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}
}
