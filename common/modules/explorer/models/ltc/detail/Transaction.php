<?php
namespace common\modules\explorer\models\ltc\detail;

use common\modules\explorer\models\ltc\WalletTransaction;
use Yii;

class Transaction extends \common\modules\explorer\models\ltc\Transaction
{
    public function fields() {
        return [
            'blocknum',
            'txid',
            'hash',
            'version',
            'size',
            'vsize',
            'weight',
            'locktime',
            'vin' => function($model) {
                $wtx = WalletTransaction::find()
                    ->where(['txid' => $model->txid])
                    ->andWhere(['type' => WalletTransaction::TYPE_VIN])
                    ->all();

                return $wtx;
            },
            'vout' => function($model) {
                $wtx = WalletTransaction::find()
                    ->where(['txid' => $model->txid])
                    ->andWhere(['type' => WalletTransaction::TYPE_VOUT])
                    ->all();

                return $wtx;
            },
        ];
    }
}