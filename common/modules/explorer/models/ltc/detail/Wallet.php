<?php
namespace common\modules\explorer\models\ltc\detail;

use common\modules\explorer\models\ltc\WalletTransaction;
use Yii;

class Wallet extends \common\modules\explorer\models\ltc\Wallet {

    public function fields()
    {
        return [
            'address',
            'balance',
            'txs'  => function($model) {
                $wtx = WalletTransaction::find()
                    ->where(['address' => $model->address])
                    ->all();

                return $wtx;
            },
        ];
    }
}