<?php
namespace common\modules\explorer\models\ltc\detail;

use Yii;

class Block extends \common\modules\explorer\models\ltc\Block {

    public function fields()
    {
        return [
            'num',
            'hash',
            'confirmations',
            'strippedsize',
            'size',
            'weight',
            'height',
            'version',
            'versionHex',
            'merkleroot',
            'time',
            'mediantime',
            'nonce',
            'bits',
            'difficulty',
            'chainwork',
            'nTx',
            'previousblockhash',
            'nextblockhash',
            'tx' => function($model) {
                return $model->getTxs()->all();
            },
        ];
    }
}