<?php
namespace common\modules\explorer\models\ltc;

use Yii;
use yii\mongodb\ActiveRecord;

class Block extends \yii\db\ActiveRecord {

    public static function getDb() {
        return \Yii::$app->dbpostgreeltc;
    }

    public static function tableName() {
        return 'block';
    }

    public function attributes() {
        return[
            'num',
            'hash',
            'confirmations',
            'strippedsize',
            'size',
            'weight',
            'height',
            'version',
            'versionHex',
            'merkleroot',
            'time',
            'mediantime',
            'nonce',
            'bits',
            'difficulty',
            'chainwork',
            'nTx',
            'previousblockhash',
            'nextblockhash',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [[[
                'num',
                'hash',
                'confirmations',
                'strippedsize',
                'size',
                'weight',
                'height',
                'version',
                'versionHex',
                'merkleroot',
                'time',
                'mediantime',
                'nonce',
                'bits',
                'difficulty',
                'chainwork',
                'nTx',
                'previousblockhash',
                'nextblockhash',
            ],
            'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'num' => Yii::t('app', 'Номер'),
            'hash' => Yii::t('app', 'Хэш'),
            'confirmations' => Yii::t('app', 'Подтверждений'),
            'strippedsize' => Yii::t('app', 'Stripped size'),
            'size' => Yii::t('app', 'Размер'),
            'weight' => Yii::t('app', 'Вес'),
            'height' => Yii::t('app', 'Вес'),
            'version' => Yii::t('app', 'Версия'),
            'versionHex' => Yii::t('app', 'Версия HEX'),
            'merkleroot' => Yii::t('app', 'merkleroot'),
            'time' => Yii::t('app', 'Время'),
            'mediantime' => Yii::t('app', 'Время'),
            'nonce' => Yii::t('app', 'Описание'),
            'bits' => Yii::t('app', 'Bits'),
            'difficulty' => Yii::t('app', 'Сложность'),
            'chainwork' => Yii::t('app', 'Chainwork'),
            'nTx' => Yii::t('app', 'nTx'),
            'previousblockhash' => Yii::t('app', 'Previous block hash'),
            'nextblockhash' => Yii::t('app', 'Next block hash'),
        ];
    }

    public function getTxs() {
        return $this->hasMany(Transaction::className(), ['blocknum' => 'num']);
    }
}