<?php
namespace common\modules\explorer\models\ltc;

use app\models\Elastic;
use yii\base\Model;
use yii\elasticsearch\ActiveDataProvider;
use yii\elasticsearch\Query;
use yii\elasticsearch\QueryBuilder;
/**
 * ArticlesSearch represents the model behind the search form about `app\models\Articles`.
 */
class BlockSearch extends Block
{
    public function searches($params)
    {
        $db = Block::getDb();
        $query = new Query();
        $queryBuilder = new QueryBuilder($db);
        $match = ['match' => $params];
        $query->query = $match;
        $build  = $queryBuilder->build($query);
        $re  = $query->search($db, $build);
        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => ['pageSize' => 10],
        ]);

        return $dataProvider;
    }
}