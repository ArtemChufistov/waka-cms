<?php
namespace common\modules\explorer\models\ltc;

use Yii;
use yii\mongodb\ActiveRecord;

class WalletTransaction extends \yii\db\ActiveRecord {

    const TYPE_VIN = 'vin';
    const TYPE_VOUT = 'vout';

    public static function getDb() {
        return \Yii::$app->dbpostgreeltc;
    }

    public static function tableName() {
        return 'wallet_transaction';
    }

    public function attributes() {
        return[
            'address',
            'txid',
            'value',
            'type',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [[[
            'address',
            'txid',
            'value',
            'type',
        ],
            'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'address' => Yii::t('app', 'Адрес'),
            'transaction' => Yii::t('app', 'Транзакция'),
            'value' => Yii::t('app', 'Значение'),
            'type' => Yii::t('app', 'Тип'),
        ];
    }
}