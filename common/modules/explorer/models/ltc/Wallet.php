<?php
namespace common\modules\explorer\models\ltc;

use Yii;
use yii\mongodb\ActiveRecord;

class Wallet extends \yii\db\ActiveRecord {

    public static function getDb() {
        return \Yii::$app->dbpostgreeltc;
    }

    public static function tableName() {
        return 'wallet';
    }

    public function attributes() {
        return[
            'address',
            'balance',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [[[
            'address',
            'balance',
            ],
            'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'address' => Yii::t('app', 'Адрес'),
            'balance' => Yii::t('app', 'Баланс')
        ];
    }
}