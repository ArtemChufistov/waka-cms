<?php
namespace common\modules\explorer\models\ltc;

use Yii;
use yii\mongodb\ActiveRecord;

class Transaction extends \yii\db\ActiveRecord {

    public static function getDb() {
        return \Yii::$app->dbpostgreeltc;
    }

    public static function tableName() {
        return 'transaction';
    }

    public function attributes() {
        return[
            'blocknum',
            'txid',
            'hash',
            'version',
            'size',
            'vsize',
            'weight',
            'locktime',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [[[
            'blocknum',
            'txid',
            'hash',
            'version',
            'size',
            'vsize',
            'weight',
            'locktime',
            ],
            'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'blocknum' => Yii::t('app', 'Номер блока'),
            'txid' => Yii::t('app', 'Номер'),
            'hash' => Yii::t('app', 'Хэш'),
            'version' => Yii::t('app', 'Версия'),
            'size' => Yii::t('app', 'Размер'),
            'vsize' => Yii::t('app', 'Vsize'),
            'weight' => Yii::t('app', 'Weight'),
            'locktime' => Yii::t('app', 'Locktime'),
        ];
    }
}