<?php

namespace common\modules\explorer\commands;

use common\models\Currency;
use common\modules\crypto\coin\factory\CoinFactory;
use common\modules\explorer\models\ltc\Wallet;
use common\modules\explorer\models\ltc\WalletTransaction;

class CoinChainParser
{
    // Обработка Blockchain
    public function handle($startFrom = 0) {

        $currency = Currency::find()->where(['key' => Currency::KEY_LTC])->one();

        if (empty($currency->latest_block)) {
            $currency->latest_block = 1;
        }

        if ($startFrom != 0) {
            $currency->latest_block = $startFrom;
        }

        $ltcClass = CoinFactory::get($currency->key);

        $lastBlock = $ltcClass->getblockcount();

        if ($currency->latest_block < $lastBlock) {
            echo 'Обрабатываем валюту: ' . $currency->key . ' Блок: ' . $currency->latest_block . "\r\n";

            $data = self::updateData($currency->key, $currency->latest_block);

            $currency->latest_block++;
            $currency->save(false);

            echo 'Номер блока: ' . $currency->latest_block . ' Хэш: ' . $data['blockHash'] . ' обработан' . "\r\n";
        }
    }

    // Обновление информации BlockChain для переданой валюты и блока
    public static function updateData($currency, $blockNum) {

        $startTime = microtime(true);

        $currency = strtoupper($currency);

        $curClass = CoinFactory::get($currency);

        $blockHash = $curClass->getblockhash($blockNum);

        if (empty($blockHash)) {
            throw new \Exception('Не получен хэш блока: ' . $blockNum);
        }

        $blockNs = CoinFactory::getNs($currency, 'Block');

        $block = $blockNs::find()->where(['hash' => $blockHash])->one();

        if (empty($block)) {
            $block = new $blockNs;
        }

        $blockInfo = $curClass->getblock($blockHash);

        $block->num = $blockNum;
        $block->setAttributes($blockInfo);

        if (!$block->save()){
            throw new \Exception('Не удалось сохранить данные по блоку: ' . $block->num);
        }

        $transactionNs = CoinFactory::getNs($currency, 'Transaction');
        $walletNs = CoinFactory::getNs($currency, 'Wallet');
        $waltxNs = CoinFactory::getNs($currency, 'WalletTransaction');

        foreach ($blockInfo['tx'] as $num => $txHash) {
            echo 'Обрабатываем транзу номер: ' . $num . ' хэш: ' . $txHash . "\r\n";
            $rawTx = $curClass->getrawtransaction($txHash);
            $txInfo = $curClass->decoderawtransaction($rawTx);

            if ($txInfo['vout'][0]['scriptPubKey']['type'] == 'nonstandard') {
                $txInfo = $curClass->decoderawtransaction($rawTx, true);
            }

            foreach($txInfo['vin'] as &$vinItem) {
                if (!empty($vinItem['txid'])) {
                    $vinRawTx = $curClass->getrawtransaction($vinItem['txid']);
                    $vinTxInfo = $curClass->decoderawtransaction($vinRawTx);

                    if ($vinTxInfo['vout'][0]['scriptPubKey']['type'] == 'nonstandard') {
                        $vinTxInfo = $curClass->decoderawtransaction($vinRawTx, true);
                    }

                    $vinItem['address'] = $vinTxInfo['vout'][$vinItem['vout']]['scriptPubKey']['addresses'][0];
                    $vinItem['value'] = $vinTxInfo['vout'][$vinItem['vout']]['value'];
                }
            }

            $tx = $transactionNs::find()->where(['txid' => $txHash])->one();

            if (empty($tx)) {
                $tx = new $transactionNs;
            }

            $tx->setAttributes($txInfo);
            $tx->blocknum = $blockNum;

            if (!$tx->save(false)){
                throw new \Exception('Не удалось сохранить данные по транзакции: ' . $tx->hash);
            }

            foreach ($txInfo['vout'] as $vout) {

                $address = (string)$vout['scriptPubKey']['addresses'][0];

                $wallet = $walletNs::find()->where(['address' => $address])->one();

                if (empty($wallet)) {
                    $wallet = new $walletNs;
                    $wallet->address = $address;
                    $wallet->balance = 0;
                    $wallet->save(false);
                }

                $wtx = WalletTransaction::find()
                    ->where(['address' => $address])
                    ->andWhere(['txid' => $txInfo['txid']])
                    ->andWhere(['type' => $waltxNs::TYPE_VOUT])
                    ->one();

                if (empty($wtx)){
                    $wtx = new WalletTransaction;
                    $wtx->address = $address;
                    $wtx->value = $vout['value'];
                    $wtx->txid = $txInfo['txid'];
                    $wtx->type = $waltxNs::TYPE_VOUT;
                    $wtx->save(false);

                    $wallet->balance = $wallet->balance + $wtx->value;
                    $wallet->save(false);
                }
            }

            foreach ($txInfo['vin'] as $vin) {

                if (empty($vin['txid'])) {
                    // Coinbase..........
                    continue;
                }

                $address = $vin['address'];

                $wallet = $walletNs::find()->where(['address' => $vin['address']])->one();

                if (empty($wallet)) {
                    //print_r($txInfo);
                    print_r($vin);
                    $wallet = new $walletNs;
                    $wallet->address = $address;
                    $wallet->balance = 0;
                    $wallet->save(false);
                }

                $wtx = WalletTransaction::find()
                    ->where(['address' => $address])
                    ->andWhere(['txid' => $txInfo['txid']])
                    ->andWhere(['type' => $waltxNs::TYPE_VIN])
                    ->one();

                if (empty($wtx)){
                    $wtx = new WalletTransaction;
                    $wtx->address = $address;
                    $wtx->value = $vin['value'];
                    $wtx->txid = $txInfo['txid'];
                    $wtx->type = $waltxNs::TYPE_VIN;
                    $wtx->save(false);

                    $wallet->balance = $wallet->balance - $wtx->value;
                    $wallet->save(false);
                }
            }
        }

        return [
            'blockHash' => $blockHash
        ];
    }

    public static function debugTime($startTime, $point) {
        echo 'point' . $point . ': ' . (microtime(true) - $startTime) . "\r\n";
    }
}