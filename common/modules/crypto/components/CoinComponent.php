<?php

namespace common\modules\crypto\components;

use common\models\Currency;
use common\modules\crypto\coin\factory\CoinFactory;
use common\modules\crypto\models\AccountWallet;
use common\modules\crypto\models\PaymentTask;
use common\modules\crypto\models\Transaction;
use ErrorException;
use yii\httpclient\Client;

class CoinComponent {
	public function getNewAddress($currency, $userId = null, $accountId = null) {
		$currency = Currency::find()->where(['key' => $currency])->one();

		if (empty($currency)) {
			throw new ErrorException(Yii::t('app', 'Валюта не найдена'));
		}

		$addressInfo = CoinFactory::get($currency->key)->getNewAddress($userId);

		if (!empty($addressInfo->error)) {
			throw new ErrorException(Yii::t('app', 'Ошибка получения кошелька: ' . $address->error));
		}

		$accountWallet = AccountWallet::find()
			->where(['currency_type' => $currency->slug])
			->andWhere(['currency_wallet' => $addressInfo['currency_wallet']])
			->one();

		if (empty($accountWallet)) {
			$accountWallet = new AccountWallet();
			$accountWallet->user_id = $userId;
			$accountWallet->m_account_id = $accountId;
			$accountWallet->currency_type = $currency->slug;
			$accountWallet->setAttributes($addressInfo);
			$accountWallet->save();
		} elseif ($accountWallet->user_id != $userId) {
			throw new ErrorException(Yii::t('app', 'Этот адрес занят другим пользователем'));
		}

		return $accountWallet->currency_wallet;
	}

	public function saveForSend($sendFormObj) {

		$currency = Currency::find()->where(['key' => $sendFormObj->currencyKey])->one();

		$transaction = new Transaction();
		$transaction->amount = $sendFormObj->amount;
		$transaction->user_id = $sendFormObj->userId;
		$transaction->to_addr = $sendFormObj->toAddr;
		$transaction->m_account_id = $sendFormObj->accountId;
		$transaction->currency = $currency->slug;
		$transaction->is_show = Transaction::IS_SHOW_TRUE;
		$transaction->category = PaymentTask::CATEGORY_SEND;
		$transaction->is_processing_complete = Transaction::IS_IN_PROCESSING;
		$transaction->confirmations = 0;
		$transaction->txid = Transaction::VIRTUAL_STATUS_NEW . '-' . time();
		$transaction->setAttributes($sendFormObj->additional);

		if ($transaction->save()) {

			$paymentTask = new PaymentTask();
			$paymentTask->m_id = $sendFormObj->accountId;
			$paymentTask->amount = $sendFormObj->amount;
			$paymentTask->user_id = $sendFormObj->userId;
			$paymentTask->to_addr = $sendFormObj->toAddr;
			$paymentTask->to_addr_old = $sendFormObj->toAddr;
			$paymentTask->m_node_transaction_id = $transaction->id;
			$paymentTask->currency = $currency->slug;
			$paymentTask->id_parent = 0;
			//$paymentTask->status = PaymentTask::TX_STATUS_CREATE_NEW_TX_FAIL;
			$paymentTask->status = PaymentTask::TX_STATUS_COMPLETE;
			$paymentTask->category = PaymentTask::CATEGORY_SEND;
			$paymentTask->isShowToUser = PaymentTask::SHOW_TO_USER;
			$paymentTask->request_type = PaymentTask::REQUEST_TYPE_WEB;
			$paymentTask->feePaySender = 1;
			$paymentTask->fee_per_kByte = $sendFormObj->getEstimateFee();
			$paymentTask->fee_per_transaction = NULL;
			$paymentTask->fee_per_byte = $sendFormObj->feePer;

			if ($paymentTask->save()) {

				$transaction->m_payment_task_id = $paymentTask->id;
				$transaction->from_addr = $paymentTask->from_addr;
				$transaction->save();

				$paymentTask->status = PaymentTask::TX_STATUS_NEW;
				$paymentTask->save();

				// обновляем баланс
				$client = new Client();
				$response = $client
					->createRequest()
					->setMethod('get')
					->setUrl('https://crypchant.com/api/notify/refresh-balance?user_id=' . $paymentTask->user_id . '&currency=' . $currency->slug . '&delay=10')
					->send();

				return $transaction;
			} else {
				return $paymentTask;
			}
		}

		return $transaction;
	}
}