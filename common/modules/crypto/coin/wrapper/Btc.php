<?php

namespace common\modules\crypto\coin\wrapper;

use ErrorException;

class Btc extends \common\modules\crypto\coin\base\Btc {

	public function validateAddress($address) {
		$result = parent::validateAddress($address);
		if (isset($result['isvalid'])) {
			if ($result['isvalid'] == 1) {
				return true;
			} else {
				return false;
			}
		} else {
			throw new ErrorException('Error Processing Request: ' . print_r($result, true));
		}
	}

	public function estimatesmartfee($blockNum = 25) {
		$data = parent::estimateFee($blockNum);

		$feerate = $data['feerate'];
		$feerate = floor($feerate * 100000000) / 100000000;
		$feerate = rtrim(rtrim(number_format($feerate, 8, ".", ""), '0'), '.');

		return $feerate;
	}

	public function estimateFee($blockNum = 25) {
		$data = parent::estimateFee($blockNum);

		return round($data['feerate'] * 100000000 / 1024, 0);
	}

	public function getNewAddress($userId) {
		return ['currency_wallet' => parent::getNewAddress($userId)];
	}

	public function getBalanceByNode($userId, $confirmsNum = 0, $flag = false) {
		return parent::getBalance($userId, $confirmsNum, $flag);
	}
}