<?php

namespace common\modules\crypto\coin\wrapper;

class Bch extends \common\modules\crypto\coin\base\Btc {

	public function validateAddress($address) {
		$result = parent::validateAddress($address);
		if (isset($result['isvalid'])) {
			if ($result['isvalid'] == 1) {
				return true;
			} else {
				return false;
			}
		} else {
			throw new ErrorException('Error Processing Request: ' . print_r($result, true));
		}
	}

	public function estimatesmartfee($blockNum = 25) {

		$estimateFee = $this->altcoinClient->estimatefee();

		if (empty($this->altcoinClient->error)) {
			$feerate = $data['feerate'];
			$feerate = floor($feerate * 100000000) / 100000000;
			$feerate = rtrim(rtrim(number_format($feerate, 8, ".", ""), '0'), '.');

			return $feerate;
		} else {
			throw new ErrorException('Error Processing Request: ' . $this->altcoinClient->error);
		}
	}

	public function estimateFee($blockNum = 25) {

		$estimateFee = $this->altcoinClient->estimatefee();

		if (empty($this->altcoinClient->error)) {
			return round($estimateFee * 100000000 / 1024, 0);
		} else {
			throw new ErrorException('Error Processing Request: ' . $this->altcoinClient->error);
		}
	}

	public function getNewAddress($userId) {
		return ['currency_wallet' => str_replace('bitcoincash:', '', parent::getNewAddress($userId))];
	}

	public function getBalanceByNode($userId, $confirmsNum = 0, $flag = false) {
		return parent::getBalance($userId, $confirmsNum, $flag);
	}
}