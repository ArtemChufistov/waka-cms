<?php

namespace common\modules\crypto\coin\wrapper;

use common\models\Currency;
use common\modules\crypto\models\AccountWallet;

class Xrp extends \common\modules\crypto\coin\base\Xrp {

	public function getNewAddress($userId) {

		$currency = Currency::find()->where(['key' => Currency::KEY_XRP])->one();

		$accountWallet = AccountWallet::find()
			->where(['user_id' => $userId])
			->andWhere(['currency_type' => $currency->slug])
			->one();

		if (!empty($accountWallet)) {
			return [
				'ripple_validation_create' => $accountWallet->ripple_validation_create,
				'currency_wallet' => $accountWallet->currency_wallet,
				'wallet_keys' => $accountWallet->wallet_keys,
			];
		} else {
			$data = parent::getNewAddress($userId);
			return [
				'ripple_validation_create' => json_encode($data['validationResponse']),
				'currency_wallet' => $data['walletResponse']['account_id'],
				'wallet_keys' => json_encode($data['walletResponse']),
			];
		}
	}

	public function getBalanceByNode($userId, $confirmsNum = 0, $flag = false) {
		return 0;
	}
}