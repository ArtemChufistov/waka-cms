<?php

namespace common\modules\crypto\coin\wrapper;

class Bchsv extends \common\modules\crypto\coin\base\Btc {

	public function getNewAddress($userId) {
		$data = parent::getNewAddress($userId);
		if (!empty($data)) {
			return ['currency_wallet' => str_replace('bitcoincash:', '', $data)];
		} else {
			throw new ErrorException(Yii::t('app', 'Не получен ответ от ноды'));
		}
	}

	public function getBalanceByNode($userId, $confirmsNum = 0, $flag = false) {
		return parent::getBalance($userId, $confirmsNum, $flag);
	}
}