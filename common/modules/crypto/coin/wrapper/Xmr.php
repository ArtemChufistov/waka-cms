<?php

namespace common\modules\crypto\coin\wrapper;

class Xmr extends \common\modules\crypto\coin\base\Xmr {
	public function getNewAddress($userId) {
		return [
			'currency_wallet' => parent::getNewAddress($userId),
		];
	}

	public function getBalanceByNode($userId) {
		return parent::getBalance($userId);
	}

	public function validateAddress($address) {
		if (!preg_match('/([0-9]|[A-B])/', substr($address, 1)) || strlen($address) != 95) {
			return false;
		} else {
			return true;
		}
	}

	public function estimatesmartfee($blockNum = 1) {
		return 0;
	}
}