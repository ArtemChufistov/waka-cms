<?php

namespace common\modules\crypto\coin\wrapper;

use common\models\Currency;
use common\modules\crypto\models\AccountWallet;
use yii\httpclient\Client;

class Eth extends \common\modules\crypto\coin\base\Eth {
	public function getNewAddress($userId) {
		$data = parent::getNewAddress($userId);

		return [
			'currency_wallet' => $data['result'],
		];
	}

	public function getBalanceByNode($userId) {

		$currency = Currency::find()->where(['key' => Currency::KEY_ETH])->one();

		$accountWalletArray = AccountWallet::find()
			->where(['user_id' => $userId])
			->andWhere(['currency_type' => $currency->slug])
			->all();

		$balance = 0;
		foreach ($accountWalletArray as $accountWallet) {
			$balance += parent::getAddressBalance($accountWallet->currency_wallet);
		}

		return $balance;
	}

	public function validateAddress($address) {
		if (!preg_match("/^(?:0x)?[0-9a-fA-F]{40}$/i", $address) /*|| !self::isValidAndNotContract($address)*/) {
			return false;
		} else {
			return true;
		}
	}

	public function isValidAndNotContract($address) {
		$code = $this->getCode($address);
		if ($code) {
			if (isset($code['result'])) {
				if ($code['result'] == '0x') {
					return true;
				} else {
					return false;
					throw new ErrorException('Address mustn\'t be Contract address error raised');
				}
			}
		}
		return false;
		throw new ErrorException('Address not Valid error raised');
	}

	public function estimateFee($blockNum = 0) {
		$client = new Client();
		$response = $client->createRequest()
			->setMethod('get')
			->setUrl('https://ethgasstation.info/json/ethgasAPI.json')
			->send();
		if ($response->isOk) {
			$response = $response->data;

			if ($blockNum <= 1) {
				return $response['fastest'] / 10;
			} else {

				return $response['fast'] / 10;
			}
		} else {
			return 1;
		}
	}

	public function estimatesmartfee($blockNum = 0) {
		return 0;
	}
}