<?php

namespace common\modules\crypto\coin\factory;

use common\modules\crypto\helpers\CoinHelper;

class CoinFactory {
    const MODEL_NS = '\common\modules\explorer\models\\';

	public static function get($currency) {
		$config = CoinHelper::nodeConfig($currency);
		$classString = 'common\modules\crypto\coin\wrapper\\' . ucfirst(strtolower($currency));

		$objClass = new $classString();

		foreach ($config as $itemKey => $itemValue) {
			$objClass->$itemKey = $itemValue;
		}

		$objClass->init();

		return $objClass;
	}

    // Namespace класса
    public static function getNs($currency, $className) {
        return self::MODEL_NS . strtolower($currency) . "\\" . $className;
    }
}