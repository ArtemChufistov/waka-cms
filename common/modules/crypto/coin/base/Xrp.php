<?php

namespace common\modules\crypto\coin\base;

use common\modules\crypto\coin\clients\ripple\rpc\src\rippleWalletRPC;
use common\modules\crypto\coin\interfaces\NodeInterface;

class Xrp implements NodeInterface {

	const RIPPLE_DIGITS = 1000000;

	/**
	 * @var rippleWalletRPC
	 */
	private $rippleClient;

	public $currency;

	public $host;

	public $port;

	public function init() {
		$this->rippleClient = new rippleWalletRPC($this->host, $this->port);
	}

	public function getClient() {
		return $this->rippleClient;
	}

	public function getNewAddress($userId) {

		$validationResponse = $this->rippleClient->get_validation_create();
		$walletResponse = $this->rippleClient->get_wallet_propose($validationResponse['validation_seed']);

		return [
			'validationResponse' => $validationResponse,
			'walletResponse' => $walletResponse,
		];
	}

	public function getBalance($account_index = 0) {
		$params = array('account_index' => $account_index);
		return $this->_run('get_balance', $params);
	}

	public function getCurrentBlock() {
		return $this->rippleClient->get_ledger_current()['ledger_current_index'] ?? null;
	}

	public function getHighestBlockchainBlock() {
		$int = $this->getCurrentBlock();

		return is_int($int) ? $int : 0;
	}

	public function nodeInfo() {

		$data = $this->ethereumClient->call('eth_blockNumber', []);

		return $data;
	}
}