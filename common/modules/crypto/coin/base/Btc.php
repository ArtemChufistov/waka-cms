<?php

namespace common\modules\crypto\coin\base;

use common\modules\crypto\coin\clients\EasyBitcoin;
use common\modules\crypto\coin\interfaces\NodeInterface;
use ErrorException;

class Btc implements NodeInterface {

	/**
	 * @var EasyBitcoin
	 */
	protected $altcoinClient;

	public $username;

	public $password;

	public $host;

	public $port;

	public function init() {
		$this->altcoinClient = new EasyBitcoin($this->username, $this->password, $this->host, $this->port);
	}

	public function nodeInfo() {
		return $this->altcoinClient->getwalletinfo();
	}

	// Сгенерировать новый адрес
	public function getNewAddress($userId) {
		$address = $this->altcoinClient->getnewaddress((string) $userId);

		if (empty($this->altcoinClient->error)) {
			return $address;
		} else {
			throw new ErrorException('Error Processing Request: ' . $this->altcoinClient->error);
		}

	}

	// Проверить адрес
	public function validateAddress($address) {
		$result = $this->altcoinClient->validateaddress($address);

		if (empty($this->altcoinClient->error)) {
			return $result;
		} else {
			throw new ErrorException('Error Processing Request: ' . $this->altcoinClient->error);
		}
	}

	// Взять баланс аккаунта
	public function getBalance($userId, $confirmsNum = 0, $flag = false) {
		$balance = $this->altcoinClient->getbalance((string) $userId, (int) $confirmsNum, $flag);

		if (empty($this->altcoinClient->error)) {
			return $balance;
		} else {
			throw new ErrorException('Error Processing Request: ' . $this->altcoinClient->error);
		}
	}

	// Комиссия
	public function estimateFee($blockNum = 25) {
		$estimateFee = $this->altcoinClient->estimatesmartfee($blockNum);

		if (empty($this->altcoinClient->error)) {
			return $estimateFee;
		} else {
			throw new ErrorException('Error Processing Request: ' . $this->altcoinClient->error);
		}
	}

	// Взять данные по блоку
	public function getblock($blockNum) {
		$estimateFee = $this->altcoinClient->getblock($blockNum);

		if (empty($this->altcoinClient->error)) {
			return $estimateFee;
		} else {
			throw new ErrorException('Error Processing Request: ' . $this->altcoinClient->error);
		}
	}

	// Взять количество блоков
	public function getblockcount() {
		$estimateFee = $this->altcoinClient->getblockcount();

		if (empty($this->altcoinClient->error)) {
			return $estimateFee;
		} else {
			throw new ErrorException('Error Processing Request: ' . $this->altcoinClient->error);
		}
	}

	// Взять хэш блока по номеру
	public function getblockhash($blockNum) {
		$estimateFee = $this->altcoinClient->getblockhash($blockNum);

		if (empty($this->altcoinClient->error)) {
			return $estimateFee;
		} else {
			throw new ErrorException('Error Processing Request: ' . $this->altcoinClient->error);
		}
	}

	// Взять кошельки аккаунта
	public function getAddresses($userId, $confirmsNum = 0, $flag = false) {
		$wallets = $this->altcoinClient->getaddressesbyaccount((string) $userId);

		if (empty($this->altcoinClient->error)) {
			return $wallets;
		} else {
			throw new ErrorException('Error Processing Request: ' . $this->altcoinClient->error);
		}
	}

	public function verifychain() {
		$result = $this->altcoinClient->verifychain();

		if (empty($this->altcoinClient->error)) {
			return $result;
		} else {
			throw new ErrorException('Error Processing Request: ' . $this->altcoinClient->error);
		}
	}

	public function dumpprivkey($address) {
		$result = $this->altcoinClient->dumpprivkey($address);

		if (empty($this->altcoinClient->error)) {
			return $result;
		} else {
			throw new ErrorException('Error Processing Request: ' . $this->altcoinClient->error);
		}
	}

	public function dumpwallet() {
		$result = $this->altcoinClient->dumpwallet();

		if (empty($this->altcoinClient->error)) {
			return $result;
		} else {
			throw new ErrorException('Error Processing Request: ' . $this->altcoinClient->error);
		}
	}

	public function getrawmempool() {
		$result = $this->altcoinClient->getrawmempool();

		if (empty($this->altcoinClient->error)) {
			return $result;
		} else {
			throw new ErrorException('Error Processing Request: ' . $this->altcoinClient->error);
		}
	}

	public function listaccounts() {
		$result = $this->altcoinClient->listaccounts();

		if (empty($this->altcoinClient->error)) {
			return $result;
		} else {
			throw new ErrorException('Error Processing Request: ' . $this->altcoinClient->error);
		}
	}

	public function listreceivedbyaccount() {
		$result = $this->altcoinClient->listreceivedbyaccount();

		if (empty($this->altcoinClient->error)) {
			return $result;
		} else {
			throw new ErrorException('Error Processing Request: ' . $this->altcoinClient->error);
		}
	}

	public function listtransactions() {
		$result = $this->altcoinClient->listtransactions();

		if (empty($this->altcoinClient->error)) {
			return $result;
		} else {
			throw new ErrorException('Error Processing Request: ' . $this->altcoinClient->error);
		}
	}

	public function getrawtransaction($hash) {
		$result = $this->altcoinClient->getrawtransaction($hash);

		if (empty($this->altcoinClient->error)) {
			return $result;
		} else {
			throw new ErrorException('Error Processing Request: ' . $this->altcoinClient->error);
		}
	}

	public function decoderawtransaction($raw, $iswitness = false) {
	    if ($iswitness == true) {
            $result = $this->altcoinClient->decoderawtransaction($raw, $iswitness);
        }else{
            $result = $this->altcoinClient->decoderawtransaction($raw);
        }

		if (empty($this->altcoinClient->error)) {
			return $result;
		} else {
			throw new ErrorException('Error Processing Request: ' . $this->altcoinClient->error);
		}
	}

	public function validateAdditional() {
		return true;
	}
}