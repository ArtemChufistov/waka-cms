<?php

namespace common\modules\crypto\coin\base;

use common\modules\crypto\coin\clients\monerophp\src\walletRPC;
use common\modules\crypto\coin\interfaces\NodeInterface;
use ErrorException;

class Xmr implements NodeInterface {
	const MONERO_DIGITS = 1000000000000;

	private $moneroWalletRPCClient;

	public $currency;

	public $host;

	public $port;

	public function init() {
		$this->moneroWalletRPCClient = new walletRPC($this->host, $this->port);
	}

	public function getClient() {
		return $this->moneroWalletRPCClient;
	}

	public function nodeInfo() {

		$info = [];
		foreach ($this->moneroWalletRPCClient->get_all_wallet_accounts_transactions() as $key => $account) {
			$info[$key] = $this->moneroWalletRPCClient->get_balance($key)['balance'] / self::MONERO_DIGITS;
		}

		return $info;
	}

	public function getTransaction($txId) {
		$trans = $this->moneroWalletRPCClient->get_transfer_and_account_by_txid_pretty_like_bitcoin((string) $txId);
		return $trans;
	}

	public function getTransactionsList($userId) {
		return $this->moneroWalletRPCClient->get_all_wallet_accounts_transactions(1734249, true, $userId);
	}

	public function getHighestBlockchainBlock() {
		return $this->moneroWalletRPCClient->get_height()['height'] ?? 1685555;
	}

	public function getNewAddress($userId) {

		$address = $this->moneroWalletRPCClient->create_address((int) $userId)['address'];
		if ($address) {
			return $address;
		} else {
			throw new ErrorException('getnewaddress error: ' . $this->moneroWalletRPCClient->error);
		}
	}

	public function getBalance($userId, $confirmsNum = 0) {
		$data = $this->moneroWalletRPCClient->get_balance($userId);

		if ($data) {
			return $data['balance'] / self::MONERO_DIGITS;
		} else {
			throw new ErrorException('get balance error: ' . $this->moneroWalletRPCClient->error);
		}
	}

	public function getInfo() {
		$info = $this->moneroWalletRPCClient->getBalance();
		$info['balance'] /= self::MONERO_DIGITS;
		$info['total_balance'] = $info['balance'];
		$info['unlocked_balance'] /= self::MONERO_DIGITS;
		$info['balance'] = $info['unlocked_balance'];

		return $info;
	}

	public function getTxKey($txId) {
		$info = $this->moneroWalletRPCClient->get_tx_key($txId);

		return $info;
	}

	public function getPaymentId($address) {
		$data = $this->moneroClient->splitIntegratedAddress($address);
		$data = json_decode($data, true);

		return $data['payment_id'];
	}

	public function tagAccounts($tag, $accountId) {
		$data = $this->moneroClient->tagAccounts($tag, $accountId);
		$data = json_decode($data, true);

		return $data;
	}

	public function getPayments($paymentId) {
		$data = $this->moneroClient->getPayments($paymentId);
		$data = json_decode($data, true);

		return $data;
	}

	public function move($from_user_id, $amount, $to_user_id = 1, $comment = 'fee from ') {
		$txid = $this->moneroWalletRPCClient->transfer($amount, '82XmGbb7kf22NVvVAhTCeBFiZ4vYhnyC422k1yyVtBBGMSp34Vzubhdh4SVCkKoHLJ6Qv3F5LJnNjAH7ipzQ7fvKLXT2Uxt',
			'', 6, $from_user_id)['tx_hash'] ?? 'Err';

		if ($txid) {
			return $txid;
		} else {
			throw new ErrorException('move error: ');
		}
	}
}