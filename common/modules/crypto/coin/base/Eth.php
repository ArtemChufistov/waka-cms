<?php

namespace common\modules\crypto\coin\base;

use common\modules\crypto\coin\clients\RpcClient;
use common\modules\crypto\coin\interfaces\NodeInterface;
use ErrorException;
use yii\httpclient\Client;

class Eth implements NodeInterface {

	protected $ethereumClient;

	public $username;

	public $password;

	public $host;

	public $port;

	public $gas = 21000;

	public $gasPrice = 22000000000;

	const ETHER_DIGITS = 1000000000000000000;
	const PASS_PHRASE = 'oi+t3@rXWUdtDPKl5Mb4hgfjhfghfd9v1hXqbTvQWDeNnzA';

	public function init() {
		$this->ethereumClient = new RpcClient(new Client(['baseUrl' => 'http://' . $this->host . ':' . $this->port]));
	}

	public function getClient() {
		return $this->ethereumClient;
	}

	public function getCurrentBlock() {
		$data = $this->ethereumClient->call('eth_blockNumber', []);
		if (isset($data['error'])) {
			throw new ErrorException('eth_blockNumber error raised: ' . $data['error']['message']);
		}
		return hexdec($data['result']);
	}

	public function nodeInfo() {
		return $this->ethereumClient->call('eth_blockNumber', []);
	}

	public function getPendingTransaction($from = null, $to = null) {
		return $this->ethereumClient->call('eth_pendingTransactions', []);
	}

	public function getDefaultTransactionFee() {
		return $this->gasPrice * $this->gas;
	}

	public function getNewAddress($userId) {
		$data = $this->ethereumClient->call('personal_newAccount', [self::pass($userId)]);
		if (isset($data['error'])) {
			throw new ErrorException('personal_newAccount error raised: ' . $data['error']['message']);
		}

		return $data;
	}

	public function getAddressBalance($address) {
		$data = $this->ethereumClient->call('eth_getBalance', [(string) $address, 'latest']);

		if (isset($data['error'])) {
			throw new ErrorException('eth_getBalance error raised: ' . $data['error']['message']);
		}

		$walletBalance = $data['result'];

		return $walletBalance;
	}

	public function getCode($address = null, $quantity = 'latest') {
		$params = [
			(string) $address,
			$quantity,
		];

		$data = $this->ethereumClient->call('eth_getCode', $params);
		if (isset($data['error'])) {
			throw new ErrorException('eth_getCode error raised: ' . $data['error']['message']);
		}

		return $data;
	}

	public static function pass($userId) {
		return (string) md5($userId . self::PASS_PHRASE);
	}
}