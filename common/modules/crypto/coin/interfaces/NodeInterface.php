<?php

namespace common\modules\crypto\coin\interfaces;

interface NodeInterface {
	public function nodeInfo();

	public function getNewAddress($userId);
}