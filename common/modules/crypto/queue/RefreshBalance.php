<?php

namespace common\modules\crypto\queue;

use app\modules\merchant\models\MAccountBalanceLog;
use Yii;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class RefreshBalance extends BaseObject implements JobInterface {
	public $currency;
	public $user_id;

	public function execute($queue) {

		echo $this->currency . " - user " . $this->user_id . " ok!\n";
		$balance = Yii::$app->get($this->currency)->getAddressesBalanceSum($this->user_id, 0, false, false, true);

		echo "Баланс: " . $balance . " - $this->currency \n";

		$balance_log_model = new MAccountBalanceLog();
		$balance_log_model->user_id = $this->user_id;
		$balance_log_model->currency = $this->currency;
		$balance_log_model->balance = $balance;
		$balance_log_model->created_at = date("Y-m-d H:i:s");
		$balance_log_model->save();

	}
}