<?php

namespace common\modules\crypto\helpers;

use common\modules\crypto\models\CoinmarketcapListing;
use Yii;

class CoinHelper {
	const COIN_LOGO_URL = '/coins';

	public static function logoUrl($currencyKey, $size = 200) {
		$model = CoinmarketcapListing::find()->where(['symbol' => $currencyKey])->one();

		return self::COIN_LOGO_URL . '/' . $size . 'x' . $size . '/' . $model->id . '.png';
	}

	public static function nodeConfig($currencyKey) {
		$config = Yii::$app->params['crypto'];
		return $config[$currencyKey];
	}

	public static function makeSalted($secret) {
		return hash('sha256', md5($secret . 'be_cool'));
	}
}