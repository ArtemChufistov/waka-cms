<?php

namespace common\modules\crypto;

use Yii;

/**
 * Class Module
 * @package common\modules\crypto
 */
class Module extends \yii\base\Module
{

    public function init()
    {
        parent::init();
    }
}
