<?php

namespace common\modules\crypto\commands;

use common\models\Currency;
use common\modules\crypto\models\CourseChart;
use yii\httpclient\Client;

class Chart {

	public function refreshPoloniexCourse() {

		$currencyArray = Currency::find()->where(['active' => Currency::ACTIVE_TRUE])->all();

		foreach ($currencyArray as $currency) {

			$time = time();
			$roundedTime = $time - ($time % (30 * 60));

			$data = [
				'command' => 'returnChartData',
				'currencyPair' => 'USDT_' . $currency->key,
				'start' => $roundedTime - (60 * 60 * 24 * 2),
				'end' => $roundedTime,
				'period' => 1800,
			];

			$client = new Client();
			$response = $client->createRequest()
				->setMethod('get')
				->setUrl('https://poloniex.com/public?' . http_build_query($data))
				->send();

			if ($response->isOk) {
				foreach ($response->data as $item) {
					if (empty($item['date'])) {
						echo 'Для валюты: ' . $currency->key . ' непонятный ответ от сервера: ' . $item . "\r\n";
					} else {
						$courseChart = CourseChart::find()
							->where(['date' => $item['date']])
							->andWhere(['currency_id_from' => $currency->id])
							->one();

						if (empty($courseChart)) {
							$courseChart = new CourseChart;
							$courseChart->setAttributes($item);
							$courseChart->currency_id_from = $currency->id;
							$courseChart->save(false);
							echo 'Добавление данных по валюте: ' . $currency->key . ' о курсах с Poloniex за: ' . date('Y-m-d H:i:s', $item['date']) . "\r\n";
						}
					}
				}
			} else {
				echo 'Ошибка получения данных с Poloniex' . "\r\n";
				print_r($response);
			}
		}
	}
}