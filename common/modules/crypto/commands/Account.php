<?php

namespace common\modules\crypto\commands;

use common\models\Currency;
use common\modules\crypto\coin\factory\CoinFactory;
use common\modules\crypto\models\AccountBalance;

class Account {

	public function syncUserBalance($userId) {
		$currencyArray = Currency::find()
			->where(['active' => Currency::ACTIVE_TRUE])
			->all();

		foreach ($currencyArray as $currency) {
			$balance = CoinFactory::get($currency->key)->getBalanceByNode($userId);

			$accountWallet = AccountBalance::find()
				->where(['user_id' => $userId])
				->andWhere(['currency' => $currency->slug])
				->one();

			if (!empty($accountWallet)) {

				echo 'Для юзера ID: ' . $userId . ' валюта: ' . $currency->key . ' баланс: ' . $balance . "\r\n";

				$accountWallet->balance = $balance;
				$accountWallet->save(false);
			}
		}
	}
}