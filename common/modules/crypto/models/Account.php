<?php

namespace common\modules\crypto\models;

use common\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "m_account".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $secret
 * @property string $created_at
 * @property string $updated_at
 * @property string $comment
 * @property string $url
 * @property int $is_delete
 * @property int $is_turn_on
 * @property int $crypchant_merchant_get_fee_automatically
 * @property float $crypchant_merchant_fee
 * @property object $image
 *
 * @property User $user
 * @property [] $allActiveUserMerchants
 */
class Account extends \yii\db\ActiveRecord {

	public $image;

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'm_account';
	}

	public function behaviors() {
		return [
			'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
				'value' => new Expression('NOW()'),
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				// 'value' => 0,
				'createdByAttribute' => 'user_id',
				'updatedByAttribute' => 'user_id',

			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['name', 'url', 'comment', 'secret', 'is_turn_on'], 'required'],
			[['user_id', 'is_delete'], 'integer'],
			[['created_at', 'updated_at', 'url', 'comment', 'image', 'crypchant_merchant_fee', 'crypchant_merchant_get_fee_automatically'], 'safe'],
			[['crypchant_merchant_fee'], 'number', 'max' => 0.01],
			[['url'], 'url', 'defaultScheme' => ''],
			[['name'], 'string', 'max' => 100],
			[['secret'], 'string', 'min' => 6, 'max' => 255],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
			[['image'], 'file' /*, 'skipOnEmpty' => true*/, 'extensions' => 'png, jpg'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'user_id' => 'Пользователь',
			'name' => 'Название магазина',
			'secret' => 'Секретный Ключ',
			'created_at' => 'Создан',
			'updated_at' => 'Обновлен',
			'is_delete' => 'Is Delete',
			'is_turn_on' => 'Включен',
			'comment' => 'Описание магазина',
			'url' => 'URL магазина',
			'image' => 'Файл картинки',
			'crypchant_merchant_fee' => 'Коммисия',
			'crypchant_merchant_get_fee_automatically' => 'Снимать комисию автоматически',
		];
	}

	public function makeSalted() {
		$this->secret = MerchantHelper::makeSalted($this->secret);

		return true;
	}

	public function upload() {
		if ($this->validate() && isset($this->image)) {
			$this->image->saveAs('/m-logo/{$this->id}.png');
		} else {
			return false;
		}
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAccountAllowIps() {
		return $this->hasMany(AccountAllowIp::className(), ['m_account_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAccountApiRequestLogs() {
		return $this->hasMany(AccountApiRequestLog::className(), ['m_account_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAccountWallets() {
		return $this->hasMany(AccountWallet::className(), ['m_account_id' => 'id']);
	}
}