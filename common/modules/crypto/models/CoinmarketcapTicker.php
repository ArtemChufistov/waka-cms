<?php

namespace common\modules\crypto\models;

use Yii;

/**
 * This is the model class for table "m_coinmarketcap_ticker".
 */
class CoinmarketcapTicker extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'm_coinmarketcap_ticker';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['id', 'name'], 'required'],
			[['name', 'symbol'], 'string', 'max' => 100],
			[['rank', 'price_usd', 'price_btc', 'h24h_volume_usd', 'market_cap_usd', 'available_supply', 'total_supply', 'max_supply', 'percent_change_1h', 'percent_change_24h', 'percent_change_7d', 'last_updated'], 'safe'],
			[['id'], 'unique'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'name' => 'Name',
			'symbol' => 'Symbol',
			'rank' => 'Rank',
			'price_usd' => 'Price USD',
			'price_btc' => 'Price BTC',
			'h24h_volume_usd' => 'H 24 volume USD',
			'market_cap_usd' => 'Market Cap USD',
			'available_supply' => 'Available Supply',
			'total_supply' => 'Total Supply',
			'max_supply' => 'Max supply',
			'percent_change_1h' => 'Percent change 1 H',
			'percent_change_24h' => 'Percent change 24 H',
			'percent_change_7d' => 'Percent change 7 D',
			'last_updated' => 'Last Updated',
		];
	}
}