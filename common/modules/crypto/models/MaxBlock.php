<?php

namespace common\modules\crypto\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "m_account".
 *
 * @property int $id
 * @property int $currency
 * @property int $block_height
 * @property int $updated_ad
 */
class MaxBlock extends \yii\db\ActiveRecord {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'm_max_block';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['currency', 'block_height', 'updated_at'], 'safe'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'currency' => 'Валюта',
			'block_height' => 'Номер блока',
			'updated_at' => 'Дата обновления',
		];
	}
}