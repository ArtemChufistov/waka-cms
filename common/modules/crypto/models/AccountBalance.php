<?php

namespace common\modules\crypto\models;

use common\models\Currency;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user_balance".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $currency_id
 * @property integer $amount
 * @property integer $updated_at
 *
 * @property Currency $currency
 * @property User $user
 */
class AccountBalance extends \yii\db\ActiveRecord {

	public function behaviors() {
		return [
			'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['updated_at'],
				],
				'value' => new Expression('NOW()'),
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'm_account_balance';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['user_id', 'currency'], 'required'],
			[['user_id', 'currency', 'amount'], 'integer'],
			[['currency'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency' => 'id']],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'user_id' => Yii::t('app', 'User ID'),
			'currency' => Yii::t('app', 'Currency'),
			'balance' => Yii::t('app', 'Balance'),
			'updated_at' => Yii::t('app', 'Updated At'),
		];
	}

	public static function getActiveForUserAddNot($userId) {

		$accountBalanceArray = self::getActiveForUser($userId);

		$currencyKeys = ArrayHelper::map($accountBalanceArray, 'id', 'currency');

		$currencyArray = Currency::find()
			->where(['NOT IN', 'slug', $currencyKeys])
			->andWhere(['active' => Currency::ACTIVE_TRUE])
			->orderBy(['sort' => SORT_DESC])
			->all();

		if (!empty($currencyArray)) {
			foreach ($currencyArray as $currency) {
				$accountBalance = new AccountBalance;
				$accountBalance->user_id = $userId;
				$accountBalance->currency = $currency->slug;
				$accountBalance->balance = 0;

				$accountBalance->save(false);
			}

			$accountBalanceArray = self::getActiveForUser($userId);
		}

		return $accountBalanceArray;
	}

	public static function getActiveForUser($userId) {
		return self::find()
			->joinWith([Currency::tableName()])
			->where([AccountBalance::tablename() . '.user_id' => $userId])
			->andWhere([Currency::tableName() . '.active' => Currency::ACTIVE_TRUE])
			->orderBy([Currency::tableName() . '.sort' => SORT_DESC])
			->all();
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCurrency() {
		return $this->hasOne(Currency::className(), ['slug' => 'currency']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}
}
