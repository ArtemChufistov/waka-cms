<?php

namespace common\modules\crypto\models;

use common\models\Currency;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

class Transaction extends \yii\db\ActiveRecord {

	const CATEGORY_SEND = 'send';
	const CATEGORY_RECEIVE = 'receive';

	const IS_SHOW_TRUE = 1;
	const IS_SHOW_FALSE = 0;

	const IS_IN_PROCESSING = 0;
	const IS_COMPLETE = 1;

	const VIRTUAL_STATUS_NEW = 'new';
	const VIRTUAL_STATUS_UNKNOWN = 'unknown';
	const VIRTUAL_STATUS_IN_WORK = 'in_work';
	const VIRTUAL_STATUS_COMPLETE = 'complete';

	public function behaviors() {
		return [
			'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'blockchain_time'],
				],
				'value' => new Expression('NOW()'),
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'm_nodes_transactions';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['is_show', 'user_id', 'currency'], 'required'],
			[['block_number', 'm_payment_task_id', 'is_processing_complete', 'm_account_id', 'category', 'amount', 'fee', 'crypchant_fee', 'status_crypchant_fee'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'is_show' => Yii::t('app', 'Отображать'),
			'block_number' => Yii::t('app', 'Номер блока'),
			'm_payment_task_id' => Yii::t('app', 'Payment task id'),
			'is_processing_complete' => Yii::t('app', 'Обработка завершена'),
			'user_id' => Yii::t('app', 'Пользователь'),
			'm_account_id' => Yii::t('app', 'Аккаунт'),
			'currency' => Yii::t('app', 'Валюта'),
			'category' => Yii::t('app', 'Категория'),
			'amount' => Yii::t('app', 'Сумма'),
			'fee' => Yii::t('app', 'Комиссия'),
			'crypchant_fee' => Yii::t('app', 'Комиссия системы'),
			'status_crypchant_fee' => Yii::t('app', 'Статус комиссии системы'),
			'm_fee_pay_task_id' => Yii::t('app', 'Fee pay task id'),
			'send_msg_status' => Yii::t('app', 'Send message status'),
			'from_addr' => Yii::t('app', 'From address'),
			'to_addr' => Yii::t('app', 'To address'),
			'txid' => Yii::t('app', 'Хэш транзакции'),
			'payment_id' => Yii::t('app', 'Payment'),
			'destination_tag' => Yii::t('app', 'Destination tag'),
			'ripple_result' => Yii::t('app', 'Ripple result'),
			'ripple_tx_result' => Yii::t('app', 'Ripple tx result'),
			'confirmations' => Yii::t('app', 'Confirmations'),
			'created_at' => Yii::t('app', 'Created at'),
			'blockchain_time' => Yii::t('app', 'Blockchain time'),
		];
	}

	public static function getVirtualStatusArray() {
		return [
			self::VIRTUAL_STATUS_NEW => Yii::t('app', 'Новый'),
			self::VIRTUAL_STATUS_UNKNOWN => Yii::t('app', 'Неизвестный'),
			self::VIRTUAL_STATUS_IN_WORK => Yii::t('app', 'Обрабатывается'),
			self::VIRTUAL_STATUS_COMPLETE => Yii::t('app', 'Завершён'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	public function getCurrency() {
		return $this->hasOne(Currency::className(), ['slug' => 'currency']);
	}

	public function getPaymentTask() {
		return $this->hasOne(PaymentTask::className(), ['id' => 'm_payment_task_id']);
	}
}
