<?php

namespace common\modules\crypto\models;

use Yii;

/**
 * This is the model class for table "course_chart".
 *
 * @property int $id
 * @property double $high
 * @property double $low
 * @property double $open
 * @property double $close
 * @property double $volume
 * @property double $quoteVolume
 * @property double $weightedAverage
 * @property int $date
 */
class CourseChart extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'course_chart';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['high', 'low', 'open', 'close', 'volume', 'quoteVolume', 'weightedAverage'], 'number'],
			[['date'], 'integer'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'high' => 'High',
			'low' => 'Low',
			'open' => 'Open',
			'close' => 'Close',
			'volume' => 'Volume',
			'quoteVolume' => 'Quote Volume',
			'weightedAverage' => 'Weighted Average',
			'date' => 'Date',
		];
	}
}
