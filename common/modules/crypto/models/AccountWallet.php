<?php

namespace common\modules\crypto\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "m_account_wallet".
 *
 * @property int $id
 * @property int $m_account_id
 * @property int $user_id
 * @property string $currency_wallet
 * @property string $currency_type
 * @property string $ripple_validation_create
 * @property string $wallet_keys
 * @property double $balance
 * @property int $created_at
 *
 * @property Account $account
 * @property LbUser $user
 */
class AccountWallet extends ActiveRecord {
	public static function tableName() {
		return 'm_account_wallet';
	}

	public function behaviors() {
		return [
			'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
				],
				'value' => new Expression('NOW()'),
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['currency_wallet', 'currency_type', 'user_id'], 'required'],
			[['ripple_validation_create', 'wallet_keys'], 'safe'],
			[['m_account_id', 'created_at'], 'integer'],
			[['balance'], 'number'],
			[['currency_wallet'], 'string', 'max' => 255],
			[['currency_type'], 'string', 'max' => 50],
			[['m_account_id'], 'exist', 'skipOnError' => true, 'targetClass' => Account::className(), 'targetAttribute' => ['m_account_id' => 'id']],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'm_account_id' => 'Аккаунт',
			'currency_wallet' => 'Кошелек',
			'currency_type' => 'Валюта',
			'balance' => 'Баланс',
			'created_at' => 'Дата создания',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAccount() {
		return $this->hasOne(Account::className(), ['id' => 'm_account_id']);
	}
}
