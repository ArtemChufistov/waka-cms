<?php
return [
	'aliases' => [
		'@bower' => '@vendor/bower-asset',
		'@npm' => '@vendor/npm-asset',
	],
	'language' => 'ru_RU',
	'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
	'components' => [
		'authManager' => [
			'class' => 'yii\rbac\DbManager',
		],
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'i18n' => [
			'translations' => [
				'*' => [
					'class' => 'yii\i18n\DbMessageSource',
					'forceTranslation' => true,
					'sourceMessageTable' => '{{%source_message}}',
					'messageTable' => '{{%message}}',
					'enableCaching' => false,
					'cachingDuration' => 3600,
					'sourceLanguage' => 'en_US',
				],
			],
		],
		'queue' => [
			'class' => \yii\queue\db\Queue::class,
			'db' => 'db',
			'tableName' => '{{%queue}}',
			'channel' => 'default',
			'mutex' => \yii\mutex\MysqlMutex::class,
		],
        'elasticsearch' => [
            'class' => 'yii\elasticsearch\Connection',
            'nodes' => [
                ['http_address' => '172.17.0.7:9200'],
            ],
        ],

    ],
];
