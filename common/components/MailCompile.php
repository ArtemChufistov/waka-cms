<?php
/**
 * Created by PhpStorm.
 * User: admins
 * Date: 31.08.2017
 * Time: 18:08
 */

namespace common\components;

use common\models\Mail;
use common\models\MailTemplate;
use common\models\Preference;
use Yii;

class MailCompile {
	public static function renderMail($to, $type, $data = [], $language = 'ru_RU') {
		$template = MailTemplate::findOne(['type' => $type, 'language' => $language]);

		$body = $template->content;
		$headers = $template->headers;
		foreach ($data as $name => $item) {
			$body = str_replace('{{' . $name . '}}', $item, $body);
			$headers = str_replace('{{' . $name . '}}', $item, $headers);
		}

		$from = Preference::find()->where(['key' => Preference::KEY_SEND_MAIL_USERNAME])->one()->value;

		$mail = new Mail();
		$mail->headers = $headers;
		$mail->content = $body;
		$mail->from = $from;
		$mail->to = $to;
		$mail->status = Mail::STATUS_CREATE;
		$mail->save();
	}

	public static function sendMail() {

		$host = Preference::find()->where(['key' => Preference::KEY_SEND_MAIL_HOST])->one()->value;
		$username = Preference::find()->where(['key' => Preference::KEY_SEND_MAIL_USERNAME])->one()->value;
		$password = Preference::find()->where(['key' => Preference::KEY_SEND_MAIL_PASSWORD])->one()->value;
		$port = Preference::find()->where(['key' => Preference::KEY_SEND_MAIL_PORT])->one()->value;
		$encryption = Preference::find()->where(['key' => Preference::KEY_SEND_MAIL_ENCRYPTION])->one()->value;

		$transport = [
			'class' => 'Swift_SmtpTransport',
			'host' => $host,
			'username' => $username,
			'password' => $password,
			'port' => $port,
			'encryption' => $encryption,
		];

		if ($mail = Mail::findOne(['status' => Mail::STATUS_CREATE])) {
			try {
				$mailer = Yii::$app->mailer;
				$mailer->setTransport($transport);

				$result = Yii::$app->mailer->compose()
					->setTo($mail->to)
					->setFrom($mail->from)
					->setSubject($mail->headers)
					->setHtmlBody($mail->content)
					->send();

				$mail->status = Mail::STATUS_SEND;
				$mail->save();

				$mailer->getTransport()->stop();
				unset($mailer);

				return $mail;
			} catch (Swift_TransportException $e) {
				return $e->getMessage();
			}
		}
	}
}