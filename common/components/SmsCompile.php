<?php

namespace common\components;

use common\models\Sms;
use common\models\Preference;
use common\components\Smsru;

class SmsCompile {
    public static function add($phone, $text, $userId = 0) {
        $sms = new Sms;
        $sms->phone = $phone;
        $sms->text = $text;
        $sms->user_id = $userId;
        $sms->status = Sms::STATUS_NEW;
        $sms->date = date('Y-m-d H:i:s');
        $sms->save();
    }

    public static function send() {
        $sms = Sms::find()->where(['status' => Sms::STATUS_NEW])->one();

        if (empty($sms)){
            return;
        }

        $smsru = new Smsru('79ab08af-4e4e-0ea4-11f9-e7b929b67f56');
        $data = new \stdClass();
        $data->to = $sms->phone;
        $data->text = $sms->text;
        $result = $smsru->send_one($data);

        print_r($result);

        $sms->status = Sms::STATUS_SEND;
        $sms->save(false);
    }
}