<?php

class Crypchant {

	public $url = 'https://crypchant.com/api/v1';
	public $merchantId;
	public $merchantSecret;

	function __construct($merchantId, $merchantSecret) {
		$this->merchantId = $merchantId;
		$this->merchantSecret = $merchantSecret;
	}

	// Create transaction
	public function sendFromMerchant($curency, $toAddress, $amount, $fee = false, $feePaySender = true) {
		$options = [
			"currency" => $curency,
			"command" => 'sendFromMerchant',
			"toAddress" => $toAddress,
			"amount" => $amount,
			"feePaySender" => $feePaySender,
			"fee" => $fee,
		];

		return $this->sendRequest($options);
	}

	// Generate wallet
	public function generateAddress($currency) {

		$options = [
			"currency" => $currency,
			"command" => 'generateAddress',
		];

		return $this->sendRequest($options);
	}

	// List transactions
	public function getTransactionsList($currency, $count = 20, $pageNum = 0) {

		$options = [
			"currency" => $currency,
			"command" => 'getTransactionsList',
			"count" => $count,
			"page_num" => $pageNum,
		];

		return $this->sendRequest($options);
	}

	// List wallets
	public function showAddresses($currency, $merchantOnly = false, $count = 20, $pageNum = 0) {

		$options = [
			"currency" => $currency,
			"command" => 'showAddresses',
			"merchant_only" => $merchantOnly,
			"count" => $count,
			"page_num" => $pageNum,
		];

		return $this->sendRequest($options);
	}

	// Account balance
	public function getAddressesBalanceSum($currency, $countConfirms = 1) {

		$options = [
			"currency" => $currency,
			"command" => 'getAddressesBalanceSum',
			"confirmations_num" => $countConfirms,
		];

		return $this->sendRequest($options);
	}

	// Currency list
	public function getCurrenciesList() {
		$options = [
			"command" => 'getCurrenciesList',
		];

		return $this->sendRequest($options);
	}

	// Default fees list
	public function getDefaultFeesList() {
		$options = [
			"command" => 'getDefaultFeesList',
		];

		return $this->sendRequest($options);
	}

	// Send POST to merchant
	public function sendRequest($options) {

		$params = [
			'merchantId' => $this->merchantId,
			'secret' => $this->merchantSecret,
			'options' => $options,
		];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->url,
			CURLOPT_POST => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POSTFIELDS => http_build_query($params),
		)
		);

		$result = curl_exec($curl);
		curl_close($curl);

		return json_decode($result, true);
	}

	// Send JSON to merchant
	public function sendJsonRequest($options) {

		$params = [
			'merchantId' => $this->merchantId,
			'secret' => $this->merchantSecret,
			'options' => $options,
		];

		$params = json_encode($params);

		$curl = curl_init();

		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($params))
		);

		$result = curl_exec($curl);

		curl_close($curl);

		return json_decode($result, true);
	}
}