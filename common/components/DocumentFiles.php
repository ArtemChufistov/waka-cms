<?php
/**
 * Created by PhpStorm.
 * User: admins
 * Date: 31.08.2017
 * Time: 18:08
 */

namespace common\components;

use common\models\Document;
use Yii;
use yii\helpers\FileHelper;

class DocumentFiles {

	public static function createDir($dir) {
		if (!is_dir($dir)) {
			FileHelper::createDirectory($dir, 0777);
		}
	}

	public static function saveFile($user_id, $type, $filename, $data) {
		$dir = Yii::getAlias('@common' . Yii::$app->params['pathFile'] . "/$user_id/$type/");
		self::createDir($dir);
		file_put_contents("$dir$filename", $data);
		return true;
	}

	public static function saveImageVerify($userId, $type, $data) {
		if (!($model = Document::find()->where(['user_id' => $userId, 'type' => $type])->one())) {
			$model = new Document();
			$model->type = $type;
			$model->user_id = $userId;
		}

		$model->verify = 0;
		$model->save();

		$filename = $model->id . ".jpg";
		$dir = Yii::getAlias('@common' . Yii::$app->params['pathFile'] . "/$userId/$type");
		self::createDir($dir);

		if (move_uploaded_file($data["tmp_name"], "$dir/$filename")) {
			$extType = pathinfo("$dir/$filename", PATHINFO_EXTENSION);
			$data = file_get_contents("$dir/$filename");
			return base64_encode($data);
		} else {
			return false;
		}
	}

	public static function getFile($user_id, $type, $filename) {
		$dir = FileHelper::normalizePath(\Yii::getAlias('@common' . Yii::$app->params['pathFile'] . "/$user_id/$type/"));
		if (is_file("$dir/$filename")) {
			return file_get_contents("$dir/$filename");
		}
		return false;
	}

	public static function getFileImage($user_id, $type, $filename) {
		$dir = FileHelper::normalizePath(\Yii::getAlias('@common' . Yii::$app->params['pathFile'] . "/$user_id/$type/"));
		if (is_file("$dir/$filename")) {
			$type = pathinfo("$dir/$filename", PATHINFO_EXTENSION);
			$data = file_get_contents("$dir/$filename");
			return 'data:image/' . $type . ';base64,' . base64_encode($data);
		}
		return false;
	}
}