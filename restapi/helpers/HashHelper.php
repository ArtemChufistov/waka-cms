<?php

namespace restapi\helpers;

use Yii;

class HashHelper
{
    public static function generate($dataArray)
    {
        return md5(hash('sha256', implode('', $dataArray)));
    }
}
