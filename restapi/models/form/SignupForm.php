<?php

namespace restapi\models\form;

use common\components\MailCompile;
use common\models\MailTemplate;
use restapi\models\User;
use Yii;
use yii\base\Model;
use yii\httpclient\Client;

/**
 * Signup form
 */
class SignupForm extends Model {
	public $username;
	public $email;
	public $password;
	public $passwordRepeat;
	public $recaptcha;
	public $ref;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
            [['ref'], 'safe'],
            ['recaptcha', 'trim'],
            ['recaptcha', 'required'],
            ['recaptcha', 'string', 'max' => 512],
            [['username', 'email', 'password', 'passwordRepeat'], 'required'],
            ['passwordRepeat', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t('app', 'Пароли не совпадают')],
            ['email', 'trim'],
            ['email', 'email'],
			['email', 'string', 'max' => 255],
			['email', 'unique', 'targetClass' => User::className(), 'message' => Yii::t('app', 'Этот E-mail уже занят')],
		];
	}

	public function attributeLabels() {
		return [
			'username' => Yii::t('app', 'Имя пользователя'),
			'email' => Yii::t('app', 'E-mail'),
			'password' => Yii::t('app', 'Пароль'),
			'passwordRepeat' => Yii::t('app', 'Повтор пароля'),
		];
	}

	/**
	 * Signs user up.
	 *
	 * @return bool|null|User
	 */
	public function signup() {
		if (!$this->validate()) {
			return;
		}

		$client = new Client();
		$response = $client->createRequest()
			->setMethod('post')
			->setUrl('https://www.google.com/recaptcha/api/siteverify')
			->setData([
				'secret' => \Yii::$app->params['recaptcha'],
				'response' => $this->recaptcha,
			])
			->send();
		if (!$response->isOk) {
			return;
		}

		$user = new User();
		$user->step_auth = User::STEP_AUTH_FALSE;
		$user->username = $this->username;
		$user->email = $this->email;
		$user->password = $this->password;
		$user->generateAuthKey();

		$parentUser = '';
		if (!empty($this->ref)) {
			$parentUser = User::find()->where(['username' => $this->ref])->one();
		}

		if (!empty($parentUser)) {
			$user->parent_id = $parentUser->id;
		} else {
			$adminUser = User::find()->orderBy(['id' => SORT_ASC])->one();
			$user->parent_id = $adminUser->id;
		}

		if ($user->save()) {
			if ($user_role = \Yii::$app->authManager->getRole('user')) {
				\Yii::$app->authManager->assign($user_role, $user->id);
			}

			$user->appendToParent();

			MailCompile::renderMail($this->email, MailTemplate::TYPE_REGISTRATION_SUCCESS_CONFIRM, $this->attributes);
			return $user;
		}

		$this->addError('email', print_r($user->getErrors(), true));
		return $user->getErrors();
	}
}
