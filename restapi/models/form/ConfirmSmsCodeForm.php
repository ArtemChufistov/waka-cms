<?php

namespace restapi\models\form;

use Yii;
use common\components\SmsCompile;
use common\models\MailTemplate;
use yii\base\Model;
use common\models\Preference;
use restapi\models\User;
use yii\web\UnauthorizedHttpException;

/**
 * Reset form
 */
class ConfirmSmsCodeForm extends Model {
    public $phone;
    public $smsConfirmationCode;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['phone', 'required'],
            ['smsConfirmationCode', 'required'],
            ['smsConfirmationCode', 'integer']
        ];
    }

    /**
     * Finds user by [[username]]
     * @return null|User
     * @throws UnauthorizedHttpException
     */
    protected function getUser()
    {
        $user = Yii::$app->user->identity;
        if (!$user) {
            throw new UnauthorizedHttpException();
        }

        if (isset($user->id)) {
            return User::findOne(['id' => $user->id]);
        } else {
            return null;
        }
    }

    public function create() {
        if (!$this->validate()) {
            return false;
        }

        $user = $this->getUser();
        if (!$user)
            return false;;

        $user->phone = $this->phone;
        $user->sms_confirmation_code = $this->smsConfirmationCode;

        if ($user->save()) {
            SmsCompile::add($user->phone, Yii::t('app', 'Confirmation code: {code}', ['code' => $user->sms_confirmation_code]), $user->id);
            return $user;
        }

        return false;;
    }

    /**
     * Resets confirmation code
     *
     * @return bool whether the account is updated
     */
    public function reset() {
        $user = $this->getUser();
        if (!$user)
            return;

        $user->email_confirmation_code = null;

        $user->save();
        return $user;
    }

    /**
     * Validate confirmation code and update email
     *
     * @return bool whether the account is updated
     */
    public function update() {
        if (!$this->validate()) {
            return;
        }

        $user = $this->getUser();
        if (!$user)
            return;

        if ($user->email_confirmation_code != $this->emailConfirmationCode)
            return;

        $user->username = $this->email;
        $user->email = $this->email;
        $user->email_confirmation_code = null;

        $user->save();
        return $user;
    }
}
