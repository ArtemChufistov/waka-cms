<?php

namespace restapi\models\form;

use common\components\MailCompile;
use common\models\MailTemplate;
use restapi\models\User;
use Yii;
use yii\base\Model;
use yii\httpclient\Client;

/**
 * Reset form
 */
class ResetForm extends Model {
	public $email;
	public $recaptcha;

	private $_user;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			['recaptcha', 'trim'],
			['recaptcha', 'required'],
			['recaptcha', 'string', 'max' => 5120],
			['email', 'trim'],
			['email', 'required'],
			['email', 'email'],
			['email', 'string', 'max' => 255],
			['email', 'validateEmail'],
		];
	}

	/**
	 * Finds user by [[username]]
	 *
	 * @return User|null
	 */
	protected function getUser() {
		if ($this->_user === null) {
			$this->_user = User::find()->where('email = :email', [':email' => $this->email])->one();
		}

		return $this->_user;
	}

	public function validateEmail($attribute, $params) {
		if (!$this->hasErrors()) {
			$user = $this->getUser();
			if (!$user) {
				$this->addError($attribute, Yii::t('app', 'Пользователь с таким E-mail не найден'));
			}
		}
	}

	/**
	 * Resets user's password.
	 *
	 * @return bool whether the account is found
	 */
	public function reset() {
		if (!$this->validate()) {
			return false;
		}

		$client = new Client();
		$response = $client->createRequest()
			->setMethod('post')
			->setUrl('https://www.google.com/recaptcha/api/siteverify')
			->setData([
				'secret' => \Yii::$app->params['recaptcha'],
				'response' => $this->recaptcha,
			])
			->send();
		if (!$response->isOk) {
			return false;
		}

		$user = $this->getUser();

		$user->generatePasswordResetToken();
		$user->save();

		$link = Yii::$app->params['resetPasswordUrl'] . '/' . $user->password_reset_token;

		MailCompile::renderMail($this->getUser()->email, MailTemplate::TYPE_RESET_PASSWORD, ['link' => $link]);
		return $user;
	}
}
