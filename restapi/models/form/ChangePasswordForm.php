<?php

namespace restapi\models\form;

use restapi\models\User;
use Yii;
use yii\base\Model;
use yii\web\UnauthorizedHttpException;

/**
 * Reset form
 */
class ChangePasswordForm extends Model {
	public $password;
	public $newPassword;
	public $repeatNewPassword;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['password', 'newPassword', 'repeatNewPassword'], 'required'],
			[['newPassword'], 'string', 'max' => 64],
			['password', 'validateOldPassword'],
			['repeatNewPassword', 'compare', 'compareAttribute' => 'newPassword', 'skipOnEmpty' => false, 'message' => Yii::t('app', 'Новые пароли не совпадают')],
		];
	}

	/**
	 * Finds user by [[username]]
	 * @return null|User
	 * @throws UnauthorizedHttpException
	 */
	protected function getUser() {
		$user = Yii::$app->user->identity;
		if (!$user) {
			throw new UnauthorizedHttpException();
		}

		if (isset($user->id)) {
			return User::findOne(['id' => $user->id]);
		} else {
			return null;
		}
	}

	public function validateOldPassword($attribute, $params) {
		if (!$this->hasErrors()) {
			$user = $this->getUser();
			if (!$user || !$user->validatePassword($this->password)) {
				$this->addError($attribute, Yii::t('app', 'Текущий пароль введён неправильно'));
			}
		}
	}

	public function attributeLabels() {
		return [
			'password' => Yii::t('app', 'Старый пароль'),
			'newPassword' => Yii::t('app', 'Новый пароль'),
			'repeatNewPassword' => Yii::t('app', 'Подтвердить новый пароль'),
		];
	}

	/**
	 * Resets user's password.
	 *
	 * @return bool whether the account is found
	 */
	public function update() {
		$user = $this->getUser();
		if (!$user) {
			return;
		}

		$user->setPassword($this->newPassword);

		$user->save();
		return $user;
	}
}
