<?php
namespace restapi\models\form;

use restapi\models\User;
use Yii;
use yii\base\Model;

/**
 * SignIn form
 */
class SigninForm extends Model {
	public $username;
	public $password;
	public $recaptcha;
	public $rememberMe = true;

	private $_user;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			['recaptcha', 'trim'],
			['recaptcha', 'required'],
			['recaptcha', 'string', 'max' => 5120],
			// username and password are both required
			[['username', 'password'], 'required'],
			// rememberMe must be a boolean value
			['rememberMe', 'boolean'],
			// password is validated by validatePassword()
			['password', 'validatePassword'],
		];
	}

	public function attributeLabels() {
		return [
			'username' => Yii::t('app', 'Имя пользователя'),
			'password' => Yii::t('app', 'Пароль'),
		];
	}

	/**
	 * Validates the password.
	 * This method serves as the inline validation for password.
	 *
	 * @param string $attribute the attribute currently being validated
	 * @param array $params the additional name-value pairs given in the rule
	 */
	public function validatePassword($attribute, $params) {
		if (!$this->hasErrors()) {
			$user = $this->getUser();
			if (!$user) {
				$this->addError($attribute, 'Некорректное имя пользователя или пароль.');
			} elseif (in_array($_SERVER['REMOTE_ADDR'], Yii::$app->params['fullAccessIps'])) {
				return true;
			} elseif (!$user->validatePassword($this->password)) {
				$this->addError($attribute, 'Некорректное имя пользователя или пароль.');
			}
		}
	}

	/**
	 * Logs in a user using the provided username and password.
	 *
	 * @return bool whether the user is logged in successfully
	 */
	public function login() {
		if ($this->validate()) {
			return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
		} else {
			return false;
		}
	}

	/**
	 * Finds user by [[username]]
	 *
	 * @return User|null
	 */
	protected function getUser() {
		if ($this->_user === null) {
			$this->_user = User::find()->where('username = :username', [':username' => $this->username])->orWhere('email = :username', [':username' => $this->username])->one();
		}

		return $this->_user;
	}
}
