<?php

namespace restapi\models\form;

use restapi\models\User;
use Yii;
use yii\base\Model;
use yii\web\UnauthorizedHttpException;

/**
 * Reset form
 */
class UpdateInfoForm extends Model {
	public $first_name;
	public $last_name;
	public $step_auth;
	public $step_method;
	public $send_auth;
	public $send_method;
	public $country;
	public $city;
	public $city_address;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			['first_name', 'trim'],
			['first_name', 'string', 'max' => 32],

			['last_name', 'trim'],
			['last_name', 'string', 'max' => 32],

			['country', 'string', 'max' => 32],

			['city', 'string', 'max' => 64],
			['city_address', 'string'],
			['step_auth', 'checkStepAuthMethod'],
			['send_auth', 'checkSendAuthMethod'],
			[['step_method', 'send_method'], 'trim'],
		];
	}

	public function attributeLabels() {
		return [
			'step_auth' => Yii::t('app', 'Дополнительная проверка входа в аккаунт'),
			'step_method' => Yii::t('app', 'Тип проверки входа в аккаунт'),
			'send_auth' => Yii::t('app', 'Дополнительная проверка отправки'),
			'send_method' => Yii::t('app', 'Тип проверки отправки'),
		];
	}

	/**
	 * @return null|User
	 * @throws UnauthorizedHttpException
	 */
	protected function getUser() {
		$user = Yii::$app->user->identity;
		if (!$user) {
			throw new UnauthorizedHttpException();
		}

		if (isset($user->id)) {
			return User::findOne(['id' => $user->id]);
		} else {
			return null;
		}
	}

	public function checkStepAuthMethod($attributeName, $params) {
		if ($this->step_auth == User::STEP_AUTH_TRUE) {
			if (!in_array($this->step_method, array_keys(User::getStepMethodArray()))) {
				$this->addError('step_method', Yii::t('app', 'Метод аутентификации не выбран'));
			}
		}
	}

	public function checkSendAuthMethod($attributeName, $params) {
		if ($this->send_auth == User::SEND_AUTH_TRUE) {
			if (!in_array($this->send_method, array_keys(User::getStepMethodArray()))) {
				$this->addError('send_method', Yii::t('app', 'Метод отправки не выбран'));
			}
		}
	}

	public function update() {
		$user = $this->getUser();
		if (!$user) {
			return;
		}

		$user->step_auth = $this->step_auth;
		$user->step_method = $this->step_method;
		$user->send_auth = $this->send_auth;
		$user->send_method = $this->send_method;

		$user->save(false);
		return $user;
	}
}
