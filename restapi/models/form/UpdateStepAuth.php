<?php

namespace restapi\models\form;

use Yii;
use yii\base\Model;
use restapi\models\User;
use yii\web\UnauthorizedHttpException;

/**
 * Reset form
 */
class UpdateStepAuth extends Model {
    public $step_auth;
    public $stepMethod;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['step_auth', 'integer'],
            ['stepMethod', 'integer']
        ];
    }

    /**
     * Finds user by [[username]]
     * @return null|User
     * @throws UnauthorizedHttpException
     */
    protected function getUser()
    {
        $user = Yii::$app->user->identity;
        if (!$user) {
            throw new UnauthorizedHttpException();
        }

        if (isset($user->id)) {
            return User::findOne(['id' => $user->id]);
        } else {
            return null;
        }
    }

    public function update() {
        if (!$this->validate()) {
            return;
        }

        $user = $this->getUser();
        if (!$user)
            return;

        $user->step_auth = $this->step_auth;
        $user->step_method = $this->stepMethod;

        $user->save();
        return $user;
    }
}
