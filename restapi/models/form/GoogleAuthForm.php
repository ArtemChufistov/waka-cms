<?php

namespace restapi\models\form;

use common\components\GoogleAuthenticator;
use restapi\models\User;
use Yii;
use yii\base\Model;
use yii\web\UnauthorizedHttpException;

/**
 * Change Email form
 */
class GoogleAuthForm extends Model {
	public $code;
	public $secret;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['code', 'secret'], 'required'],
		];
	}

	/**
	 * Finds user by [[username]]
	 * @return null|User
	 * @throws UnauthorizedHttpException
	 */
	protected function getUser() {
		$user = Yii::$app->user->identity;
		if (!$user) {
			throw new UnauthorizedHttpException();
		}

		if (isset($user->id)) {
			return User::findOne(['id' => $user->id]);
		} else {
			return null;
		}
	}

	public function handle() {
		$user = $this->getUser();

		if ($user->ga_secret != 0) {
			$this->addError('code', Yii::t('app', 'Код уже установлен'));
		}

		$ga = new GoogleAuthenticator;
		$code = $ga->getCode($this->secret);

		if ($ga->verifyCode($this->secret, $code, 2)) {
			if ($code == $this->code) {
				$user->ga_secret = $this->secret;
				$user->save(false);
				return $user;
			} else {
				$this->addError('code', Yii::t('app', 'Введён неправильный код'));
				return $this;
			}
		} else {
			$this->addError('secret', Yii::t('app', 'Некорректный секретный ключ'));
			return $this;
		}
	}

	public function attributeLabels() {
		return [
			'code' => Yii::t('app', 'Код аутентификации'),
			'secret' => Yii::t('app', 'Секретный ключ'),
		];
	}
}