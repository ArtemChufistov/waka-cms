<?php

namespace restapi\models\form;

use common\components\MailCompile;
use common\models\MailTemplate;
use restapi\models\User;
use Yii;
use yii\base\Model;

/**
 * Reset form
 */
class NewpasswordForm extends Model {
	public $token;
	public $password;

	private $_user;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			['token', 'trim'],
			['token', 'string', 'max' => 256],
			['token', 'validateToken'],
			['password', 'string', 'min' => 6],
			[['token', 'password'], 'required'],
		];
	}

	/**
	 * Finds user by [[username]]
	 *
	 * @return User|null
	 */
	protected function getUser() {
		if ($this->_user === null) {
			$this->_user = User::find()->where('password_reset_token = :token', [':token' => $this->token])->one();
		}

		return $this->_user;
	}

	public function validateToken($attribute, $params) {
		if (!$this->hasErrors()) {
			$user = $this->getUser();
			if (!$user) {
				$this->addError($attribute, 'Incorrect token.');
			}
		}
	}

	/**
	 * Changes user's password.
	 *
	 * @return User|null whether the account is found
	 */
	public function newpassword() {
		if (!$this->validate()) {
			return false;
		}

		$user = $this->getUser();

		$user->removePasswordResetToken();
		$user->setPassword($this->password);
		$user->generateAuthKey();
		$user->save();

		MailCompile::renderMail($this->getUser()->email, MailTemplate::TYPE_NEW_PASSWORD);

		if (Yii::$app->user->login($this->getUser(), 0)) {
			return $user;
		}

		return false;
	}
}
