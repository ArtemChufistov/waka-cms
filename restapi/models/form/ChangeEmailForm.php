<?php

namespace restapi\models\form;

use common\components\MailCompile;
use common\models\MailTemplate;
use common\models\Preference;
use restapi\models\User;
use Yii;
use yii\base\Model;
use yii\web\UnauthorizedHttpException;

/**
 * Change Email form
 */
class ChangeEmailForm extends Model {
	public $newEmail;
	public $emailConfirmationCode;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			['newEmail', 'required'],
			['newEmail', 'email'],
			['emailConfirmationCode', 'safe'],
		];
	}

	/**
	 * Finds user by [[username]]
	 * @return null|User
	 * @throws UnauthorizedHttpException
	 */
	protected function getUser() {
		$user = Yii::$app->user->identity;
		if (!$user) {
			throw new UnauthorizedHttpException();
		}

		if (isset($user->id)) {
			return User::findOne(['id' => $user->id]);
		} else {
			return null;
		}
	}

	/**
	 * Create, check email confirmation code, chane E-mail if correct
	 *
	 * @return bool whether the code is created
	 */
	public function handle() {
		$user = $this->getUser();
		if (!$user) {
			return;
		}

		if (!empty($this->emailConfirmationCode)) {
			if ($this->emailConfirmationCode == $user->email_confirmation_code) {
				$user->email_confirmation_code = null;
				$user->email = $this->newEmail;
				$user->save();
				return $user;
			} else {
				$this->addError('emailConfirmationCode', Yii::t('app', 'Введён неверный код подтверждения'));
				return $this;
			}
		} else {
			if ((time() - strtotime($user->last_date_email_confirm_token)) > 60) {
				$user->email_confirmation_code = rand(100000, 999999);
				$user->last_date_email_confirm_token = date('Y-m-d H:i:s');
				$user->save(false);

				$username = Preference::find()->where(['key' => Preference::KEY_SEND_MAIL_USERNAME])->one()->value;
				MailCompile::renderMail($user->email, MailTemplate::TYPE_CHANGE_EMAIL_CODE, ['emailConfirmationCode' => $user->email_confirmation_code]);

				$this->addError('emailConfirmationCode', Yii::t('app', 'На ваш текущий E-mail отправлен код, введите его для подтверждения'));
				return $this;
			} else {
				$this->addError('emailConfirmationCode', Yii::t('app', 'Повторная отправка кода на E-mail возможна через {time} сек', [
					'time' => (60 - (time() - strtotime($user->last_date_email_confirm_token))),
				]));
				return $this;
			}
		}
	}

	public function attributeLabels() {
		return [
			'newEmail' => Yii::t('app', 'Новый E-mail'),
			'emailConfirmationCode' => Yii::t('app', 'Код подтверждения'),
		];
	}
}