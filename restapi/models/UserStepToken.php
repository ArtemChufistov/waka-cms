<?php

namespace restapi\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user_step_token".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $token
 * @property string $key
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class UserStepToken extends \common\models\UserStepToken {
	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['user_id', 'token', 'key'], 'required'],
			[['user_id', 'created_at', 'updated_at'], 'integer'],
			[['token', 'key'], 'string', 'max' => 32],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'user_id' => Yii::t('app', 'User ID'),
			'token' => Yii::t('app', 'Token'),
			'key' => Yii::t('app', 'Key'),
			'created_at' => Yii::t('app', 'Created At'),
			'updated_at' => Yii::t('app', 'Updated At'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	public static function createNow($user_id, $token = null) {
		if (!($model = self::findOne(['user_id' => $user_id]))) {
			$model = new self();
			$model->user_id = $user_id;
		}

		if ($token == null) {
			$model->token = Yii::$app->security->generateRandomString(32);
		} else {
			$model->token = $token;
		}

		$model->key = mt_rand(100, 999) . mt_rand(100, 999);

		if ($model->save()) {
			return $model;
		}
		return false;
	}
}
