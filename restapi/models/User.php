<?php

namespace restapi\models;

use common\models\Document;
use common\models\UserPswWallet;
use common\models\UserTokenBearer;
use restapi\models\UserQuery;
use valentinek\behaviors\ClosureTable;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @inheritdoc
 *
 * @property UserPswWallet[] $userPswWallets
 * @property UserTokenBearer[] $userTokenBearers
 * @property Document[] $avatar
 */
class User extends \common\models\User implements IdentityInterface {

	public function rules() {
		return [
			[['username', 'password_hash', 'email'], 'required'],
			[['parent_id', 'status', 'created_at', 'updated_at'], 'integer'],
			[['username', 'country', 'first_name', 'last_name', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
			[['auth_key', 'phone'], 'string', 'max' => 32],
			[['username'], 'unique'],
			[['email'], 'unique'],
			[['phone'], 'unique'],
			[['step_auth', 'step_method', 'send_auth', 'send_method'], 'safe'],
			[['password_reset_token'], 'unique'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [[
			'class' => TimestampBehavior::className(),
			'createdAtAttribute' => 'created_at',
			'updatedAtAttribute' => 'updated_at',
			'value' => time(),
		], [
			'class' => ClosureTable::className(),
			'tableName' => 'user_tree',
		],
		];
	}

	public static function find() {
		return new UserQuery(static::className());
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'parent_id' => Yii::t('app', 'Parent ID'),
			'username' => Yii::t('app', 'Username'),
			'ga_secret' => Yii::t('app', 'Ga secret'),
			'country' => Yii::t('app', 'Country'),
			'city' => Yii::t('app', 'City'),
			'city_address' => Yii::t('app', 'Address'),
			'firstName' => Yii::t('app', 'First Name'),
			'lastName' => Yii::t('app', 'Last Name'),
			'auth_key' => Yii::t('app', 'Auth Key'),
			'password_hash' => Yii::t('app', 'Password Hash'),
			'password_reset_token' => Yii::t('app', 'Password Reset Token'),
			'email' => Yii::t('app', 'Email'),
			'email_confirmation_code' => Yii::t('app', 'Email Confirmation Code'),
			'sms_confirmation_code' => Yii::t('app', 'Sms Confirmation Code'),
			'phone' => Yii::t('app', 'Phone'),
			'step_auth' => Yii::t('app', 'Step Auth'),
			'step_method' => Yii::t('app', 'Step Method'),
			'send_auth' => Yii::t('app', 'Send Auth'),
			'send_method' => Yii::t('app', 'Send Method'),
			'status' => Yii::t('app', 'Status'),
			'avatar' => Yii::t('app', 'Avatar'),
			'created_at' => Yii::t('app', 'Created At'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'last_date_email_confirm_token' => Yii::t('app', 'Last date E-mail confirm Token'),
		];
	}

	public function fields() {
		return [
			'email',
			'phone',
			'sms_confirmation_code',
			'username',
			'ga_secret',
			'country',
			'city',
			'city_address',
			'first_name',
			'last_name',
			'created_at',
			'updated_at',
			'step_auth',
			'step_method',
			'send_auth',
			'send_method',
			'agreement' => function ($model) {
				return $this->agreement ? $this->agreement->verify : false;
			},
			'avatar' => function ($model) {
				return $model->avatar ? base64_encode($model->avatar->file) : '';
			},
			'verifyImage' => function ($model) {
				return (Document::find()->where(['type' => Document::TYPE_IMAGE_VERIFY_1])->orWhere(['type' => Document::TYPE_IMAGE_VERIFY_2])->andWhere(['user_id' => $model->id])->andWhere(['verify' => 1])->count() == 2);
			},
			'last_date_email_confirm_token',
		];
	}

	public function appendToParent() {
		$parentUser = $this->find()->where(['id' => $this->parent_id])->one();

		$currentUser = $this->find()->where(['id' => $this->id])->one();

		$currentUser->appendTo($parentUser);
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null) {
		$token = UserTokenBearer::findOne(['token' => $token]);
		return $token->user;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUserTokenBearers() {
		return $this->hasMany(UserTokenBearer::className(), ['user_id' => 'id']);
	}

	public function getAvatar() {
		return $this->hasOne(Document::className(), ['user_id' => 'id'])->onCondition(['type' => 1]);
	}

	public function getAgreement() {
		return $this->hasOne(Document::className(), ['user_id' => 'id'])->onCondition(['type' => 5]);
	}
}
