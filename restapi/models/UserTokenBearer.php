<?php

namespace restapi\models;

use Yii;
use yii\behaviors\TimestampBehavior;

class UserTokenBearer extends \common\models\UserTokenBearer
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'token'], 'required'],
            [['user_id', 'expire_at', 'created_at', 'updated_at'], 'integer'],
            [['token'], 'string', 'max' => 64],
            [['user_id', 'token'], 'unique', 'targetAttribute' => ['user_id', 'token'], 'message' => 'The combination of User ID and Token has already been taken.'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'token' => Yii::t('app', 'Token'),
            'expire_at' => Yii::t('app', 'Expire At'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function createToken($user_id){
        $date = new \DateTime();
        self::deleteAll('expire_at < :date',[':date'=>$date->getTimestamp()]);

        $model = new self();
        $model->user_id = $user_id;
        $model->token = Yii::$app->security->generateRandomString(32);
        $model->expire_at = $date->getTimestamp() + 60*60*24*7; // 7 дней жизни токена
        return ($model->save()?$model:false);
    }
}
