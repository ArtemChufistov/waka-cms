<?php

namespace restapi\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Json;

/**
 * This is the model class for table "user_log".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $type
 * @property string $additional_info
 * @property string $comments
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class UserLog extends \common\models\UserLog
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function add($userId, $type, $additional = '', $comment = '')
    {
        $model = new self();
        $model->user_id = $userId;
        $model->type = $type;
        $model->additional_info = Json::encode([
            "user_ip" => Yii::$app->request->getUserIP(),
            "user_agent" => Yii::$app->request->getUserAgent(),
            "host_name" => Yii::$app->request->getHostName(),
            "additional" => $additional
        ]);
        $model->comments = $comment;
        $model->save();
    }
}
