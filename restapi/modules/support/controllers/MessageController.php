<?php
namespace restapi\modules\support\controllers;

use restapi\modules\support\models\form\TicketMessageForm;
use restapi\modules\support\models\Ticket;
use restapi\modules\support\models\TicketMessage;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * Message controller
 */
class MessageController extends Controller {
	public $enableCsrfValidation = false;

	public function behaviors() {
		$behaviors = parent::behaviors();
		$behaviors['corsFilter'] = [
			'class' => Cors::className(),
		];
		$behaviors['authenticator'] = [
			'class' => HttpBearerAuth::className(),
			'except' => ['options'],
		];
		return $behaviors;
	}

	public function actions() {
		return [
			'options' => [
				'class' => 'yii\rest\OptionsAction',
			],
		];
	}

	public function actionAdd() {
		$form = new TicketMessageForm;

		if ($form->load(Yii::$app->request->post(), '') && $form->validate()) {
			$message = new TicketMessage;
			$message->user_id = Yii::$app->user->identity->id;
			$message->setAttributes($form->attributes);
			$message->save();

			return $message;
		} else {
			return $form;
		}
	}

	public function actionList() {
		$ticket = Ticket::find()
			->where(['user_id' => Yii::$app->user->identity->id])
			->andWhere(['id' => Yii::$app->request->get('id')])
			->one();

		if (empty($ticket)) {
			return $this->renderError(Yii::t('app', 'Тикет не найден'));
		}

		$ticketMessages = TicketMessage::find()
			->where(['ticket_id' => $ticket->id])
			->orderBy(['id' => SORT_DESC])
			->all();

		return $ticketMessages;
	}
}