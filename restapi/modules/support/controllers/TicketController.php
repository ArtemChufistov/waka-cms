<?php

namespace restapi\modules\support\controllers;

use restapi\modules\support\models\form\TicketForm;
use restapi\modules\support\models\Ticket;
use restapi\modules\support\models\TicketDepartment;
use restapi\modules\support\models\TicketStatus;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * Ticket controller
 */
class TicketController extends Controller {
	public $enableCsrfValidation = false;

	public function behaviors() {
		$behaviors = parent::behaviors();
		$behaviors['corsFilter'] = [
			'class' => Cors::className(),
		];
		$behaviors['authenticator'] = [
			'class' => HttpBearerAuth::className(),
			'except' => ['options'],
		];
		return $behaviors;
	}

	public function actions() {
		return [
			'options' => [
				'class' => 'yii\rest\OptionsAction',
			],
		];
	}

	public function actionList() {
		$tickets = Ticket::find()
			->where(['user_id' => Yii::$app->user->id])
			->orderBy(['id' => SORT_DESC])
			->all();

		return $tickets;
	}

	public function actionDepartments() {
		return TicketDepartment::find()->all();
	}

	public function actionAdd() {
		$form = new TicketForm;

		if ($form->load(Yii::$app->request->post(), '') && $form->validate()) {
			$ticketStatus = TicketStatus::find()->where(['key' => TicketStatus::KEY_NEW])->one();

			$ticket = new Ticket;
			$ticket->status_id = $ticketStatus->id;
			$ticket->user_id = Yii::$app->user->identity->id;
			$ticket->setAttributes($form->attributes);
			$ticket->save();

			return $ticket;
		} else {
			return $form;
		}
	}
}