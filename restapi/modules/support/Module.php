<?php

namespace restapi\modules\support;

use Yii;

/**
 * Class Module
 * @package restapi\modules\api\v1
 */
class Module extends \yii\base\Module {
	/**
	 * @var string
	 */
	public $controllerNamespace = 'restapi\modules\support\controllers';

	public function init() {
		parent::init();
	}
}
