<?php

namespace restapi\modules\support\models;

class TicketDepartment extends \common\modules\support\models\TicketDepartment {
	public function fields() {
		return [
			'name',
			'key',
		];
	}
}
