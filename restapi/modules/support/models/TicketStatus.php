<?php

namespace restapi\modules\support\models;

class TicketStatus extends \common\modules\support\models\TicketStatus {
	public function fields() {
		return [
			'name',
			'key',
		];
	}
}
