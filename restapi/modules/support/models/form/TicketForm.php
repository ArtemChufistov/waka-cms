<?php
namespace restapi\modules\support\models\form;

use Yii;
use yii\base\Model;

/**
 * Ticket form
 */
class TicketForm extends Model {
	public $department;
	public $text;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['department', 'text'], 'required'],
		];
	}

	public function attributeLabels() {
		return [
			'department' => Yii::t('app', 'Отдел'),
			'text' => Yii::t('app', 'Текст'),
		];
	}
}
