<?php
namespace restapi\modules\support\models\form;

use Yii;
use yii\base\Model;

/**
 * Ticket Message form
 */
class TicketMessageForm extends Model {
	public $text;
	public $ticket_id;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['text', 'ticket_id'], 'required'],
		];
	}

	public function attributeLabels() {
		return [
			'ticket_id' => Yii::t('app', 'Номер тикета'),
			'text' => Yii::t('app', 'Текст'),
		];
	}
}
