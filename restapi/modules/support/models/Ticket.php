<?php

namespace restapi\modules\support\models;

class Ticket extends \common\modules\support\models\Ticket {
	public function fields() {
		return [
			'id',
			'text',
			'department',
			'dateAdd',
		];
	}
}
