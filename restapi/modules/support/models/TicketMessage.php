<?php

namespace restapi\modules\support\models;

class TicketMessage extends \common\modules\support\models\TicketMessage {
	public function fields() {
		return [
			'id',
			'text',
			'dateAdd',
		];
	}
}
