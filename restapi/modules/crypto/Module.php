<?php

namespace restapi\modules\crypto;

use Yii;

/**
 * Class Module
 * @package restapi\modules\api\v1
 */
class Module extends \yii\base\Module
{
    /**
     * @var string
     */
    public $controllerNamespace = 'restapi\modules\crypto\controllers';

    public function init()
    {
        parent::init();
    }
}
