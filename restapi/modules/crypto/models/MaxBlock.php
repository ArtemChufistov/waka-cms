<?php

namespace restapi\modules\crypto\models;

class MaxBlock extends \common\modules\crypto\models\MaxBlock {
	public function fields() {
		return [
			'id',
			'currency',
			'block_height',
		];
	}
}
