<?php

namespace restapi\modules\crypto\models;

use common\models\Currency;

class Transaction extends \common\modules\crypto\models\Transaction {
	public function fields() {
		return [
			'id',
			'amount',
			'currency',
			'category',
			'txid' => function ($model) {
				return $model->txid;
			},
			'tx_key' => function ($model) {
				switch ($model->getCurrency()->one()->key) {
				case Currency::KEY_XMR:
					return $model->tx_key != '' ? $model->tx_key : '';
					break;
				default:
					return '';
					break;
				}
			},
			'payment_id' => function ($model) {
				switch ($model->getCurrency()->one()->key) {
				case Currency::KEY_XMR:
					return $model->payment_id != 0 ? $model->payment_id : '';
					break;
				default:
					return '';
					break;
				}
			},
			'from_addr' => function ($model) {
				switch ($model->getCurrency()->one()->key) {
				case Currency::KEY_ETH:
					return $model->from_addr;
					break;
				default:
					return '';
					break;
				}
			},
			'virtualStatus' => function ($model) {
				if ($model->category == Transaction::CATEGORY_SEND) {
					$paymentTask = $model->getPaymentTask()->one();
					if (empty($paymentTask)) {
						return Transaction::VIRTUAL_STATUS_UNKNOWN;
					} else {
						if ($paymentTask->txid == '') {
							return Transaction::VIRTUAL_STATUS_NEW;
						} else {
							if ($model->block_number == 0) {
								return Transaction::VIRTUAL_STATUS_IN_WORK;
							} else {
								return Transaction::VIRTUAL_STATUS_COMPLETE;
							}
						}
					}
				} else {
					if ($model->block_number == 0) {
						return Transaction::VIRTUAL_STATUS_IN_WORK;
					} else {
						return Transaction::VIRTUAL_STATUS_COMPLETE;
					}
				}
			},
			'to_addr',
			'blockchain_time',
			'block_number',
		];
	}
}
