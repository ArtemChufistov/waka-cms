<?php

namespace restapi\modules\crypto\models;

use common\modules\crypto\helpers\CoinHelper;
use common\modules\crypto\models\CoinmarketcapTicker;
use common\modules\crypto\models\MaxBlock;

class Currency extends \common\models\Currency {
	public function fields() {
		return [
			'key',
			'slug',
			'title',
			'price_usd' => function ($model) {
				return $model->getCoinmarketcapTicker()->one()->price_usd;
			},
			'block_height' => function ($model) {
				return $model->getMaxBlock()->one()->block_height;
			},
			'logoUrl' => function ($model) {
				return $model->getLogoUrl();
			},
		];
	}

	public function getLogoUrl() {
		return CoinHelper::logoUrl($this->key);
	}

	public function getMaxBlock() {
		return $this->hasOne(MaxBlock::className(), ['currency' => 'slug']);
	}

	public function getCoinmarketcapTicker() {
		return $this->hasOne(CoinmarketcapTicker::className(), ['id' => 'slug']);
	}
}
