<?php

namespace restapi\modules\crypto\models;

class AccountBalance extends \common\modules\crypto\models\AccountBalance {
	public function fields() {
		return [
			'currency',
			'balance' => function ($model) {
				return round($model->balance, 8);
			},
		];
	}
}
