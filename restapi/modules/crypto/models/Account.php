<?php

namespace restapi\modules\crypto\models;

class Account extends \common\modules\crypto\models\Account {
	public function fields() {
		return [
			'id',
			'is_turn_on',
			'name',
			'url',
			'comment',
			'created_at',
			'crypchant_merchant_fee',
		];
	}
}
