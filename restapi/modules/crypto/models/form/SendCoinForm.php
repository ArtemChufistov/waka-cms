<?php
namespace restapi\modules\crypto\models\form;

use common\models\Currency;
use common\modules\crypto\coin\factory\CoinFactory;
use common\modules\crypto\models\AccountBalance;
use common\modules\crypto\models\AccountWallet;
use restapi\models\User;
use Yii;
use yii\base\Model;

/**
 * SignIn form
 */
class SendCoinForm extends Model {
	public $currencyKey;
	public $accountId;
	public $feePer;
	public $userId;
	public $amount;
	public $toAddr;
	public $additional;
	public $confirmCode = 0;
	public $tagId = 0;
	public $paymentId = 0;

	private $estimateFee;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			['confirmCode', 'safe'],
			['tagId', 'safe'],
			['paymentId', 'safe'],
			[['amount', 'toAddr'], 'trim'],
			[['currencyKey', 'amount', 'toAddr', 'feePer', 'userId'], 'required'],
			['currencyKey', 'validateCurrency'],
			['feePer', 'calcEstimateFee'],
			['toAddr', 'notThisAccount'],
			['toAddr', 'correctAddress'],
			['amount', 'moreThanZero'],
			['amount', 'checkBalance'],
			['additional', 'validateAdditional'],
			['confirmCode', 'checkConfirmCode'],
		];
	}

	public function validateCurrency($attribute, $params) {
		$currency = Currency::find()->where(['key' => $this->$attribute])->one();
		if (empty($currency)) {
			$this->addError($attribute, Yii::t('app', 'Валюта не найдена'));
		}
	}

	public function calcEstimateFee($attribute, $params) {
		$coinFactory = CoinFactory::get($this->currencyKey);
		$this->estimateFee = $coinFactory->estimatesmartfee();
	}

	public function notThisAccount($attribute, $params) {
		if (!$this->hasErrors()) {
			$accountWallet = AccountWallet::find()
				->where(['user_id' => $this->userId])
				->andWhere(['currency_wallet' => $this->$attribute])
				->one();

			if (!empty($accountWallet)) {
				$this->addError($attribute, Yii::t('app', 'Этот адрес принадлежит вашему аккаунту, отправка самому себе отклонена'));
			}
		}
	}

	public function correctAddress($attribute, $params) {
		if (!$this->hasErrors()) {
			$coinFactory = CoinFactory::get($this->currencyKey);
			if (!$coinFactory->validateAddress($this->$attribute)) {
				$this->addError($attribute, Yii::t('app', 'Некорректный адрес'));
			}
		}
	}

	public function moreThanZero($attribute, $params) {
		if (!($this->$attribute > 0)) {
			$this->addError($attribute, Yii::t('app', 'Сумма должна быть больше нуля'));
		}
	}

	public function checkBalance($attribute, $params) {
		if (!$this->hasErrors()) {
			$currency = Currency::find()->where(['key' => $this->currencyKey])->one();
			$balance = AccountBalance::find()
				->where(['user_id' => $this->userId])
				->andWhere(['currency' => $currency->slug])
				->one();

			$resultBalance = $balance->balance - $this->estimateFee;

			if ($this->amount > $resultBalance) {
				$this->addError($attribute, Yii::t('app', 'Недостаточно средств, вы указали {amount} {currencyKey}, доступно {resultBalance} {currencyKey}', [
					'resultBalance' => $resultBalance,
					'currencyKey' => $this->currencyKey,
					'amount' => $this->amount,
				]));
			}
		}
	}

	public function validateAdditional($attribute, $params) {
		if (!$this->hasErrors()) {
			$coinFactory = CoinFactory::get($this->currencyKey);
			if (!$coinFactory->validateAdditional($this->additional)) {
				$this->addError($attribute, Yii::t('app', 'Ошибка дополнительного атрибута'));
			}
		}
	}

	public function checkConfirmCode($attributes, $params) {

		$user = User::find()->where(['id' => $this->userId])->one();

		// Если включена проверка кодом подтверждения и код не передан
		if ($user->send_auth == User::SEND_AUTH_TRUE && empty($this->confirmCode)) {
			// toDo отправить код
			$this->addError('confirmCode', Yii::t('app', 'Укажите код подтверждения'));
		}

		// Если включена проверка кодом подтверждения и код неправильный
		if ($user->send_auth == User::SEND_AUTH_TRUE && $this->confirmCode != 1111) {
			$this->addError('confirmCode', Yii::t('app', 'Неправильный код подтверждения'));
		}
	}

	public function getEstimateFee() {
		return $this->estimateFee;
	}

	public function attributeLabels() {
		return [
			'currencyKey' => Yii::t('app', 'Валюта'),
			'toAddr' => Yii::t('app', 'Кошелёк'),
			'amount' => Yii::t('app', 'Сумма'),
			'feePer' => Yii::t('app', 'Комиссия'),
		];
	}
}
