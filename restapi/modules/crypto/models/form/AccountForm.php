<?php
namespace restapi\modules\crypto\models\form;

use restapi\models\User;
use Yii;
use yii\base\Model;

/**
 * SignIn form
 */
class AccountForm extends Model {
	public $user_id;
	public $name;
	public $secret;
	public $created_at;
	public $updated_at;
	public $is_delete;
	public $is_turn_on;
	public $comment;
	public $url;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['name', 'url', 'comment', 'secret', 'is_turn_on'], 'required'],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
		];
	}

	public function attributeLabels() {
		return [
			'user_id' => Yii::t('app', 'Пользователь'),
			'name' => Yii::t('app', 'Название'),
			'url' => Yii::t('app', 'URL магазина'),
			'comment' => Yii::t('app', 'Описание магазина'),
			'secret' => Yii::t('app', 'Секретный ключ'),
			'created_at' => Yii::t('app', 'Дата создания'),
			'updated_at' => Yii::t('app', 'Дата обновления'),
			'is_turn_on' => Yii::t('app', 'Включен'),
		];
	}
}
