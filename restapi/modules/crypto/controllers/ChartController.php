<?php

namespace restapi\modules\crypto\controllers;

use common\models\Currency;
use common\modules\crypto\models\CourseChart;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * Site controller
 */
class ChartController extends Controller {
	public $enableCsrfValidation = false;

	public function behaviors() {
		$behaviors = parent::behaviors();
		$behaviors['corsFilter'] = [
			'class' => Cors::className(),
		];
		$behaviors['authenticator'] = [
			'class' => HttpBearerAuth::className(),
			'only' => ['save-account', 'get-account'],
		];

		return $behaviors;
	}

	public function actions() {
		$actions = parent::actions();
		$actions['options'] = [
			'class' => 'yii\rest\OptionsAction',
		];
		return $actions;
	}

	public function actionCourse() {
		$currency = Currency::find()->where(['key' => Currency::KEY_BTC])->one();
		$courseChartArray = CourseChart::find()
			->where(['currency_id_from' => $currency->id])
			->orderBy(['date' => SORT_DESC])
			->limit(48)
			->all();

		return array_reverse($courseChartArray);
	}
}