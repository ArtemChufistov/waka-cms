<?php

namespace restapi\modules\crypto\controllers;

use common\modules\crypto\coin\factory\CoinFactory;
use common\modules\crypto\components\CoinComponent;
use restapi\models\User;
use restapi\modules\crypto\models\AccountBalance;
use restapi\modules\crypto\models\Currency;
use restapi\modules\crypto\models\form\SendCoinForm;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * Site controller
 */
class CoinController extends Controller {
	public $enableCsrfValidation = false;

	public function behaviors() {
		$behaviors = parent::behaviors();
		$behaviors['corsFilter'] = [
			'class' => Cors::className(),
		];
		$behaviors['authenticator'] = [
			'class' => HttpBearerAuth::className(),
			'except' => ['options'],
		];
		return $behaviors;
	}

	public function actions() {
		return [
			'options' => [
				'class' => 'yii\rest\OptionsAction',
			],
		];
	}

	public function actionGetAddress() {

		$currencyKey = Yii::$app->request->post('currencyKey');
		$userId = Yii::$app->user->id;

		$address = CoinComponent::getNewAddress($currencyKey, $userId);

		return ['address' => $address];
	}

	public function actionSend() {

		$model = new SendCoinForm();
		$model->userId = Yii::$app->user->id;

		if ($model->load(Yii::$app->request->post(), '') && $model->validate()) {
			return CoinComponent::saveForSend($model);
		} else {
			return $model;
		}
	}

	public function actionEstimateFee() {
		$currency = Currency::find()->where(['key' => Yii::$app->request->post('currencyKey')])->one();

		return [
			[
				'key' => 'standart',
				'title' => Yii::t('app', 'Стандартная'),
				'value' => CoinFactory::get($currency->key)->estimateFee(6),
			], [
				'key' => 'priority',
				'title' => Yii::t('app', 'Приоритетная'),
				'value' => CoinFactory::get($currency->key)->estimateFee(1),
			],
		];
	}

	public function actionGetCurrencyes() {
		return Currency::find()
			->where(['active' => Currency::ACTIVE_TRUE])
			->orderBy(['sort' => SORT_DESC])
			->all();
	}

	public function actionAccountBalance() {
		return AccountBalance::getActiveForUserAddNot(Yii::$app->user->id);
	}
}