<?php

namespace restapi\modules\crypto\controllers;

use common\models\Currency;
use restapi\modules\crypto\models\MaxBlock;
use restapi\modules\crypto\models\Transaction;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * Transaction controller
 */
class TransactionController extends Controller {
	public $modelClass = 'restapi\modules\crypto\models\Transaction';
	public $enableCsrfValidation = false;

	public $serializer = [
		'class' => 'yii\rest\Serializer',
		'collectionEnvelope' => 'items',
	];

	public function behaviors() {
		$behaviors = parent::behaviors();
		$behaviors['corsFilter'] = [
			'class' => Cors::className(),
		];
		$behaviors['authenticator'] = [
			'class' => HttpBearerAuth::className(),
			'except' => ['options'],
		];
		return $behaviors;
	}

	public function actions() {
		return [
			'options' => [
				'class' => 'yii\rest\OptionsAction',
			],
		];
	}

	public function actionIndex() {
		$actions = parent::actions();

		unset($actions['create']);

		$query = $this->modelClass::find();
		$query->where(['user_id' => Yii::$app->user->id]);
		$query->andWhere(['is_show' => Transaction::IS_SHOW_TRUE]);

		$page = 0;
		if (!empty(Yii::$app->request->get('page'))) {
			$page = Yii::$app->request->get('page');
		}

		$pageSize = 20;
		if (!empty(Yii::$app->request->get('page_size'))) {
			$pageSize = Yii::$app->request->get('page_size');
		}

		if (!empty(Yii::$app->request->get('currency'))) {
			$query->andWhere(['currency' => Yii::$app->request->get('currency')]);
		}

		if (!empty(Yii::$app->request->get('txid'))) {
			$query->andFilterWhere(['LIKE', 'txid', Yii::$app->request->get('txid')]);
		}

		if (!empty(Yii::$app->request->get('to_addr'))) {
			$query->andFilterWhere(['LIKE', 'to_addr', Yii::$app->request->get('to_addr')]);
		}

		if (!empty(Yii::$app->request->get('amount'))) {
			$query->andFilterWhere(['LIKE', 'amount', Yii::$app->request->get('amount')]);
		}

		if (!empty(Yii::$app->request->get('date'))) {
			$query->andFilterWhere(['>=', 'created_at', date('Y-m-d H:i:s', strtotime(Yii::$app->request->get('date')))]);
			$query->andFilterWhere(['<=', 'created_at', date('Y-m-d H:i:s', strtotime(Yii::$app->request->get('date') . ' 23:59:59'))]);
		}

		if (!(Yii::$app->request->get('confirms') == '')) {
			if (Yii::$app->request->get('confirms') == 0) {
				$query->andFilterWhere(['block_number' => 0]);
			} else {
				$maxBlock = MaxBlock::find()->where(['currency' => Yii::$app->request->get('currency')])->one();
				$query->andFilterWhere(['block_number' => ($maxBlock->block_height - Yii::$app->request->get('confirms'))]);
			}
		}

		$transactionProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => $pageSize,
				'page' => $page,
			],
			'sort' => [
				'defaultOrder' => ['created_at' => SORT_DESC],
			],
		]);

		$dataModels = $transactionProvider->getModels();

		return new ArrayDataProvider([
			'allModels' => $dataModels,
			'pagination' => [
				'pageSize' => $pageSize,

			],
		]);
	}

	public function actionNotConfirms() {
		$txs = [];
		$amount = 0;

		if (!empty(Yii::$app->request->get('currency'))) {
			$currency = Currency::find()
				->where(['slug' => Yii::$app->request->get('currency')])
				->one();

			if (!empty($currency)) {
				$txs = $this->modelClass::find()
					->where(['currency' => $currency])
					->andWhere(['user_id' => Yii::$app->user->id])
					->andWhere(['is_show' => Transaction::IS_SHOW_TRUE])
					->andWhere(['block_number' => 0])
					->andWhere(['>=', 'created_at', date('Y-m-d H:i:s', time() - 60 * 60 * 24 * 7)])
					->all();

				foreach ($txs as $tx) {
					$amount += $tx->amount;
				}
			}
		}

		return [
			'count' => count($txs),
			'amount' => $amount,
		];
	}

	public function actionLast() {
		$txs = [];

		if (!empty(Yii::$app->request->get('limit'))) {
			$limit = Yii::$app->request->get('limit');
		} else {
			$limit = 10;
		}

		$txs = $this->modelClass::find()
			->where(['user_id' => Yii::$app->user->id])
			->andWhere(['is_show' => Transaction::IS_SHOW_TRUE])
			->orderBy(['id' => SORT_DESC])
			->limit($limit)
			->all();

		return $txs;
	}
}