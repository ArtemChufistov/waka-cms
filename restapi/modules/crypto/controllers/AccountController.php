<?php

namespace restapi\modules\crypto\controllers;

use common\modules\crypto\helpers\CoinHelper;
use restapi\modules\crypto\models\Account;
use restapi\modules\crypto\models\form\AccountForm;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * Site controller
 */
class AccountController extends Controller {
	public $enableCsrfValidation = false;

	public function behaviors() {
		$behaviors = parent::behaviors();
		$behaviors['corsFilter'] = [
			'class' => Cors::className(),
		];
		$behaviors['authenticator'] = [
			'class' => HttpBearerAuth::className(),
			'except' => ['options'],
		];
		return $behaviors;
	}

	public function actions() {
		return [
			'options' => [
				'class' => 'yii\rest\OptionsAction',
			],
		];
	}

	public function actionList() {
		$accountList = Account::find()
			->where(['user_id' => Yii::$app->user->id])
			->orderBy(['id' => SORT_DESC])
			->all();

		return $accountList;
	}

	public function actionAdd() {
		$form = new AccountForm;

		if ($form->load(Yii::$app->request->post(), '') && $form->validate()) {
			$form->secret = CoinHelper::makeSalted($form->secret);

			$model = new Account;
			$model->setAttributes($form->attributes);
			$model->save();

			return $model;
		} else {
			return $form;
		}
	}

	public function actionUpdate() {
		$account = $this->findModel(Yii::$app->request->post()['id']);

		if ($account->user_id != Yii::$app->user->id) {
			throw new NotFoundHttpException('Another user');
		}

		if ($account->load(Yii::$app->request->post(), '') && $account->validate()) {
			$account->save();
		}

		return ['test' => $account];
	}

	protected function findModel($id) {
		if (($model = Account::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}