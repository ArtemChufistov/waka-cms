<?php

namespace restapi\modules\changer;

use Yii;

/**
 * Class Module
 * @package restapi\modules\changer
 */
class Module extends \yii\base\Module
{
    /**
     * @var string
     */
    public $controllerNamespace = 'restapi\modules\changer\controllers';

    public function init()
    {
        parent::init();
    }
}
