<?php
namespace restapi\modules\changer\models\admin;

use Yii;
use yii\mongodb\ActiveRecord;

class Currency extends \common\modules\changer\models\Currency {
    public $fields;

    public function fields() {
        return [
            'id',
            'key',
            'title',
            'in',
            'out',
            'minamount',
            'maxamount',
            'active',
            'label'
        ];
    }
}