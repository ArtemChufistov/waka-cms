<?php
namespace restapi\modules\changer\models\admin;

use Yii;
use yii\mongodb\ActiveRecord;

class Paysys extends \common\modules\changer\models\Paysys {
    public $fields;

    public function fields() {
        return [
            'id',
            'key',
            'title',
        ];
    }
}