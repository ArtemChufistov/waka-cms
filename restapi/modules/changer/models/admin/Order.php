<?php
namespace restapi\modules\changer\models\admin;

use Yii;
use yii\mongodb\ActiveRecord;

class Order extends \common\modules\changer\models\Order {
    public $fields;

    public function fields() {
        return [
            'id',
            'type',
            'status',
            'status_title' => function ($model) {
                return $model->statusTitle();
            },
            'currency_from' => function ($model) {
                return $model->getCurrencyFrom()->one();
            },
            'currency_to' => function ($model) {
                return $model->getCurrencyTo()->one();
            },
            'amount_from',
            'amount_to',
            'course',
            'email',
            'phone',
            'hash',
            'time_for_pay',
            'date_add',
            'wallet_from',
            'wallet_to',
            'card_to',
            'card_from',
            'fio_from',
            'fio_to',
        ];
    }
}