<?php
namespace restapi\modules\changer\models;

use restapi\modules\changer\models\Currency;
use Yii;
use yii\mongodb\ActiveRecord;

class Course extends \common\modules\changer\models\Course {
    public function fields() {
        return [
               'from' => function ($model) {
                $currency = $model->getCurrencyFrom()->one();
                $formClass = "\\restapi\modules\changer\models\\form\\from\\" . ucfirst(mb_strtolower($currency->key));

                if (class_exists($formClass)) {
                    $form = new $formClass;

                    if (method_exists($formClass, 'fields')) {
                        foreach ($form->fields() as $fieldName => $field) {
                            $currency->fields[] = [$fieldName => $field];
                        }
                    }
                }

                return $currency;
            }, 'to' => function ($model) {
                $currency = $model->getCurrencyFrom()->one();
                $formClass = "\\restapi\modules\changer\models\\form\\to\\" . ucfirst(mb_strtolower($currency->key));

                if (class_exists($formClass)) {
                    $form = new $formClass;

                    if (method_exists($formClass, 'fields')) {
                        foreach ($form->fields() as $fieldName => $field) {
                            $currency->fields[] = [$fieldName => $field];
                        }
                    }
                }

                return $currency;
            }, 'amount' => function ($model) {
                return 1;
            },
            'course'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyFrom() {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyTo() {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id_to']);
    }
}