<?php
namespace restapi\modules\changer\models;

use Yii;
use yii\mongodb\ActiveRecord;

class Order extends \common\modules\changer\models\Order {
    public $fields;

    public function fields() {
        return [
            'id',
            'type',
            'status',
            'currency_id_from',
            'currency_id_to',
            'amount_from',
            'amount_to',
            'course',
            'email',
            'phone',
            'hash',
            'date_add',
            'wallet_from',
            'wallet_to',
            'card_to',
            'card_from',
            'fio_from',
            'fio_to',
        ];
    }

    public function prepareNewAttributes($attributes) {
        $currencyFrom = Currency::find()->where([
            'key' => $attributes['from']['currencyKey']
        ])->one();

        $currencyTo = Currency::find()->where([
            'key' => $attributes['to']['currencyKey']
        ])->one();

        $this->type = self::TYPE_REAL;
        $this->status = self::STATUS_NEW;
        $this->currency_id_from = $currencyFrom->id;
        $this->currency_id_to = $currencyTo->id;
        $this->amount_from = $attributes['from']['amount'];
        $this->amount_to = $attributes['to']['amount'];
        $this->course = 545;
        $this->email = $attributes['email'];
        $this->phone = $attributes['phone'];
        $this->date_add = date('Y-m-d H:i:s');
        $this->hash = md5(rand(0, 100000) . time());
        $this->wallet_from = $attributes['from']['fields']['wallet'];
        $this->wallet_to = $attributes['to']['fields']['wallet'];
        $this->card_from = $attributes['from']['fields']['card'];
        $this->card_to = $attributes['to']['fields']['card'];
        $this->fio_from = $attributes['from']['fields']['fio'];
        $this->fio_to = $attributes['to']['fields']['fio'];
    }
}