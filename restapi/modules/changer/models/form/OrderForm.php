<?php
namespace restapi\modules\changer\models\form;

use restapi\modules\changer\models\Course;
use restapi\modules\changer\models\Currency;
use yii\base\Model;
use Yii;

/**
 * OrderForm form
 */
class OrderForm extends Model {

    const TYPE_FROM = 'from';
    const TYPE_TO = 'to';

    public $email;
    public $phone;
    public $from;
    public $to;

    public function rules() {
        return [
            [['from', 'to', 'email', 'phone'], 'required'],
            ['email', 'email'],
            ['from', 'validateFrom'],
            ['to', 'validateTo'],
            ['to', 'validateCourse']
        ];
    }

    public function validateFrom($attribute, $params) {
        if ($this->validateCurrency($attribute, $this->$attribute['currencyKey'], self::TYPE_FROM) == false) {
            return false;
        }

        $formClass = "\\restapi\modules\changer\models\\form\\from\\" . ucfirst(mb_strtolower($this->$attribute['currencyKey']));

        if (class_exists($formClass)) {
            $form = new $formClass;
            if (!($form->load($this->$attribute, 'fields') && $form->validate())) {
                $this->addError($attribute, $form->getErrors());
                return false;
            }
        }

        return true;
    }

    public function validateTo($attribute, $params) {
        if ($this->validateCurrency($attribute, $this->$attribute['currencyKey'], self::TYPE_TO) == false) {
            return false;
        }

        $formClass = "\\restapi\modules\changer\models\\form\\to\\" . ucfirst(mb_strtolower($this->$attribute['currencyKey']));

        if (class_exists($formClass)) {

            $form = new $formClass;
            if (!($form->load($this->$attribute, 'fields') && $form->validate())) {
                $this->addError($attribute, $form->getErrors());
                return false;
            }
        }

        return true;
    }

    public function validateCourse($attribute, $params) {

        $currencyFrom = Currency::find()->where([
            'key' => $this->from['currencyKey'],
            'active' => true
        ])->one();

        $currencyTo = Currency::find()->where([
            'key' => $this->to['currencyKey'],
            'active' => true
        ])->one();

        $course = Course::find()->where([
            'currency_id_from' => $currencyFrom->id,
            'currency_id_to' => $currencyTo->id,
            'active' => true
        ])->one();

        if (empty($course)) {
            $this->addError('to', Yii::t('app', 'Выберите валюту'));
            return false;
        }

        return true;
    }

    private function validateCurrency($attribute, $currencyKey, $type) {
        if (empty($currencyKey)){
            $prefixPhrase = $type == self::TYPE_FROM ? Yii::t('app', 'отдать') : Yii::t('app', 'получить');
            $this->addError($attribute, Yii::t('app', 'Выберите валюту, которую хотите ') . $prefixPhrase );
            return false;
        }

        $currency = Currency::find()->where([
            'key' => $currencyKey,
            'active' => true
        ])->one();

        if (empty($currency)){
            $this->addError($attribute, Yii::t('app', 'Такая валюта не найдена'));
            return false;
        }

        return true;
    }

    public function attributeLabels() {
        return [
            'email' => Yii::t('app', 'E-mail'),
            'phone' => Yii::t('app', 'Телефон'),
        ];
    }
}