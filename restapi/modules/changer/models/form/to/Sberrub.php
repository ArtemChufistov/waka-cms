<?php
namespace restapi\modules\changer\models\form\to;

use Yii;
use yii\base\Model;

/**
 * Sberrub form
 */
class Sberrub extends \restapi\modules\changer\models\form\currency\Sberrub {
    public $cardNumber;
    public $fio;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['cardNumber', 'fio'], 'required'],
        ];
    }

    public function fields() {
        return [
            'cardNumber' => [
                'title' => Yii::t('app', 'На карту'),
                'placeholder' => Yii::t('app', 'Номер карты'),
            ], 'fio' => [
                'title' => Yii::t('app', 'Ф.И.О. получателя'),
                'placeholder' => Yii::t('app', 'ФИО получателя'),
            ]
        ];
    }
}