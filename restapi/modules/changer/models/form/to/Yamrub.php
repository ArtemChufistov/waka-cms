<?php
namespace restapi\modules\changer\models\form\to;

use Yii;
use yii\base\Model;

/**
 * Yamrub form
 */
class Yamrub extends \restapi\modules\changer\models\form\currency\Yamrub {
    public $wallet;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['wallet'], 'required'],
        ];
    }

    public function fields() {
        return [
            'wallet' => [
                'title' => Yii::t('app', 'Кошелёк'),
                'placeholder' => Yii::t('app', 'Укажите кошелёк для получения'),
            ]
        ];
    }
}