<?php
namespace restapi\modules\changer\models\form\to;

use Yii;
use yii\base\Model;

/**
 * Prrub form
 */
class Prrub extends \restapi\modules\changer\models\form\currency\Prrub {
    public $wallet;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['wallet'], 'required'],
        ];
    }

    public function attributeLabels() {
        return [
            'wallet' => Yii::t('app', 'Кошелёк'),
        ];
    }

    public function fields() {
        return [
            'wallet' => [
                'title' => Yii::t('app', 'Кошелёк'),
                'placeholder' => Yii::t('app', 'Укажите кошелёк для получения'),
            ]
        ];
    }
}