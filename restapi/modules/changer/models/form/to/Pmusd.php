<?php
namespace restapi\modules\changer\models\form\to;

use Yii;
use yii\base\Model;

/**
 * Pmusd form
 */
class Pmusd extends \restapi\modules\changer\models\form\currency\Pmusd {

    public function rules() {
        return [
            [['wallet'], 'required'],
            ['wallet', 'validateWallet']
        ];
    }

    public function fields() {
        return [
            'wallet' => [
                'title' => Yii::t('app', 'Кошелёк'),
                'placeholder' => Yii::t('app', 'Укажите кошелёк для получения'),
            ]
        ];
    }
}