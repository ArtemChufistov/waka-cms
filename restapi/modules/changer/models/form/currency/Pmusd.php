<?php
namespace restapi\modules\changer\models\form\currency;

use Yii;
use yii\base\Model;

/**
 * Pmusd form
 */
class Pmusd extends Model {
    public $wallet;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['wallet'], 'required'],
            ['wallet', 'validateWallet']
        ];
    }

    public function validateWallet($attribute, $params) {
        $str = $this->$attribute;

        if (!preg_match('/^U/', $str)) {
            $this->addError($attribute, Yii::t('app', 'Кошелёк Perfect Money должен начинаться с "U"'));
            return false;
        }

        $res = explode('U', $str);

        if (count($res) != 2) {
            $this->addError($attribute, Yii::t('app', 'Пожалуйста укажите правильный кошелёк Perfect Money'));
            return false;
        }

        if (preg_match("/[^0-9$]/", $res[1])) {
            $this->addError($attribute, Yii::t('app', 'Пожалуйста укажите правильный кошелёк Perfect Money'));
            return false;
        }
    }

    public function attributeLabels() {
        return [
            'wallet' => Yii::t('app', 'Кошелёк'),
        ];
    }

    public function fieldsTo() {
        return [
            'wallet' => [
                'title' => Yii::t('app', 'Кошелёк'),
                'placeholder' => Yii::t('app', 'Укажите кошелёк для получения'),
            ]
        ];
    }
}