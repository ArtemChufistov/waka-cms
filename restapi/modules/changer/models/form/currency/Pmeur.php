<?php
namespace restapi\modules\changer\models\form\currency;

use Yii;
use yii\base\Model;

/**
 * Pmeur form
 */
class Pmeur extends Model {
    public $wallet;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['wallet'], 'required'],
        ];
    }

    public function attributeLabels() {
        return [
            'wallet' => Yii::t('app', 'Кошелёк'),
        ];
    }

    public function fieldsTo() {
        return [
            'wallet' => [
                'title' => Yii::t('app', 'Кошелёк'),
                'placeholder' => Yii::t('app', 'Укажите кошелёк для получения'),
            ]
        ];
    }
}