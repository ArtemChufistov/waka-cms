<?php
namespace restapi\modules\changer\models\form\currency;

use Yii;
use yii\base\Model;

/**
 * Advcusd form
 */
class Advcusd extends Model {
    public $wallet;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['wallet'], 'required'],
            ['wallet', 'validateWallet']
        ];
    }

    public function validateWallet($attribute, $params) {
        $str = $this->$attribute;

        if (!preg_match('/^U/', $str)) {
            $this->addError($attribute, Yii::t('app', 'Кошелёк Advanced Cash должен начинаться с "U"'));
            return false;
        }
    }

    public function attributeLabels() {
        return [
            'wallet' => Yii::t('app', 'Кошелёк'),
        ];
    }

    public function fieldsTo() {
        return [
            'wallet' => [
                'title' => Yii::t('app', 'Кошелёк'),
                'placeholder' => Yii::t('app', 'Укажите кошелёк для получения'),
            ]
        ];
    }
}