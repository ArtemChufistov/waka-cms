<?php
namespace restapi\modules\changer\models\form\currency;

use Yii;
use yii\base\Model;

/**
 * Sberrub form
 */
class Sberrub extends Model {
    public $cardNumber;
    public $fio;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['cardNumber', 'fio'], 'required'],
        ];
    }

    public function attributeLabels() {
        return [
            'cardNumber' => Yii::t('app', 'Карта'),
            'fio' => Yii::t('app', 'Ф.И.О.'),
        ];
    }

    public function fields() {
        return [
            'cardNumber' => [
                'title' => Yii::t('app', 'На карту'),
                'placeholder' => Yii::t('app', 'Номер карты'),
            ], 'fio' => [
                'title' => Yii::t('app', 'Ф.И.О. получателя'),
                'placeholder' => Yii::t('app', 'ФИО получателя'),
            ]
        ];
    }
}