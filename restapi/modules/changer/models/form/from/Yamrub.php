<?php
namespace restapi\modules\changer\models\form\from;

use Yii;
use yii\base\Model;

/**
 * Yamrub form
 */
class Yamrub extends \restapi\modules\changer\models\form\currency\Yamrub {

    public function rules() {
        return [];
    }

    public function fields() {
        return [];
    }
}