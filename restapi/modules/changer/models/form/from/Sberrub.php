<?php
namespace restapi\modules\changer\models\form\from;

use Yii;
use yii\base\Model;

/**
 * Sberrub form
 */
class Sberrub extends \restapi\modules\changer\models\form\currency\Sberrub {
    public $cardNumber;
    public $fio;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['cardNumber', 'fio'], 'required'],
        ];
    }

    public function fields() {
        return [
            'cardNumber' => [
                'title' => Yii::t('app', 'С карты'),
                'placeholder' => Yii::t('app', 'Укажите номер карты'),
            ], 'fio' => [
                'title' => Yii::t('app', 'Ф.И.О. отправителя'),
                'placeholder' => Yii::t('app', 'Укажите ФИО отправителя'),
            ],
        ];
    }
}