<?php
namespace restapi\modules\changer\models;

use Yii;
use yii\mongodb\ActiveRecord;

class Currency extends \common\modules\changer\models\Currency {
    public $fields;

    public function fields() {
        return [
            'key',
            'title',
            'label',
            'in',
            'out',
            'minamount',
            'maxamount',
            'fields'
        ];
    }
}