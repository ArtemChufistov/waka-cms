<?php

namespace restapi\modules\changer\controllers;

use restapi\modules\changer\models\Course;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * Course controller
 */
class CourseController extends Controller
{

    public $enableCsrfValidation = false;

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
        ];

        return $behaviors;
    }

    public function actionDetail() {
        $course = Course::find()
            ->where(['active' => Course::ACTIVE_TRUE])
            ->all();

        return $course;
    }
}