<?php

namespace restapi\modules\changer\controllers;

use restapi\modules\changer\models\form\OrderForm;
use restapi\modules\changer\models\Order;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\Cors;
use yii\rest\Controller;
use Yii;

/**
 * Order controller
 */
class OrderController extends Controller {
    public $enableCsrfValidation = false;

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
        ];

        return $behaviors;
    }

    public function actions() {
        $actions = parent::actions();
        $actions['options'] = [
            'class' => 'yii\rest\OptionsAction',
        ];
        return $actions;
    }

    // Список обменов
    public function actionList() {
        $actions = parent::actions();

        $query = \restapi\modules\changer\models\admin\Order::find();

        $page = 0;
        if (!empty(Yii::$app->request->get('page'))) {
            $page = Yii::$app->request->get('page');
        }

        $pageSize = 20;
        if (!empty(Yii::$app->request->get('page_size'))) {
            $pageSize = Yii::$app->request->get('page_size');
        }

        $transactionProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $pageSize,
                'page' => $page,
            ],
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
            ],
        ]);

        $dataModels = $transactionProvider->getModels();

        return new ArrayDataProvider([
            'allModels' => $dataModels,
            'pagination' => [
                'pageSize' => $pageSize,

            ],
        ]);
    }

    // Создание обмена
    public function actionMake() {
        $orderForm = new OrderForm;

        if ($orderForm->load(Yii::$app->request->post(), '') && $orderForm->validate()) {

            $order = new Order;
            $order->prepareNewAttributes($orderForm->attributes);
            $order->save();

            return $order;
        }else{
            return $orderForm;
        }
    }

    // Подтверждение обмена
    public function actionConfirm($hash, $orderId) {
        $order = Order::find()->where(['id' => $orderId])->one();
        $order->status = Order::STATUS_WAIT_INCOMING_MONEY;
        $order->save();

        return $return;
    }

    public function actionValidateFields() {
        $orderForm = new OrderForm;

        if ($orderForm->load(Yii::$app->request->post(), '') && $orderForm->validate()) {
            return true;
        }else{
            return $orderForm;
        }
    }

    public function actionGet($hash, $orderId) {
        $order = Order::find()
            ->where(['hash' => $hash])
            ->andWhere(['id' => $orderId])
            ->one();

        return $order;
    }

    public function actionLast() {
        return [];
    }
}