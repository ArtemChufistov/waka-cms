<?php

namespace restapi\modules\changer\controllers;

use restapi\modules\changer\models\Course;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * Reserve controller
 */
class ReserveController extends Controller
{

    public $enableCsrfValidation = false;

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
        ];

        return $behaviors;
    }

    public function actionList() {
        return [];
    }
}