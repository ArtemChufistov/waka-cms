<?php

namespace restapi\modules\changer\controllers;

use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\Cors;
use yii\rest\Controller;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Order controller
 */
class WalletController extends Controller
{
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        $actions['options'] = [
            'class' => 'yii\rest\OptionsAction',
        ];
        return $actions;
    }

    public function actionGet($id) {
        $model = $this->findAdminModel($id);

        return $model;
    }

    public function actionUpdate() {
        $model = $this->findAdminModel(Yii::$app->request->post()['id']);

        if ($model->load(Yii::$app->request->post(), '') && $model->save()) {
            return $model;
        }else{
            return $model;
        }
    }

    // Список платёжных систем
    public function actionList() {
        $actions = parent::actions();

        $query = \restapi\modules\changer\models\admin\Wallet::find();

        $page = 0;
        if (!empty(Yii::$app->request->get('page'))) {
            $page = Yii::$app->request->get('page');
        }

        $pageSize = 20;
        if (!empty(Yii::$app->request->get('page_size'))) {
            $pageSize = Yii::$app->request->get('page_size');
        }

        $transactionProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $pageSize,
                'page' => $page,
            ],
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
            ],
        ]);

        $dataModels = $transactionProvider->getModels();

        return new ArrayDataProvider([
            'allModels' => $dataModels,
            'pagination' => [
                'pageSize' => $pageSize,

            ],
        ]);
    }

    protected function findAdminModel($id) {
        if (($model = \restapi\modules\changer\models\admin\Wallet::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}