<?php

namespace restapi\modules\personal\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $title
 * @property string $small_text
 * @property string $content
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property NewsCategory $category
 */
class News extends \common\models\News {

    public function fields() {
        return [
            'id',
            'category' => function ($model) {
                /** @var News $model */
                return $model->category->title;
            },
            'title',
            'small_text',
            'content',
            'status',
            'created_at',
            'updated_at',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory() {
        return $this->hasOne(NewsCategory::className(), ['id' => 'category_id']);
    }
}
