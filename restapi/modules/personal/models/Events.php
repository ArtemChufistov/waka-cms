<?php
/**
 * Created by PhpStorm.
 * User: admins
 * Date: 09.09.2017
 * Time: 17:58
 */

namespace restapi\modules\personal\models;


use common\components\Node;
use restapi\models\UserPayWalletEva;
use yii\base\Model;
use yii\helpers\Json;

class Events extends Model {

    public static function getMinEthBalance()
    {
        return 0.001;
    }

    public static function addCoin($to){
        self::sendRefillGas($to);
    }

    public static function burn(){
        // start return real $$$
    }

    public static function withdraw($to){
        $tmp2 = self::sendRefillGas($to);
        return [$tmp2];
    }

    public static function transfer($from, $to, $gas){
        $tmp1 = self::returnGas($from, $gas);
        $tmp2 = self::sendRefillGas($to);
        return [$tmp1, $tmp2];
    }

    protected static function returnGas($from, $gas){
        $user_from = UserPayWalletEva::findOne(['wallet' => $from]);
        if ($user_from) {
            $balance_from = Node::getBalance($from);
            if ($balance_from['ETH']/10000000000000000 < self::getMinEthBalance() * 1000) { // < 0.001 eth
                $returnGas = (self::getMinEthBalance() - $balance_from['ETH']/pow(10, 18));
                if ($transaction_from = Node::sendTransactionMU($user_from->address, $returnGas, 1)) {
                    $transaction_new = Transaction::updateOrCreateTransaction('master wallet', $from, $transaction_from['hash'], 'eth', $returnGas * pow(10, 18), 'return GAS', null, Transaction::TYPE_RETURN_ETH, Transaction::STATUS_CREATED);
                    return [Json::encode($transaction_from), Json::encode($transaction_new), $returnGas, $balance_from['ETH']];
                }

            }
        }
        return "no return GAS";
    }

    protected static function sendRefillGas($to){
        $user_to = UserPayWalletEva::findOne(['wallet' => $to]);
        if ($user_to) {
            $balance_to = Node::getBalance($to);
            if ($user_to->user->refill ==0 && $balance_to['ETH'] ==0){
                $transaction_to = Node::sendTransactionMU($user_to->address, self::getMinEthBalance(), 2); // 0.001 eth
                $user_to->user->refill = 1;
                $user_to->user->save();
                $transaction_new = Transaction::updateOrCreateTransaction('master wallet', $to, $transaction_to['hash'], 'eth', self::getMinEthBalance(), 'add ETH', null,Transaction::TYPE_ADD_ETH, Transaction::STATUS_CREATED);
                return [$transaction_to, $transaction_new];
            }
        }
        return "no refill GAS";
    }

}