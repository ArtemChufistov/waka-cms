<?php

namespace restapi\modules\personal\models;

use common\models\ServiceCommission;
use Yii;

class Transaction extends \restapi\models\Transaction
{
    public $showAmount;
    public $showAddress;

    public function fields()
    {
        return [
            'id',
            'currency',
            'amount',
            'from',
            'to',
            'created_at',
            'updated_at',
            'status',
            'comission',
            'hash',
            'comment',
            'block',
            'type',
            'showAmount',
            'showAddress'
        ];
    }

    public static function updateOrCreateTransaction($from, $to, $hash, $currency, $amount, $comment, $block, $type, $status=Transaction::STATUS_CREATED){
        $model = Transaction::findOne(['hash' => $hash]);
        if (empty($model)) {
            $model = new Transaction();
            $model->currency = $currency;
            $model->amount = ($currency =='eth' || $currency == 'ETH')?($amount/pow(10, 18)):$amount;
            $model->from = $from;
            $model->to = $to;
            $model->status = Transaction::STATUS_CONFIRM;
            $model->commission_id = null;
            $model->hash = $hash;
            $model->comment = $comment;
            $model->block = $block;
            $model->type = $type?$type:Transaction::TYPE_TRANSFER;
            $model->save();
            return $model;
        } elseif ($model->status != $status || $model->block == null) {
            if ($model->block == null && $block) {
                $model->block = $block;
            }
            $model->status = $status;
            $model->save();
            return $model;
        }
    }
}
