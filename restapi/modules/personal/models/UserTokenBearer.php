<?php

namespace restapi\modules\personal\models;

use common\models\Documents;
use common\models\UserIpAgent;
use Yii;
use yii\behaviors\TimestampBehavior;

class UserTokenBearer extends \restapi\models\UserTokenBearer
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function fields()
    {
        return [
            'bearer' => function($model){ return $model->token;},
            'expire_token' => function($model){ return $model->expire_at;},
            'step_auth' => function($model){
                if (!self::findUserIp($model->user->id)) {
                    return $model->user->step_auth;
                }else{
                    return 0;
                }
            },
        ];
    }

    public static function createToken($userId){
        $date = new \DateTime();
        self::deleteAll('expire_at < :date',[':date'=>$date->getTimestamp()]);

        $model = new self();
        $model->user_id = $userId;
        $model->token = Yii::$app->security->generateRandomString(32);
        $model->expire_at = $date->getTimestamp() + 60*60*24*7; // 7 дней жизни токена
        $model->ip = Yii::$app->request->getUserIP();
        $model->agent = Yii::$app->request->getUserAgent();
        return ($model->save()?$model:$model->getErrors());
    }

    public static function findUserIp($user_id){
        $ip = Yii::$app->request->getUserIP();
        $agent = Yii::$app->request->getUserAgent();
        /** @var self $model */
        $model = self::find()->where(['user_id'=>$user_id])->orderBy('created_at DESC')->one();
        return (!empty($model) && $model->agent == $agent && $model->ip == $ip) ;
    }
}
