<?php

namespace restapi\modules\personal\models;

use Yii;

class Currency extends \common\models\Currency
{
    public function fields()
    {
        return [
            'id',
            'key',
            'title',
            'course_to_usd',
        ];
    }
}
