<?php

namespace restapi\modules\personal\controllers;

use restapi\modules\personal\models\News;
use restapi\modules\personal\models\NewsCategory;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * Site controller
 */
class NewsController extends Controller {
	public $enableCsrfValidation = false;

	public function behaviors() {
		$behaviors = parent::behaviors();
		$behaviors['corsFilter'] = [
			'class' => Cors::className(),
		];
		$behaviors['authenticator'] = [
			'class' => HttpBearerAuth::className(),
			'except' => ['options'],
		];

		return $behaviors;
	}

	public function actions() {
		$actions = parent::actions();
		$actions['options'] = [
			'class' => 'yii\rest\OptionsAction',
		];
		return $actions;
	}

	public function actionIndex() {
		return $this->getListNews();
	}

	public function actionView($id) {
		return $this->getItemNews($id);
	}

	public function actionCategory() {
		return NewsCategory::getList();
	}

	protected function getListNews() {
		return News::find()->where('id >=0')->limit(10)->all();
	}

	protected function getItemNews($id) {
		return News::find()->where(['id' => $id])->one();
	}

}
