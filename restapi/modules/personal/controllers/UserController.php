<?php

namespace restapi\modules\personal\controllers;

use common\components\DocumentFiles;
use common\components\GoogleAuthenticator;
use common\models\Document;
use restapi\models\form\ChangeEmailForm;
use restapi\models\form\ChangePasswordForm;
use restapi\models\form\ConfirmSmsCodeForm;
use restapi\models\form\GoogleAuthForm;
use restapi\models\form\UpdateInfoForm;
use restapi\models\form\UpdateStepAuth;
use restapi\models\User;
use restapi\models\UserLog;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;

class UserController extends Controller {

	public function behaviors() {
		$behaviors = parent::behaviors();
		$behaviors['corsFilter'] = [
			'class' => Cors::className(),
		];
		$behaviors['authenticator'] = [
			'class' => HttpBearerAuth::className(),
			'except' => ['options'],
		];
		return $behaviors;
	}

	public function actions() {
		return [
			'options' => [
				'class' => 'yii\rest\OptionsAction',
			],
		];
	}

	public function actionIndex() {
		return true;
	}

	public function actionInfo() {
		$user = User::findOne(['id' => Yii::$app->user->identity->id]);
		return $user;
	}

	public function actionCreatesmscode() {
		$model = new ConfirmSmsCodeForm();
		$model->smsConfirmationCode = rand(100000, 999999);

		if ($model->load(Yii::$app->request->post(), '') && $user = $model->create()) {
			return $user;
		} else {
			return $model;
		}
	}

	public function actionUpdateInfo() {
		$model = new UpdateInfoForm();
		if ($model->load(Yii::$app->request->post(), '') && $model->validate() && $user = $model->update()) {
			return $user;
		} else {
			return $model;
		}
	}

	public function actionUpdateStepAuth() {
		$model = new UpdateStepAuth();
		if ($model->load(Yii::$app->request->post(), '') && $user = $model->update()) {
			return $user;
		} else {
			return $model;
		}
	}

	public function actionChangeEmail() {
		$model = new ChangeEmailForm();
		if ($model->load(Yii::$app->request->post(), '') && $model->validate() && $user = $model->handle()) {
			return $user;
		} else {
			return $model;
		}
	}

	public function actionChangePassword() {
		$model = new ChangePasswordForm();
		if ($model->load(Yii::$app->request->post(), '') && $model->validate() && $user = $model->update()) {
			return $user;
		} else {
			return $model;
		}
	}

	public function actionGetGoogleAuth() {

		$ga = new GoogleAuthenticator;

		$user = User::find()->where(['id' => Yii::$app->user->identity->id])->one();

		if ($user->ga_secret != 0) {
			$secret = $user->ga_secret;
		} else {
			$secret = $ga->createSecret();
		}

		return [
			'secret' => $secret,
			'qr' => $ga->getQRCodeGoogleUrl(Yii::$app->params['personalUrl'], $secret),
		];
	}

	public function actionSaveGoogleAuth() {

		$model = new GoogleAuthForm;
		if ($model->load(Yii::$app->request->post(), '') && $model->validate() && $user = $model->handle()) {
			return $user;
		} else {
			return $model;
		}
	}

	public function actionAgreement() {
		$type = Document::TYPE_IMAGE_USER_AGREEMENT;
		/** @var User $user */
		$user = Yii::$app->user->identity;
		if (!($document = Document::find()->where(['user_id' => $user->id, 'type' => $type])->one())) {
			$document = new Document();
			$document->user_id = $user->id;
			$document->type = $type;
		}
		$document->verify = 1;
		$document->save();

		return User::findOne(['id' => $user->id]);
	}

	public function actionGetPhoneInfo() {
		$user = User::find()->where(['id' => Yii::$app->user->identity->id])->one();

		$phoneConfirmed = false;

		if (!empty($user->phone) && empty($user->sms_confirmation_code)) {
			$phoneConfirmed = true;
		}

		return [
			'phone' => $user->phone,
			'confirmed' => $phoneConfirmed,
		];
	}

	public function actionCheckPhoneCode() {
		$user = Yii::$app->user->identity;

		$data = Yii::$app->request->post();

		if ($user->sms_confirmation_code == $data['code']) {
			$user = User::find()->where(['id' => Yii::$app->user->identity->id])->one();
			$user->sms_confirmation_code = '';
			$user->save(false);
			return ['confirmed' => true];
		} else {
			return ['confirmed' => false];
		}
	}

	public function actionUploadAvatar() {
		try {
			$type = Document::TYPE_IMAGE_USER_AVATAR;
			$result = DocumentFiles::saveImageVerify(Yii::$app->user->identity->id, $type, $_FILES["image"]);
		} catch (Exception $e) {
			return $e->getMessage();
		}

		return $result;
	}

	public function actionBalance() {
		$user = User::find()->where(['id' => Yii::$app->user->id])->one();

		return $user->getBalances();
	}

	public function actionGetHistory() {

		$user = Yii::$app->user->identity;
		if (!$user) {
			return false;
		}
		$list = UserLog::find()->where(['user_id' => $user->id])->orderBy('created_at DESC')->limit(10)->all();
		return $list;
	}

	public function actionOptions() {
		return;
	}
}
