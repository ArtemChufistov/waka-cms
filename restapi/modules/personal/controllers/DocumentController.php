<?php

namespace restapi\modules\personal\controllers;

use common\components\DocumentFiles;
use common\models\Document;
use restapi\models\User;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * Site controller
 */
class DocumentController extends Controller {
	public $enableCsrfValidation = false;

	public function behaviors() {
		$behaviors = parent::behaviors();
		$behaviors['corsFilter'] = [
			'class' => Cors::className(),
		];
		$behaviors['authenticator'] = [
			'class' => HttpBearerAuth::className(),
			'except' => ['options', 'test'],
		];

		return $behaviors;
	}

	public function actions() {
		$actions = parent::actions();
		unset($actions['index']);
		$actions['options'] = [
			'class' => 'yii\rest\OptionsAction',
		];
		return $actions;
	}

	public function actionIndex() {
		$user = Yii::$app->user->identity;
		return $this->fetchVerifyList($user->id);
	}

	public function actionUploadImageVerify() {
		$image = Yii::$app->request->post('data');
		$type = Yii::$app->request->post('type');
		$user = Yii::$app->user->identity;
		$data = explode(',', $image);
		DocumentFiles::saveImageVerify($user->id, $type, base64_decode($data[1]));
		return $this->fetchVerifyList($user->id);
	}

	public function actionTest() {
		$user = User::find()->where(['id' => 67])->one();

		return base64_encode($user->avatar->file);
	}

	protected function fetchVerifyList($user_id) {
		$data = Document::find()
			->where(['type' => Document::TYPE_IMAGE_VERIFY_1])
			->orWhere(['type' => Document::TYPE_IMAGE_VERIFY_2])
			->andWhere(['user_id' => $user_id])->all();
		$data_new = [];
		foreach ($data as $item) {
			$data_new[$item->type] = $item;
		}
		return $data_new;
	}
}
