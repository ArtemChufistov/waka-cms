<?php

namespace restapi\modules\personal\controllers;

use common\components\Node;
use common\models\PersonalWallet;
use common\models\UserPswWallet;
use common\modules\finance\models\Payment;
use restapi\models\form\SigninForm;
use restapi\models\form\SignupForm;
use restapi\models\User;
use restapi\modules\personal\models\Contract;
use restapi\modules\personal\models\Events;
use restapi\modules\personal\models\Transaction;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\VarDumper;
use yii\rest\Controller;
use yii\filters\Cors;
use yii\web\UnauthorizedHttpException;

/**
 * Site controller
 */
class EventsController extends Controller {
    public $enableCsrfValidation = false;

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
        ];
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => ['save-account', 'get-account'],
        ];

        return $behaviors;
    }

    public function actions() {
        $actions = parent::actions();
        $actions['options'] = [
            'class' => 'yii\rest\OptionsAction',
        ];
        return $actions;
    }

    public function actionIndex() {
        return ['index'];
    }
}
