<?php

namespace restapi\modules\personal\controllers;

use restapi\modules\personal\models\Language;
use restapi\modules\personal\models\Message;
use restapi\modules\personal\models\SourceMessage;
use restapi\modules\personal\models\Translate;
use Yii;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;

/**
 * Site controller
 */
class TranslateController extends Controller {
    public $enableCsrfValidation = false;

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
        ];

        return $behaviors;
    }


    public function actions() {
//        $actions = parent::actions();
        $actions['options'] = [
            'class' => 'yii\rest\OptionsAction',
        ];
        return $actions;
    }

    public function actionIndex() {
        return true;
    }

    public function actionLanguage() {
        return ArrayHelper::map(Language::find()->all(), 'code', 'title');
    }

    public function actionTranslate($code = 'en_US') {
        return Translate::getMessagesForWallet($code);
    }

    public function actionTranslateAdd() {
        $data = Yii::$app->request->post('data', []);
        $current = Yii::$app->request->post('current', 'en_US');
        $sourceMessage = SourceMessage::findAll(['category' => 'Wallet', 'message' => $data]);
        $messagesDB = ArrayHelper::map($sourceMessage, 'message', 'id');
        foreach ($data as $item) {
            if (empty($messagesDB[$item])) {
                $newMessage = new SourceMessage();
                $newMessage->message = $item;
                $newMessage->category = "Wallet";
                $newMessage->save();
                $newMessage->getPrimaryKey();

                $messageItem = new Message();
                $messageItem->id = $newMessage->getPrimaryKey();
                $messageItem->language = $current;
                $messageItem->translation = $item;
                $messageItem->correctly = 0;
                $messageItem->save();
            } else {
                if (!(Message::findOne(['id'=> $messagesDB[$item], 'language'=> $current]))){
                    $messageItem = new Message();
                    $messageItem->id = $messagesDB[$item];
                    $messageItem->language = $current;
                    $messageItem->translation = $item;
                    $messageItem->correctly = 0;
                    $messageItem->save();
                }
            }
        }
        return Translate::getMessagesForWallet($current);
    }
}
