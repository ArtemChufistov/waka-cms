<?php

namespace restapi\modules\personal\controllers;

use common\components\GoogleAuthenticator;
use common\components\MailCompile;
use common\models\GoRef;
use common\models\MailTemplate;
use common\models\UserPswWallet;
use DateTime;
use restapi\models\form\NewpasswordForm;
use restapi\models\form\ResetForm;
use restapi\models\form\SigninForm;
use restapi\models\form\SignupForm;
use restapi\models\User;
use restapi\models\UserLog;
use restapi\models\UserStepToken;
use restapi\modules\personal\models\UserTokenBearer;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * Site controller
 */
class AuthController extends Controller {
	public $enableCsrfValidation = false;

	public function behaviors() {
		$behaviors = parent::behaviors();
		$behaviors['corsFilter'] = [
			'class' => Cors::className(),
		];
		$behaviors['authenticator'] = [
			'class' => HttpBearerAuth::className(),
			'only' => ['save-account', 'get-account'],
		];

		return $behaviors;
	}

	public function actions() {
		$actions = parent::actions();
		$actions['options'] = [
			'class' => 'yii\rest\OptionsAction',
		];
		return $actions;
	}

	public function actionIndex() {
		return ['index'];
	}

	/**
	 * @return array|SignInForm|\yii\web\IdentityInterface
	 */
	public function actionSignin() {
		$model = new SigninForm();
		if ($model->load(Yii::$app->request->post(), '') && $model->login()) {
			$user = Yii::$app->user->identity;
			if ($user->step_auth && !UserTokenBearer::findUserIp($user->id)) {
				if ($user->step_method == User::STEP_METHOD_EMAIL) {
					$token = UserStepToken::createNow($user->id);
					MailCompile::renderMail($user->email, MailTemplate::TYPE_CONFIRM_LOGIN, ['key' => $token->key]);
				} elseif ($user->step_method == User::STEP_METHOD_GOOGLE_AUTH) {
					$token = UserStepToken::createNow($user->id, $user->ga_secret);
				}

				return [
					'step_auth' => $user->step_auth,
					'token' => $token->token,
				];
			} else {
				UserLog::add(Yii::$app->user->id, UserLog::TYPE_LOGIN);
				return UserTokenBearer::createToken($user->id);
			}
		} else {
			return $model;
		}
	}

	public function actionSignup() {
		$model = new SignupForm();
		if ($model->load(Yii::$app->request->post(), '') && $user = $model->signup()) {
			UserLog::add($user->id, UserLog::TYPE_SIGNUP);
			return UserTokenBearer::createToken($user->id);
		} else {
			return $model;
		}
	}

	private function renderError($message) {
		Yii::$app->response->statusCode = 400;
		UserLog::add(Yii::$app->user->id, UserLog::TYPE_TO_STEP_LOGIN_ERROR, $message);
		return [[
			"field" => "key",
			"message" => $message,
		]];
	}

	public function actionConfirmLogin() {

		if (!($userToken = Yii::$app->request->post('token', false))) {
			return $this->renderError(Yii::t('app', 'Неизвестный токен'));
		}

		if (!($userKey = Yii::$app->request->post('key', false))) {
			return $this->renderError(Yii::t('app', 'Неизвестный код'));
		}

		if (!($token = UserStepToken::findOne(['token' => $userToken, 'key' => $userKey]))) {
			if (!($token = UserStepToken::findOne(['token' => $userToken]))) {
				return $this->renderError(Yii::t('app', 'Неправильный токен'));
			} else {
				if ($token->user->step_method == User::STEP_METHOD_GOOGLE_AUTH) {
					$ga = new GoogleAuthenticator;
					$code = $ga->getCode($userToken);
					if ($ga->verifyCode($userToken, $code, 2)) {
						if ($userKey != $code) {
							return $this->renderError(Yii::t('app', 'Неправильный код userKey'));
						}
					} else {
						return $this->renderError(Yii::t('app', 'Неправильный токен'));
					}
				} else {
					return $this->renderError(Yii::t('app', 'Неправильный код'));

				}
			}
		}

		$user = $token->user;
		$date = new DateTime();

		if (($date->getTimestamp() - (60 * 60 * 12)) > $token->updated_at) {
			return $this->renderError(Yii::t('app', 'Прошло больше 12 часов'));
		}

		$token->delete();
		UserLog::add($user->id, UserLog::TYPE_TO_STEP_LOGIN);
		return UserTokenBearer::createToken($user->id);
	}

	public function actionReset() {
		$model = new ResetForm();
		if ($model->load(Yii::$app->request->post(), '') && $user = $model->reset()) {
			UserLog::add($user->id, UserLog::TYPE_RESET_PASSWORD);
			return $user;
		} else {
			return $model;
		}
	}

	public function actionNewPassword() {
		$model = new NewpasswordForm();
		if ($model->load(Yii::$app->request->post(), '') && $user = $model->newpassword()) {
			UserLog::add(Yii::$app->user->id, UserLog::TYPE_SET_PASSWORD);
			return $user;
		} else {
			return $model;
		}
	}

	public function actionUpdateCountRefs() {
		$goRef = new GoRef;
		$goRef->ref = $_REQUEST['ref'];
		$goRef->date = date('Y-m-d H:i:s');

		$goRef->save(false);

		return [true];
	}

	public function actionGetAccount() {
		$user = Yii::$app->user->identity;
		$account = UserPswWallet::findOne(['user_id' => $user->id]);
		return $account;
	}

	/**
	 * Logout action.
	 *
	 * @return string
	 */
	public function actionLogout() {
		UserLog::add(Yii::$app->user->id, UserLog::TYPE_LOGOUT);
		Yii::$app->user->logout();

		return 'true';
	}

}
