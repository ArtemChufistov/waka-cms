<?php

namespace restapi\modules\personal;

use Yii;

/**
 * Class Module
 * @package restapi\modules\api\v1
 */
class Module extends \yii\base\Module
{
    /**
     * @var string
     */
    public $controllerNamespace = 'restapi\modules\personal\controllers';

    public function init()
    {
        parent::init();
    }
}
