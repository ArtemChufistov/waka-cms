<?php

namespace restapi\modules\explorer;

use Yii;
use yii\base\BootstrapInterface;

/**
 * Class Module
 * @package restapi\modules\explorer\v1
 */
class Module extends \yii\base\Module implements BootstrapInterface {
    /**
     * @var string
     */
    public $controllerNamespace = 'restapi\modules\explorer\controllers';

    public function init() {
        parent::init();
    }

    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules([[
                'class' => 'yii\web\UrlRule',
                'pattern' => 'explorer/transaction/detail/<currencyKey>/<txid>',
                'route' => 'explorer/transaction/detail',
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'explorer/block/detail/<currencyKey>/<hash>',
                'route' => 'explorer/block/detail',
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'explorer/block/all/<currencyKey>/<count>',
                'route' => 'explorer/block/all',
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'explorer/block/all/<currencyKey>',
                'route' => 'explorer/block/all',
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'explorer/block/height/<currencyKey>',
                'route' => 'explorer/block/height',
            ], [
                'class' => 'yii\web\UrlRule',
                'pattern' => 'explorer/wallet/address/<currencyKey>/<address>',
                'route' => 'explorer/wallet/address',
            ]
        ], false);
    }
}
