<?php

namespace restapi\modules\explorer\controllers;

use common\modules\crypto\coin\factory\CoinFactory;
use restapi\modules\explorer\models\Currency;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * Block controller
 */
class BlockController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
        ];

        return $behaviors;
    }

    public $enableCsrfValidation = false;

    public function actionDetail($currencyKey, $hash) {
        $currency = Currency::find()
            ->where(['active' => Currency::ACTIVE_TRUE])
            ->andWhere(['key' => strtoupper($currencyKey)])
            ->one();

        if (empty($currency)) {
            throw new \Exception(Yi::t('app', 'Такой валюты не существует'));
        }

        $blockNs = CoinFactory::getNs($currency->key, 'detail\Block');

        $block = $blockNs::find()->where(['hash' => $hash])->one();

        return $block;
    }

    public function actionAll($currencyKey, $count = 10) {
        $currency = Currency::find()
            ->where(['active' => Currency::ACTIVE_TRUE])
            ->andWhere(['key' => strtoupper($currencyKey)])
            ->one();

        if (empty($currency)) {
            throw new \Exception(Yi::t('app', 'Такой валюты не существует'));
        }

        $blockNs = CoinFactory::getNs($currency->key, 'Block');

        $blockArray = $blockNs::find()
            ->orderBy(['num' => SORT_DESC])
            ->limit($count)
            ->all();

        return $blockArray;
    }

    public function actionHeight($currencyKey) {
        return [
            'test1221'
        ];
    }
}