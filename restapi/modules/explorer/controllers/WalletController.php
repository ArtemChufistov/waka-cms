<?php

namespace restapi\modules\explorer\controllers;

use common\modules\crypto\coin\factory\CoinFactory;
use restapi\modules\explorer\models\Currency;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * Wallet controller
 */
class WalletController extends Controller
{
    public $enableCsrfValidation = false;

    public function actionAddress($currencyKey = '', $address = ''){
        $currency = Currency::find()
            ->where(['active' => Currency::ACTIVE_TRUE])
            ->andWhere(['key' => strtoupper($currencyKey)])
            ->one();

        if (empty($currency)) {
            throw new \Exception(Yi::t('app', 'Такой валюты не существует'));
        }

        $walletNs = CoinFactory::getNs($currency->key, 'detail\Wallet');

        $wallet = $walletNs::find()->where(['address' => $address])->one();

        return $wallet;

    }
}