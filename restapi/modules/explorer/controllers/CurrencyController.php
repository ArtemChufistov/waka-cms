<?php

namespace restapi\modules\explorer\controllers;

use restapi\modules\explorer\models\Currency;
use restapi\modules\explorer\models\CurrencySummary;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * Block controller
 */
class CurrencyController extends Controller{

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
        ];

        return $behaviors;
    }

    public $enableCsrfValidation = false;

    public function actionSummary() {
        $currencyArray = CurrencySummary::find()
            ->where(['active' => Currency::ACTIVE_TRUE])
            ->all();

        return $currencyArray;
    }
}