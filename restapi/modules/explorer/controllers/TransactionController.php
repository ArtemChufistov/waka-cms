<?php

namespace restapi\modules\explorer\controllers;

use common\modules\crypto\coin\factory\CoinFactory;
use restapi\modules\explorer\models\Currency;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;

/**
 * Transaction controller
 */
class TransactionController extends Controller {

    public $enableCsrfValidation = false;

    public function actionDetail($currencyKey, $txid) {
        $currency = Currency::find()
            ->where(['active' => Currency::ACTIVE_TRUE])
            ->andWhere(['key' => strtoupper($currencyKey)])
            ->one();

        if (empty($currency)) {
            throw new \Exception(Yi::t('app', 'Такой валюты не существует'));
        }

        $txNs = CoinFactory::getNs($currency->key, 'detail\Transaction');

        $tx = $txNs::find()->where(['txid' => $txid])->one();

        return $tx;
    }
}