<?php

namespace restapi\modules\explorer\models;

use Yii;
use yii\helpers\Json;
use yii\behaviors\TimestampBehavior;
use common\modules\crypto\models\CourseChart;
use common\modules\crypto\coin\factory\CoinFactory;

class CurrencySummary extends \common\models\Currency
{
    public function fields() {
        return [
            'id',
            'key',
            'title',
            'active',
            'sort',
            'course_to_usd' => function($model) {
                if (empty($model->course_to_usd)) {
                    $data = CourseChart::find()
                        ->where(['currency_id_from' => $model->id])
                        ->orderBy(['date' => SORT_DESC])
                        ->one();

                    if(empty($data)){
                        $data = 0;
                    }else{
                        $data = $data->high;
                    }
                }else{
                    $data = $model->course_to_usd;
                }

                return $data;
            },
            'latest_block',
            'count_transactions' => function ($model) {
                $transactionNs = CoinFactory::getNs($this->key, 'Transaction');
                if (class_exists($transactionNs)) {
                    $txs = $transactionNs::find()->count();
                    return $txs;
                }else{
                    return 0;
                }
            },
        ];
    }
}