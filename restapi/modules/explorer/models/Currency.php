<?php

namespace restapi\modules\explorer\models;

use Yii;
use yii\helpers\Json;
use yii\behaviors\TimestampBehavior;

class Currency extends \common\models\Currency
{
    public function fields() {
        return [
            'id',
            'key',
            'title',
            'active',
            'sort',
            'course_to_usd',
            'latest_block',
        ];
    }
}