<?php
$params = array_merge(
	require (__DIR__ . '/../../common/config/params.php'),
	require (__DIR__ . '/../../common/config/params-local.php'),
	require (__DIR__ . '/params.php'),
	require (__DIR__ . '/params-local.php')
);

return [
	'id' => 'app-restapi',
	'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'explorer', 'changer', 'personal'],
	'controllerNamespace' => 'restapi\controllers',
	'modules' => [
		'personal' => [
			'class' => 'restapi\modules\personal\Module',
		],
		'crypto' => [
			'class' => 'restapi\modules\crypto\Module',
		],
		'support' => [
			'class' => 'restapi\modules\support\Module',
		],
        'explorer' => [
            'class' => 'restapi\modules\explorer\Module',
        ],
        'changer' => [
            'class' => 'restapi\modules\changer\Module',
        ],
	],
	'language' => 'ru_RU',
	'components' => [
		'request' => [
			'parsers' => [
				'application/json' => 'yii\web\JsonParser',
			],
			'enableCsrfValidation' => false,
		],
		'user' => [
			'identityClass' => 'restapi\models\User',
			'enableAutoLogin' => true,
		],
		'response' => [
			'class' => 'yii\web\Response',
			'on beforeSend' => function ($event) {
				$response = $event->sender;
				if ($response->data !== null) {
					$response->headers['access-control-allow-origin'] = ["*"];
//                    $response->data = [
					//                        'success' => $response->isSuccessful,
					//                        'data' => $response->data,
					//                        'headers' => $response->headers,
					//                    ];
				}
			},
		],
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 3,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'i18n' => [
			'translations' => [
				'app*' => [
					'class' => 'yii\i18n\DbMessageSource',
					'forceTranslation' => true,
					'sourceMessageTable' => '{{%source_message}}',
					'messageTable' => '{{%message}}',
					'enableCaching' => false,
					'cachingDuration' => 3600,
					'sourceLanguage' => 'en_US',
				],
				'wallet*' => [
					'class' => 'yii\i18n\DbMessageSource',
					'forceTranslation' => true,
					'sourceMessageTable' => '{{%source_message}}',
					'messageTable' => '{{%message}}',
					'enableCaching' => false,
					'cachingDuration' => 3600,
					'sourceLanguage' => 'en_US',
				],
			],
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'rules' => [
				'changer/paysys/get/<id:\d+>' => 'changer/paysys/get',
				'changer/currency/get/<id:\d+>' => 'changer/currency/get',
				'changer/order/get/<hash>/<orderId:\d+>' => 'changer/order/get',
				'changer/order/confirm/<hash>/<orderId:\d+>' => 'changer/order/confirm',
				'GET crypto/currency/index' => 'crypto/currency/index',
				'GET crypto/coin/get-address' => 'crypto/coin/get-address',
				'OPTIONS personal/<controller>/<action>/<id:\d+>' => 'personal/<controller>/options',
				'OPTIONS personal/<controller>/<action>' => 'personal/<controller>/options',
				'GET personal/user/index' => 'personal/user/index',
				'GET personal/payee/<id:\d+>' => 'personal/payee/view',
				'GET personal/user/wallet' => 'personal/user/get-wallet',
				'POST personal/user/wallet' => 'personal/user/create-wallet',
				'GET personal/user/balance' => 'personal/user/balance',
				'GET personal/user/get-balance-eth' => 'personal/user/get-balance-eth',
			],
		],
	],

	'params' => $params,
];
